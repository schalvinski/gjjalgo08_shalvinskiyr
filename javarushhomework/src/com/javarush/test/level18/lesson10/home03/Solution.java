package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать содержимое третьего файла
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        String fileName3 = reader.readLine();
        reader.close();

        FileOutputStream outputStream = new FileOutputStream(fileName1, true);
        FileInputStream inputStream2 = new FileInputStream(fileName2);
        FileInputStream inputStream3 = new FileInputStream(fileName3);

        byte[] buffer2 = new byte[inputStream2.available()];
        byte[] buffer3 = new byte[inputStream3.available()];

        if (inputStream2.available() > 0) { inputStream2.read(buffer2); }
        if (inputStream3.available() > 0) { inputStream3.read(buffer3); }

        inputStream2.close();
        inputStream3.close();

        outputStream.write(buffer2);
        outputStream.write(buffer3);
        outputStream.close();
    }
}
