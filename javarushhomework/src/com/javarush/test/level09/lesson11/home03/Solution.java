package com.javarush.test.level09.lesson11.home03;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/* Метод в try..catch
Вводить с клавиатуры числа. Код по чтению чисел с клавиатуры вынести в отдельный метод readData.
Обернуть все тело (весь код внутри readData, кроме объявления списка, где будут храниться числа) этого метода
в try..catch.
Если пользователь ввёл какой-то текст, вместо ввода числа, то метод должен перехватить исключение и вывести на экран
все введенные числа в качестве результата.
Числа выводить с новой строки сохраняя порядок ввода
*/

public class Solution
{
    public static void main(String[] args)
    {
        readData();
    }

    public static void readData()
    {
        //напишите тут ваш код
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        try
        {
            while (true)
            {
                numbers.add(scanner.nextInt());
            }
        }
        catch (Throwable e)
        {
            for (Integer number : numbers)
            {
                System.out.println(number);
            }
        }
    }
}
