package com.javarush.test.level09.lesson11.home05;

import java.util.ArrayList;
import java.util.Scanner;

/* Гласные и согласные буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа должна вывести на экран две строки:
1. первая строка содержит только гласные буквы
2. вторая - только согласные буквы и знаки препинания из введённой строки.
Буквы соединять пробелом.

Пример ввода:
Мама мыла раму.
Пример вывода:
а а ы а а у
М м м л р м .
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        char[] book = input.toCharArray();
        ArrayList<Character> nonvowel = new ArrayList<Character>();
        ArrayList<Character> vowel = new ArrayList<Character>();

        for (int i = 0; i < book.length; i++)
        {
            if (isVowel(book[i]))
            {
                vowel.add(book[i]);
            } else
            {
                if (book[i] == ' ')
                {
                } else
                {
                    nonvowel.add(book[i]);
                }
            }
        }

        for (Character ch : vowel)
        {
            System.out.print(ch + " ");
        }
        System.out.println();
        for (Character ch : nonvowel)
        {
            System.out.print(ch + " ");
        }
    }


    public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};

    //метод проверяет, гласная ли буква

    public static boolean isVowel(char c)
    {
        c = Character.toLowerCase(c);  //приводим символ в нижний регистр - от заглавных к строчным буквам

        for (char d : vowels)   //ищем среди массива гласных
        {
            if (c == d)
                return true;
        }
        return false;
    }
}
