package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Integer[] numbers = new Integer[3];

        //Напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sNum1 = reader.readLine();
        String sNum2 = reader.readLine();
        String sNum3 = reader.readLine();
        numbers[0] = Integer.parseInt(sNum1);
        numbers[1] = Integer.parseInt(sNum2);
        numbers[2] = Integer.parseInt(sNum3);
        Arrays.sort(numbers, Collections.reverseOrder());

        for (int i = 0; i < 2; i++)
            System.out.print(numbers[i] + " ");
        System.out.println(numbers[2]);
    }
}
