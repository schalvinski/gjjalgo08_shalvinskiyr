package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static int max(int x, int y)
    {
        return x > y ? x : y;
    }

    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String sNum1 = reader.readLine();
        String sNum2 = reader.readLine();
        String sNum3 = reader.readLine();
        String sNum4 = reader.readLine();
        int number1 = Integer.parseInt(sNum1);
        int number2 = Integer.parseInt(sNum2);
        int number3 = Integer.parseInt(sNum3);
        int number4 = Integer.parseInt(sNum4);

        System.out.println(max(number1, number2) > max(number3, number4) ? max(number1, number2) : max(number3, number4));

    }
}
