package com.javarush.test.level07.lesson04.task05;

import java.util.Scanner;

/* Один большой массив и два маленьких
1. Создать массив на 20 чисел.
2. Ввести в него значения с клавиатуры.
3. Создать два массива на 10 чисел каждый.
4. Скопировать большой массив в два маленьких: половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.
*/

public class ArrayCopyToAnotherArray {
    public static void main(String[] args) throws Exception {
        //Напишите тут ваш код
        int[] numbers = new int[20];
        int[] numbers1 = new int[10];
        int[] numbers2 = new int[10];


        for (int i = 0; i < 20; i++) {
            numbers[i] = new Scanner(System.in).nextInt();
        }
        System.arraycopy(numbers,0 , numbers1, 0, numbers1.length);



        for (int i = 0; i < numbers1.length; i++) {
            System.out.println(numbers1[i]);
        }

        System.arraycopy(numbers,numbers2.length, numbers2, 0, numbers2.length);

        for (int i = 0; i < numbers1.length; i++) {
            System.out.println(numbers2[i]);
        }


//        for (int i = 0; i < numbers2.length; i++) {
//            System.out.println(numbers1[i]);
//        }

//        int[] numbers1 =Arrays.copyOf(arr1, 7)

//        for (int i = 0; i < 10; i++)
//        {
//            numbers1[i] = numbers[i];
//            numbers2[i] = numbers[i + 10];
//            System.out.println(numbers2[i]);
//        }
    }
}
//class ACDemo_arraycopy {
//    static byte a[] = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
//    static byte b[] = { 77, 77, 77, 77, 77, 77, 77, 77, 77, 77 };
//    static int [] fu = new int [] { 1, 2, 3, 0, 0, 07, 0, 0, 77, 77 };
//    static int fu2[] = { 4, 4, 4, 5, 77, 77, 77, 77, 77, 77 };
//
//    public static void main(String args[]) {
//        System.out.println("a = " + new String(a));
//        System.out.println("b = " + new String(b));
//
//        System.arraycopy(a, 0, b, 0, a.length);
//
//        System.out.println("a = " + new String(a));
//        System.out.println("b = " + new String(b));
//
//        System.arraycopy(a, 0, a, 1, a.length - 1);
//        System.arraycopy(b, 1, b, 0, b.length - 1);
//
//        System.out.println("a = " + new String(a));
//        System.out.println("b = " + new String(b));
//
//        System.arraycopy(fu,0,fu2,0,5);//изменился только fu2 на длину 5
//
//        for (int i = 0; i < fu2.length; i++)
//        {
//            System.out.println("c =" + fu2[i]);
//        }
//    }