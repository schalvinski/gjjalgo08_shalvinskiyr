package com.javarush.test.level07.lesson09.task05;

import java.util.ArrayList;
import java.util.Scanner;

/* Удвой слова
1. Введи с клавиатуры 10 слов в список строк.
2. Метод doubleValues должен удваивать слова по принципу a,b,c -> a,a,b,b,c,c.
3. Используя цикл for выведи результат на экран, каждое значение с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //read strings and init ArrayList list here - считать строки с консоли и объявить ArrayList list тут
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<String>();

        int n = 10;

        for (int i = 0; i < n; i++)
        {
            list.add(scanner.nextLine());
        }

        ArrayList<String> result = doubleValues(list);

        //print result - вывести на экран result

        for (String s : result)
        {
            System.out.println(s);
        }
    }

    public static ArrayList<String> doubleValues(ArrayList<String> list)
    {
        // add your code here - добавь код тут
        ArrayList<String> result = new ArrayList<String>();

        for (int i = 0; i < list.size(); i++)
        {
            result.add(list.get(i));
            result.add(list.get(i));
        }

        return result;
    }
}
