package com.javarush.test.level07.lesson12.home02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* Переставить M первых строк в конец списка
Ввести с клавиатуры 2 числа N  и M.
Ввести N строк и заполнить ими список.
Переставить M первых строк в конец списка.
Вывести список на экран, каждое значение с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> list = new ArrayList<String>();

        int n = Integer.parseInt(reader.readLine());

        int m = Integer.parseInt(reader.readLine());

        for (int i = 0; i < 2; i++)
        {
            list.add(reader.readLine());
        }

        List<String> result = new ArrayList<String>();

        for (int i = m; i < list.size(); i++)
        {
            result.add(list.get(i));
        }

        for (int i = 0; i < 2; i++)
        {
            result.add(list.get(i));
        }

        for (String string : result)
        {
            System.out.println(string);
        }
    }
}
