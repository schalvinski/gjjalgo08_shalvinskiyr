package com.javarush.test.level03.lesson06.task01;

/* Мама мыла раму
Вывести на экран все возможные комбинации слов «Мама», «Мыла», «Раму».
Подсказка: их 6 штук. Каждую комбинацию вывести с новой строки. Слова не разделять. Пример:
МылаРамуМама
РамуМамаМыла
...
*/

public class Solution
{
    public static void print(int a, int b, int c)
    {
        String[] words = new String[]{"Мама", "Мыла", "Раму"};
        System.out.println(words[a] + words[b] + words[c]);
    }

    public static void main(String[] args)
    {
        //Напишите тут ваш код
        print(0, 1, 2);
        print(1, 0, 2);
        print(1, 2, 0);
        print(0, 2, 1);
        print(2, 0, 1);
        print(2, 1, 0);
    }
}