package com.javarush.test.level05.lesson09.task05;

/* Создать класс прямоугольник (Rectangle)
Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height (левая координата, верхняя, ширина и высота). Создать для него как можно больше конструкторов:
Примеры:
-	заданы 4 параметра: left, top, width, height
-	ширина/высота не задана (оба равны 0)
-	высота не задана (равно ширине) создаём квадрат
-	создаём копию другого прямоугольника (он и передаётся в параметрах)
*/

public class Rectangle
{
    //Напишите тут ваш код
    public int left = 0, top = 0, width = 0, height = 0;

    public Rectangle(int top, int left)
    {
        this.left = left;
        this.top = top;
    }

    public Rectangle(int top, int left, int width)
    {
        this.left = left;
        this.top = top;
        this.width = width;
    }

    public Rectangle(int top, int left, int height, int width)
    {
        this.left = left;
        this.top = top;
        this.height = height;
        this.width = width;
    }

    public Rectangle(Rectangle otherRect)
    {
        this.left = otherRect.left;
        this.top = otherRect.top;
        this.height = otherRect.height;
        this.width = otherRect.width;
    }
}
