package com.javarush.test.level05.lesson12.bonus03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/* Задача по алгоритмам
Написать программу, которая:
1. вводит с консоли число N > 0
2. потом вводит N чисел с консоли
3. выводит на экран максимальное из введенных N чисел.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = 0;
        while (n <= 0)
        {
            n = Integer.parseInt(reader.readLine());
        }

        int[] numbers = new int[n];

        for (int i = 0; i < n; i++)
        {
            numbers[i] = Integer.parseInt(reader.readLine());
        }

        int maximum = max(numbers);

        System.out.println(maximum);
    }

    public static int max(int[] array)
    {
        Arrays.sort(array);

        return array[array.length - 1];
    }
}

