package com.javarush.test.level08.lesson11.home04;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/* Минимальное из N чисел
1. Ввести с клавиатуры число N.
2. Считать N целых чисел и заполнить ими список - метод getIntegerList.
3. Найти минимальное число среди элементов списка - метод getMinimum.
*/

public class Solution
{
    public static void main(String[] args) throws Exception {
        List<Integer> integerList = getIntegerList();
        System.out.println(getMinimum(integerList));
    }

    public static int getMinimum(List<Integer> array) {
        //find minimum here - найти минимум тут
        int result = 0, i = 0;

        for (Integer number : array) {
            if ( i == 0) result = number;
            if (number < result) { result = number; }
            i++;
        }

        return result;
    }

    public static List<Integer> getIntegerList() throws IOException {
        //create and initialize a list here - создать и заполнить список тут
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        List<Integer> result = new ArrayList<Integer>();

        for (int i = 0; i < n; i++)
        {
            result.add(scanner.nextInt());
        }

        return result;
    }
}
