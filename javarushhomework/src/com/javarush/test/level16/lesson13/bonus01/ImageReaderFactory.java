package com.javarush.test.level16.lesson13.bonus01;

import com.javarush.test.level16.lesson13.bonus01.common.*;

public class ImageReaderFactory
{
    private static ImageReader imageReader;

    public static ImageReader getReader(ImageTypes imageType) {
        if (imageType == ImageTypes.JPG) { imageReader = new JpgReader(); }
        else if (imageType == ImageTypes.BMP) { imageReader = new BmpReader(); }
        else if (imageType == ImageTypes.PNG) { imageReader = new PngReader(); }
        else throw new IllegalArgumentException("Неизвестный тип картинки");

        return imageReader;
    }
}
