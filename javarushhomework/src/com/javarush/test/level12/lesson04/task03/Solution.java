package com.javarush.test.level12.lesson04.task03;

/* Пять методов print с разными параметрами
Написать пять методов print с разными параметрами.
*/

public class Solution
{
    public static void main(String[] args)
    {
        print(5);
        print(new Integer(25));
        print("125");
        print("a");
        print(true);
    }

    //Напишите тут ваши методы
    public static void print(int n) { System.out.println( n ); }
    public static void print(Integer n) { System.out.println( n ); }
    public static void print(String n) { System.out.println( n ); }
    public static void print(char ch) { System.out.println( ch ); }
    public static void print(boolean b) { System.out.println( b ); }
}
