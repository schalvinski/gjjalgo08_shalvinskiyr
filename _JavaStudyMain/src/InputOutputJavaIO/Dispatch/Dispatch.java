package InputOutputJavaIO.Dispatch;

// Динамическая диспетчеризация методов
class A {
	void callme() {
		System.out.println("Внутри метода callme() класса A");
	}
}

class B extends A {
	// переопределение метода callme()
	void callme() {
		System.out.println("Внутри метода callme() класса B");
	}
}

class C extends A {
	void callme() {
		System.out.println("Внутри метода callme() класса C");
	}
}


class Dispatch {
	public static void main(String args[]) {
		A a = new A(); // объект класса A
		B b = new B(); // объект класса B
		C c = new C(); // объект класса C

		A r; // получение ссылки класса A

		r = a; // r ссылается на объект класса A
		r.callme(); // вызов версии метода callme, определенной в A

		r = b; // r ссылается на объект класса B
		r.callme(); // вызов версии метода callme, определенной в B

		r = c; // r ссылается на объект класса C
		r.callme(); // вызов версии метода callme, определенной в C
	}
}
