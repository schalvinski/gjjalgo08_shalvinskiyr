package InputOutputJavaIO.FileWriterDemo;// Демонстрация применения FileWriter.
// Эта программа использует оператор try-с-ресурсами. Требует JDK 7.

import java.io.FileWriter;
import java.io.IOException;

class FileWriterDemo {
    public static void main(String args[]) throws IOException {
        String source = "Now is the time for all good men\n" +
                        " to come to the aid of their country\n" +
                        " and pay their due taxes.";
        char buffer[] = new char[source.length()];
        source.getChars(0, source.length(), buffer, 0);

        try ( FileWriter f0 = new FileWriter("/Users/user/Documents/dev/projects/getjavajob/_StudyJava/src/InputOutputJavaIO/FileWriterDemo/file2".toUpperCase());
              FileWriter f1 = new FileWriter("/Users/user/Documents/dev/projects/getjavajob/_StudyJava/src/InputOutputJavaIO/FileWriterDemo/file1");
              FileWriter f2 = new FileWriter("file3.txt") ) {
            // записать в первый файл
            for (int i = 0; i < buffer.length; i += 2) {
                f0.write(buffer[i]);
            }

            // записать во второй файл
            f2.write(buffer);
            f1.write(source);
            f1.append("1");

            // запись в третий файл
            f2.write(buffer, buffer.length - buffer.length / 4, buffer.length / 4);
        } catch (IOException e) {
            System.out.println("An I/O Error Occurred");
        }
    }
}
