package ru.ncedu.java.examples.io.zipper;

import java.io.IOException;

/**
 * @author Sergey Kolesnikov
 */
public interface Archiver {

    /**
     * method creates new zip archive with one file
     *
     * @param zipArchiveName name of created zip archive
     * @param fileName       name of added file
     * @throws IOException
     */
    public void createZipArchiveWithFile(String zipArchiveName, String fileName)
            throws IOException;

}
