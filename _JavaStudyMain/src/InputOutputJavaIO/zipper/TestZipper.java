package ru.ncedu.java.examples.io.zipper;

import java.io.IOException;

public class TestZipper {

    public static void main(String[] args) {
        try {
            Archiver zipper = new ZipArchiver();
            zipper.createZipArchiveWithFile(args[0], args[1]);
        } catch (IOException e) {
            System.out.println("Cannot read file. Exception: " + e.toString());
        } catch (Exception e) {
            System.out.println("Cannot perform action.\n Exception: " + e.toString());
            System.out.println("use TestZipper archive_name file_name");
        }
        System.out.println("Success!");
    }
}
