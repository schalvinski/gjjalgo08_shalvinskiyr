package InputOutputJavaIO.FileReaderDemo;// Демонстрация применения FileReader.
// Эта программа использует оператор try-с-ресурсами. Требует JDK 7.

import java.io.FileReader;
import java.io.IOException;

class FileReaderDemo {
    public static void main(String args[]) {
        try ( FileReader fr = new FileReader("/Users/user/Documents/dev/projects/getjavajob/_StudyJava/src/InputOutputJavaIO/FileWriterDemo/file1") ) {
            int c;

            // читает и отображает файл.
            while ( (c = fr.read()) != -1 ) {
                System.out.print((char) c);
            }
        } catch (IOException e) {
            System.out.println("I/O Error: " + e);
        }
    }
}
