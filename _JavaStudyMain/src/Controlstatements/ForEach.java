package Controlstatements;
//8
/**
 * Created by user on 03.03.16.
 */
// Use a for-each style for loop.
class ForEach {
    public static void main(String args[]) {
        int nums[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        int sum = 0;
        // use for-each style for to display and sum the values
        for(int x : nums) {  //nums - массив, x выражает теперь весь массив
            System.out.println("Value is: " + x);
            sum += x; }  //при каждом проходе 1, 1+2=3, 3+3=6, 6+4... - нахождение суммы
        System.out.println("Summation: " + sum);
    }
}