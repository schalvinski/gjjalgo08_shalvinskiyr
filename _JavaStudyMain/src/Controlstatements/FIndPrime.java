package Controlstatements;

/**
 * Created by user on 21.03.16.
 */
// Test for primes. Нахождение простого числа
class BreakFindPrime {
    public static void main(String args[]) {
        int num;
        boolean isPrime; // введем переменную isPrime к-ая может быть true/false
        num = 27;
        if(num < 2) isPrime = false;
        else isPrime = true;
        for(int i=2; i <= num/i; i++) {  //для всех четных чисел, которые мы ввели(дел на два)
            if((num % i) == 0) { // если нет остатка от деления введенного числа на i=2,3,4,5,6,7 - то false
                isPrime = false;
                break; } //закончить цикл если false
        }
        if(isPrime) System.out.println("Prime");
        else System.out.println("Not Prime");
    }
}