package Controlstatements;
//for (int i[] : solA) {
//        System.out.print(Arrays.toString(i) + '\n');

import java.util.Arrays;

/**
 * Created by user on 03.03.16.
 */
// Use for-each style for on a two-dimensional nums.
class ForEach3 {
    public static void main(String args[]) {
        int sum = 0;
        int nums[][] = new int[3][5];
        int z=0;
        // give nums some values
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 5; j++)
//                if ((j == i) ) {
//                    nums[i][j] = 1;
//                }
//                else nums[i][j] = 0;
//                nums[i][j] = (i+1)*(j+1);
                nums[i][j] = z++;
        for (int b[] : nums)
        System.out.println(Arrays.toString(b)+'\n');
        // use for-each for to display and sum the values
        for(int x[] : nums) {
            for(int y : x) {
                System.out.println(Arrays.toString(x)+'\n');
                sum += y;

            } }
        System.out.println("Summation: " + sum);
    }
}
