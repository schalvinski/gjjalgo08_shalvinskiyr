package Controlstatements;

/**
 * Created by user on 21.03.16.
 */
// Using continue with a label.
class ContinueLabel {
    public static void main(String args[]) {
         for (int i=0; i<10; i++) {
            for(int j=0; j<10; j++) {
                if(j > i) {
                    System.out.println();
                    continue ;
                }
                System.out.print(" " + (i * j));
            }
        }
        System.out.println();
    }
}