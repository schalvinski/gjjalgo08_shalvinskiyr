package _My_Apps_.Bank_app_modify;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 07.10.16.
 */

 class Bank{
    private double interestRate = 8.5;
    private double transactionFees = 10;
    private Customer[] customers = new Customer[1000];//содержит количество клиентов array обычно не подходит тк надо знать точное количество клиентов

    private List<Account> accountList = new ArrayList<>();

    public List<Account> getDb() {
      return accountList;
   }
//
    public void setBase(List<Account> employees) {
        this.accountList = employees;
    }

    public void addListToAccountbase() {
        Account employee1 = new Account(100, "Aleksandrovich");
        accountList.add(employee1);
        Account employee2 = new Account(200, "Aleksandrovich2");
        accountList.add(employee2);
    }

    public void calculateInterest(Customer customer){//для кокого клиента какой процент(ссылка на Customer
        Account a = customer.getAccount();
        double bal = a.getBalance();//переменная где баланс = 100
        double interestAmount = bal*interestRate/100; //формула процентов
        double totalBalance = bal+interestAmount;
        System.out.println("Interest amount "+interestAmount+" Total money after adding interest: "+totalBalance);
    }
    public double getInterestRate(){
        return interestRate;
    }

    public double getTransactionFees(){ //хранятся tranasaction fees
        return transactionFees;
    }

    public Customer[] getCustomer(){//здесь хранятся кастомеры,отсюда берут кастомеров
        return customers;
    }
}
