package _My_Apps_.Bank_app_modify;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import static _My_Apps_.Bank_app_modify.Database.searchEmployeesByString;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Database database = new Database();
        database.addListToDatabase();
        System.out.println("Enter stringToSearch");
        String stringToSearch =  new Scanner(System.in).nextLine();;
        System.out.println("Enter intToSearch");
        int intToSearch =  new Scanner(System.in).nextInt();
        System.out.println("Employees have in the name a string " + stringToSearch);
        System.out.println(searchEmployeesByString(database.getDb(), String.valueOf(stringToSearch)));
        System.out.println("Employees with seniority " + intToSearch + " year");
        System.out.println(database.searchEmployeesBySeniority(intToSearch));
    }
}

class Employee extends Database {
    private String firstname;
    private String middlename;
    private String surname;
    private String address;
    private int seniorityYear;
    private int seniorityMonth;

    Employee(String firstname, String middlename, String surname, String address, int seniorityYear, int seniorityMonth) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.surname = surname;
        this.address = address;
        this.seniorityYear = seniorityYear;
        this.seniorityMonth = seniorityMonth;
    }

    Employee(String firstname, String surname, String address, int seniorityYear, int seniorityMonth) {
        this(firstname, null, surname, address, seniorityYear, seniorityMonth);
    }

    public String getFirstname() {
        return firstname;
    }

    @Override
    public String toString() {
        if (middlename != null) {
            return getFirstname() + " " + getMiddlename() + " " + getSurname() + "," + " " + getAddress() + "\n";
        } else {
            return getFirstname() + " " + getSurname() + "," + " " + getAddress() + "\n";
        }
    }

    public String getMiddlename() {
        return middlename;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public int getHireYear() {
        return seniorityYear;
    }

    public int getHireMonth() {
        return seniorityMonth;
    }

    public int getSeniority() {
        int nowYear = Calendar.getInstance().get(Calendar.YEAR);
        int nowMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int yearsWorked;
        if (nowMonth < getHireMonth() && nowYear != getHireYear()) {
            yearsWorked = nowYear - getHireYear() - 1;
        } else {
            yearsWorked = nowYear - getHireYear();
        }
        return yearsWorked;
    }
}

class Database {
    private List<Employee> employees = new ArrayList<>();

    public static String searchEmployeesByString(List<Employee> employee, String string) {
        String result = "";
        for (Employee anEmployee : employee) {
            if (anEmployee.getFirstname().toLowerCase().contains(string.toLowerCase()) || anEmployee.getSurname().toLowerCase().contains(string.toLowerCase())
                    || (anEmployee.getMiddlename() != null && anEmployee.getMiddlename().toLowerCase().contains(string.toLowerCase()) || anEmployee.getAddress().toLowerCase().contains(string.toLowerCase()))) {
                result += anEmployee.toString();
            }
        }
        return result;
    }

    public List<Employee> searchEmployeesBySeniority(int years) {
        List<Employee> result = new ArrayList<>();
        for (Employee anEmployee : getDb()) {
            if (anEmployee.getSeniority() >= years) {
                result.add(anEmployee);
            }
        }
        return result;
    }

    public List<Employee> getDb() {
        return employees;
    }

    public void setBase(List<Employee> employees) {
        this.employees = employees;
    }

    public void addListToDatabase() {
        Employee employee1 = new Employee("Roman", "Aleksandrovich", "Shalvinski", "Moscow, st. Sherbinka, 16", 2014, 12);
        employees.add(employee1);
        Employee employee2 = new Employee("Kirill", "Olegovich", "Petrovich", "Saratov, st. Litovsky, 10", 2006, 6);
        employees.add(employee2);
        Employee employee3 = new Employee("Alexeev", "Alex", "Ivanovich", "Samara, st. Koroleva, 25", 2007, 7);
        employees.add(employee3);
        Employee employee4 = new Employee("Alexanderov", "Alexandr", "Voronezh, st. Perovo, 54", 2004, 2);
        employees.add(employee4);
        Employee employee5 = new Employee("Ivanova", "Elena", "Ivanovna", "Moscow, st. Solyanka, 16", 2011, 12);
        employees.add(employee5);
        Employee employee6 = new Employee("Petrova", "Svetlana", "Moscow, st. Prospect Mira, 121", 1970, 7);
        employees.add(employee6);
        Employee employee7 = new Employee("Alexeeva", "Jhone", "Ivanovna", "Moscow, st. Koroleva, 25", 2007, 7);
        employees.add(employee7);
        Employee employee8 = new Employee("Alexanderova", "Olga", "Romanovna", "st. Perovo, 54", 2004, 2);
        employees.add(employee8);
        Employee employee9 = new Employee("Ivanova", "Marya", "Alexevna", "Moscow, st. Solyanka, 16", 2014, 12);
        employees.add(employee9);
        Employee employee10 = new Employee("Petrova", "Ekaterina", "Moscow, st. Voroncova, 10", 2012, 6);
        employees.add(employee10);
    }
}

