package _My_Apps_.Bank_app_modify;

/**
 * Created by user on 07.10.16.
 */
class Account{
    private double balance = 100;
    private String accountNumber;
    private boolean firstTime = true;

    Account(String acc){
        accountNumber=acc;
    }
    Account(double bal,String acc){
        if (bal>=100){
            balance = bal;
        }else {
            balance=100;
        }
        accountNumber=acc;
    }
    public void deposit(double howMuch){
        if(howMuch>0){
            balance=balance+howMuch;
            System.out.println(howMuch+"was successfully deposited in your account."+" The new balance of your account is "+balance);
        }else {
            System.err.println("Please ensure the amount to be deposited is not negative");//цвет будет красным
        }
    }
    /**
        @param howMuch user how much money to withdraw
        if it is the first time, user is trying to withdraw
        check if money is positive
        if the money is negative, tell the user, they cannot perform the transaction
         */

    public void withdraw(double howMuch){
        if(howMuch>=0){
            if (firstTime==true){
                double tempBalance=balance;
                //let us say your balance = 200, so tempBalance=200;if howMuch=150,
                //then amount remaining = 50 which is not accepteble
                tempBalance=tempBalance-howMuch;
                if (tempBalance>=100)
                {
                    balance=balance-howMuch;
                }
                else {
                    System.err.println("Insufficient balance to remove "+ howMuch);
                }
                firstTime=false;
            }
            else
            {
                Bank bank = new Bank();
                double tempBalance=balance;
                tempBalance=tempBalance-howMuch-bank.getTransactionFees();
                if (tempBalance>=100)
                {
                    balance=balance-howMuch-bank.getTransactionFees();
                }
                else {
                    System.err.println("Insufficient balance to remove "+ howMuch);
                }
            }

        }else {
            System.err.println("Please ensure the amount to be withdrawed is not negative");//цвет будет красным
        }
    }
    public double getBalance(){
        return balance;
    }
    public String getAccountNumber(){
        return accountNumber;
    }
}
