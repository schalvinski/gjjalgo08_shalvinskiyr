package _My_Apps_.Bank_app_modify;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by user on 04.10.16.
 * A customer opens an account in a bank. The customer must have an initial balance of 100 while opening the account, the customer can withdraw,deposit and check account any
 * time
 */
public class ViveBankProgram_my {

    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int numberOfCustomers = 0;
        Bank bank = new Bank();
        bank.addListToAccountbase();
        Customer[] c = bank.getCustomer();//массив customerov вне while loop
        System.out.println("Массив кастомеров "+c);

        while (true) {
            System.out.println("Please enter your choice");
            System.out.println("1. Add Customer");
            System.out.println("2. Deposit money");
            System.out.println("3. Withdraw money");
            System.out.println("4. Check Balance");
            System.out.println("5. Calculate Interest");
            System.out.println("6. Print Clients and their Interest");
            System.out.println("7. Exit");
            int choice = Integer.parseInt(bufferedReader.readLine());
            switch (choice)
            {
                case 1:
                    System.out.println("Creating an account for a new customer: ");
                    System.out.println("Please enter the initial amount in your account: ");
                    double bal = Double.parseDouble(bufferedReader.readLine());
                    System.out.println("Please enter your account number: ");
                    String acc = bufferedReader.readLine();
                    Account account = new Account(bal,acc);//вводишь баланс bal и аккаунт acc для account
                    System.out.println("Please enter your name: ");
                    String name = bufferedReader.readLine();
                    Customer customer = new Customer(name,account);
                    c[numberOfCustomers]=customer;//??? количество customer
                    numberOfCustomers++;
                    System.err.println("NUMBER OF CUSTOMERS "+numberOfCustomers);
                    for (int i = 0;i<numberOfCustomers;i++){
                        System.err.println(c[i].getName()+" NAME");
                    }//распечатываются все имена клиентов
                    break;
                case 2:
                    /**
                     * ask the user to enter the account number, if the number of customer the bank has=0, no money can be deposited, print a message saying
                     * Account number not found, if the number of cusotmers the bank has>0, compare the entered account number with all the accounts of the bank,
                     * if a match is found, ask the person: how much to deposit,
                     */
                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }//
                    else {
                        boolean found = false;
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//for each customer get his own account number
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//проверяет есть ли такой номер аккаунта
                                System.out.println("Please Enter the amount to deposit: ");
                                double money = Double.parseDouble(bufferedReader.readLine());
                                temp.deposit(money);
                                found = true;
                            }
                        }
                        if (found==false){
                            System.err.println("Account Number not found");
                        }
                    }

                    break;
                case 3:

                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }
                    else {
                        boolean found = false;
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//for each customer get his own account number
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//проверяет есть ли такой номер аккаунта
                                System.out.println("Please Enter the amount to withdraw: ");
                                double money = Double.parseDouble(bufferedReader.readLine());
                                temp.withdraw(money);//отличие от deposit (case 2)
                                found = true;
                            }
                        }

                        if (found==false){
                            System.err.println("Account Number not found");
                        }

                    }

                    break;
                case 4:
                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }
                    else {
                        boolean found = false;
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//for each customer get his own account number
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//
                                System.out.println("Balance is: "+temp.getBalance());
                                found = true;
                            }
                        }

                        if (found==false){
                            System.err.println("Account Number not found");
                        }

                    }
                    break;
                case 5:
                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }
                    else {
                        boolean found = false;//наоборот если найдены клиенты
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//создается переменная темп for each customer get his own account number
                            System.out.println(c[i].getAccount());//какие есть аккаунты
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//проверяет есть ли такой номер аккаунта
                                bank.calculateInterest(c[i]);//c[i] == current cusomer
                                found = true;
                            }
                        }

                        if (found==false){
                            System.err.println("Account Number not found");
                        }

                    }
                    break;
                case 6:
                    System.err.println("NUMBER OF CUSTOMERS "+numberOfCustomers);
                    for (int i = 0;i<numberOfCustomers;i++){
                        System.err.println(c[i].getName()+" NAME "+c[i].getAccount()+ " Account Number" + c[i].getAccount().getAccountNumber()+ " Amount "
                        + c[i].getAccount().getBalance() + " balance");//сначал c[i].getAccount()-ссылка на местоп аккаунта
                    }//распечатываются все имена клиентов
                    break;
                case 7:
                    System.exit(0);
                    break;
                default:

                    break;
            }
        }
            }
    }

