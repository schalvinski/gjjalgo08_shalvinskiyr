package _My_Apps_.Bank_app_modify;

/**
 * Created by user on 07.10.16.
 */

 class Customer {
    String name;
    Account account;

    Customer(String n, Account a) {
        name = n;
        account = a;
    }




    public  void display() {
        System.out.println("Name: " + name + ", Account number: " + account.getAccountNumber() + ", Balance: " + account.getBalance());
    }

    public String getName() {
        return name;
    }

    public Account getAccount() { // дает аккаунт каждому кастомеру
        return account;
    }

}