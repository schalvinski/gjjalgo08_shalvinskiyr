package _My_Apps_.Bank_app;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by user on 04.10.16.
 * A customer opens an account in a bank. The customer must have an initial balance of 100 while opening the account, the customer can withdraw,deposit and check account any
 * time
 */
public class ViveBankProgram {

    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int numberOfCustomers = 0;
        Bank bank = new Bank();
        Customer[] c = bank.getCustomer();//массив customerov вне while loop
        while (true) {
            System.out.println("Please enter your choice");
            System.out.println("1. Add Customer");
            System.out.println("2. Deposit money");
            System.out.println("3. Withdraw money");
            System.out.println("4. Check Balance");
            System.out.println("5. Calculate Interest");
            System.out.println("6. Exit");
            int choice = Integer.parseInt(bufferedReader.readLine());
            switch (choice)
            {
                case 1:
                    System.out.println("Creating an account for a new customer: ");
                    System.out.println("Please enter the initial amount in your account: ");
                    double bal = Double.parseDouble(bufferedReader.readLine());
                    System.out.println("Please enter your account number: ");
                    String acc = bufferedReader.readLine();
                    Account account = new Account(bal,acc);
                    System.out.println("Please enter your name: ");
                    String name = bufferedReader.readLine();
                    Customer customer = new Customer(name,account);
                    c[numberOfCustomers]=customer;
                    numberOfCustomers++;
                    System.err.println("NUMBER OF CUSTOMERS "+numberOfCustomers);
                    for (int i = 0; i<numberOfCustomers;i++){
                        System.err.println(c[i].getName()+" NAME");
                    }
                    break;
                case 2:
                    /**
                     * ask the user to enter the account number, if the number of customer the bank has=0, no money can be deposited, print a message saying
                     * Account number not found, if the number of cusotmers the bank has>0, compare the entered account number with all the accounts of the bank,
                     * if a match is found, ask the person: how much to deposit,
                     */
                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }
                    else {
                        boolean found = false;
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//for each customer get his own account number
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//проверяет есть ли такой номер аккаунта
                                System.out.println("Please Enter the amount to deposit: ");
                                double money = Double.parseDouble(bufferedReader.readLine());
                                temp.deposit(money);
                                found = true;
                            }
                        }

                        if (found==false){
                            System.err.println("Account Number not found");
                        }

                    }

                    break;
                case 3:

                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }
                    else {
                        boolean found = false;
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//for each customer get his own account number
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//проверяет есть ли такой номер аккаунта
                                System.out.println("Please Enter the amount to withdraw: ");
                                double money = Double.parseDouble(bufferedReader.readLine());
                                temp.withdraw(money);//отличие от deposit (case 2)
                                found = true;
                            }
                        }

                        if (found==false){
                            System.err.println("Account Number not found");
                        }

                    }

                    break;
                case 4:
                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }
                    else {
                        boolean found = false;
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//for each customer get his own account number
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//
                                System.out.println("Balance is: "+temp.getBalance());
                                found = true;
                            }
                        }

                        if (found==false){
                            System.err.println("Account Number not found");
                        }

                    }
                    break;
                case 5:
                    System.out.println("Enter account number");
                    acc=bufferedReader.readLine();
                    if (numberOfCustomers==0){
                        System.out.println("Account number not found");
                    }
                    else {
                        boolean found = false;
                        for (int i = 0; i < numberOfCustomers; i++) {
                            Account temp = c[i].getAccount();//for each customer get his own account number
                            String accTemp = temp.getAccountNumber();
                            System.out.println(accTemp);
                            if (accTemp.equals(acc)) {//проверяет есть ли такой номер аккаунта
                                bank.calculateInterest(c[i]);//c[i] == current cusomer
                                found = true;
                            }
                        }

                        if (found==false){
                            System.err.println("Account Number not found");
                        }

                    }
                    break;
                case 6:
                    System.exit(0);
                    break;
                default:

                    break;
            }
        }
            }
    static class Bank{
        private double interestRate = 8.5;
        private double transactionFees = 10;
        private Customer[] customers = new Customer[1000];//содержит количество клиентов array обычно не подходит тк надо знать точное количество клиентов
        public void calculateInterest(Customer customer){//для кокого клиента какой процент(ссылка на Customer
            Account a = customer.getAccount();
            double bal = a.getBalance();
            double interestAmount = bal*interestRate/100; //формула процентов
            double totalBalance = bal+interestAmount;
            System.out.println("Interest amount "+interestAmount+" Total money after adding interest: "+totalBalance);
        }
        public double getInterestRate(){
            return interestRate;
        }

        public double getTransactionFees(){
            return transactionFees;
        }

        public Customer[] getCustomer(){//здесь хранятся кастомеры
            return customers;
        }
    }
    static class Account{
        private double balance = 100;
        private String accountNumber;
        private boolean firstTime = true;

        public static void main(String[] args) {
            System.out.println("1");
        }

        Account(String acc){
            accountNumber=acc;
        }
        Account(double bal,String acc){
           if (bal>=100){
               balance = bal;
           }else {
               balance=100;
           }
            accountNumber=acc;
        }
        public void deposit(double howMuch){
            if(howMuch>0){
                balance=balance+howMuch;
                System.out.println(howMuch+"was successfully deposited in your account."+" The new balance of your account is "+balance);
            }else {
                System.err.println("Please ensure the amount to be deposited is not negative");//цвет будет красным
            }
        }
        /*
        @param howMuch user how much money to withdraw
        if it is the first time, user is trying to withdraw
        check if money is positive
        if the money is negative, tell the user, they cannot perform the transaction
         */

        public void withdraw(double howMuch){
            if(howMuch>=0){
                if (firstTime==true){
                    double tempBalance=balance;
                    //let us say your balance = 200, so tempBalance=200;if howMuch=150,
                    //then amount remaining = 50 which is not accepteble
                    tempBalance=tempBalance-howMuch;
                    if (tempBalance>=100)
                    {
                        balance=balance-howMuch;
                    }
                    else {
                        System.err.println("Insufficient balance to remove "+ howMuch);
                    }
                    firstTime=false;
                }
                else
                {
                    Bank bank = new Bank();
                    double tempBalance=balance;
                    tempBalance=tempBalance-howMuch-bank.getTransactionFees();
                    if (tempBalance>=100)
                    {
                        balance=balance-howMuch-bank.getTransactionFees();
                    }
                    else {
                        System.err.println("Insufficient balance to remove "+ howMuch);
                    }
                }

            }else {
                System.err.println("Please ensure the amount to be withdrawed is not negative");//цвет будет красным
            }
        }
        public double getBalance(){
            return balance;
        }
        public String getAccountNumber(){
            return accountNumber;
        }
    }
    static class Customer{
        private String name;
        private Account account;
        Customer(String n,Account a){
            name=n;
            account=a;
        }

        public void display(){
            System.out.println("Name: "+name+", Account number: "+account.getAccountNumber()+", Balance: "+account.getBalance());
        }
        public String getName(){
            return name;
        }
        public Account getAccount(){
            return account;
        }
    }
}
