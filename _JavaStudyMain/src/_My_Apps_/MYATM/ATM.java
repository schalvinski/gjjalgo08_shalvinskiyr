package _My_Apps_.MYATM;

import java.util.Scanner;

/**
 * Created by roman on 02.09.2016.
 */
public class ATM {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Bank theBank = new Bank("Bank of Drausin");
        User aUser = theBank.addUser("John","Doe","1234");//add a user, which also creates savings account
        //add a checking account for our user
        Account newAccount = new Account("Checking",aUser,theBank);
        aUser.addAccount(newAccount);
        theBank.addAccount(newAccount);
        User curUser;
        while (true){
            curUser = ATM.mainMenuPrompt(theBank,sc);//stay in login prompt until success login
            ATM.printUserMenu(curUser,sc);//stay in main menu until user quits
        }
    }
    public static User mainMenuPrompt(Bank theBank, Scanner sc){
        String userID;
        String pin;
        User authUser;

        do{//prompt the user for user ID/pin combo until a correct one is reached
            System.out.printf("\n\nWelcome to %s\n\n",theBank.getName());
            System.out.println("Enter user ID:");
            userID = sc.nextLine();
            System.out.print("Enter pin");
            pin = sc.nextLine();
            //try to get the user object corresponding to ID and pin combo
            authUser = theBank.userLogin(userID,pin);
            if (authUser == null){
                System.out.println("Incorrect user ID/pin combination. Please try again.");
            }
        }while (authUser==null);//continue loooping until succeful login
        return authUser;
    }
    public static void printUserMenu(User theUser, Scanner sc){
        theUser.printAccountsSummary();
        int choice;
        do{
            System.out.printf("Welcome %s,what would you like to do?\n", theUser.getFirstName());//%s - first name
            System.out.println("  1) Show account tranasaction history");
            System.out.println("  2) Withdrawl");
            System.out.println("  3) Deposit");
            System.out.println("  4) Transfer");
            System.out.println("  5) Quit");
            System.out.println();
            System.out.println("Enter choice: ");
            choice = sc.nextInt();
            if(choice<1||choice>5){
                System.out.println("Invalid choice. Please choose 1-5");
            }
        }while(choice<1||choice>5);//keeps looping until the right answer
        switch (choice){
            case 1:
                ATM.showTransHistory(theUser,sc);
                break;
            case 2:
                ATM.withdrawlFunds(theUser,sc);
                break;
            case 3:
                ATM.depositFunds(theUser,sc);
                break;
            case 4:
                ATM.transferFunds(theUser,sc);
                break;
        }

        if (choice != 5){  //redisplay the menu until the user wants to quit
            ATM.printUserMenu(theUser,sc);//recursion
        }
    }
    public static void showTransHistory(User theUser, Scanner sc){
        int theAcct;
        do{
            System.out.printf("Enter the number (1-%d) of the account\n"+"whose transaction you want to see: ", theUser.numAccounts());
            theAcct = sc.nextInt()-1;//because start with zero based indexing
            if (theAcct<0||theAcct>=theUser.numAccounts()){
                System.out.println("Invalid aacount.Please try again.");
            }
        }while(theAcct<0||theAcct>=theUser.numAccounts());
        //print the transaction history
        theUser.printAcctTransHistory(theAcct);
    }
    public static void transferFunds(User theUser, Scanner sc){
        int fromAcct;  //inits
        int toAcct;
        double amount;
        double acctBal;
        do{         //get the account to transfer from
            System.out.printf("Enter the number (1-%d) of the account\n"+"to tranfer from:",theUser.numAccounts());
            fromAcct = sc.nextInt()-1;
            if(fromAcct<0||fromAcct>=theUser.numAccounts()){
                System.out.println("Invalid account.Please try again.");
            }
        }   while (fromAcct<0||fromAcct>=theUser.numAccounts());
       acctBal = theUser.getAcctBalance(fromAcct); //the max how we withdraw
        do{         //get the account to transfer to
            System.out.printf("Enter the number (1-%d) of the account\n"+"to tranfer to:",theUser.numAccounts());
            toAcct = sc.nextInt()-1;
            if(toAcct<0||toAcct>=theUser.numAccounts()){
                System.out.println("Invalid aacount.Please try again.");
            }
        }   while (toAcct<0||toAcct>=theUser.numAccounts());
        do{
            System.out.printf("Enter the amount to transfer(max $%.02f):$",acctBal);//get the amount to transfer
            amount = sc.nextDouble();
            if (amount<0){
                System.out.println("Amount must be greater then zero");
            }else if(amount>acctBal){
                System.out.printf("Amount must not be greater than\n"+"balance of" +
                        " $%.02f.\n",acctBal);
            }
        } while (amount<0||amount>acctBal);
        theUser.addAcctTransaction(fromAcct,-1*amount,String.format("Transfer to account %s", theUser.getAcctUUID(toAcct)));
        theUser.addAcctTransaction(toAcct,amount,String.format("Transfer to account %s", theUser.getAcctUUID(toAcct)));
    }
    public static void withdrawlFunds(User theUser,Scanner sc ){
        int fromAcct;  //inits
        double amount;
        double acctBal;
        String memo;
        do{         //get the account to transfer from
            System.out.printf("Enter the number (1-%d) of the account\n"+"to tranfer " +
                    "from:",theUser.numAccounts());
            fromAcct = sc.nextInt()-1;
            if(fromAcct<0||fromAcct>=theUser.numAccounts()){
                System.out.println("Invalid aacount.Please try again.");
            }
        }   while (fromAcct<0||fromAcct>=theUser.numAccounts());
        acctBal = theUser.getAcctBalance(fromAcct); //the max how we withdraw
        do{
            System.out.printf("Enter the amount to transfer(max $%.02f):$",acctBal);//get the amount to transfer
            amount = sc.nextDouble();
            if (amount<0){
                System.out.println("Amount must be greater then zero");
            }else if(amount>acctBal){
                System.out.printf("Amount must not be greater than\n"+"balance of" +
                        " $%.02f.\n",acctBal);
            }
        } while (amount<0||amount>acctBal);
        sc.nextLine();
        System.out.println("Enter a memo:   ");
        memo = sc.nextLine();
        theUser.addAcctTransaction(fromAcct,-1*amount,memo);//do the withdrawl
    }
    public static void depositFunds(User theUser,Scanner sc){

        int toAcct;  //inits
        double amount;
        double acctBal;
        String memo;
        do{         //get the account to transfer from
            System.out.printf("Enter the number (1-%d) of the account\n"+"to tranfer from:",theUser.numAccounts());
            toAcct = sc.nextInt()-1;
            if(toAcct<0||toAcct>=theUser.numAccounts()){
                System.out.println("Invalid aacount.Please try again.");
            }
        }   while (toAcct<0||toAcct>=theUser.numAccounts());
        acctBal = theUser.getAcctBalance(toAcct); //the max how we withdraw
        do{
            System.out.printf("Enter the amount to transfer(max $%.02f):$",acctBal);//get the amount to transfer
            amount = sc.nextDouble();
            if (amount<0){
                System.out.println("Amount must be greater then zero");}
//            else if(amount>acctBal){
//                System.out.printf("Amount must not be greater than\n"+"balance of" +
//                        " $%.02f.\n",acctBal);
//            }
        } while (amount<0);//||amount>acctBal);
        sc.nextLine();
        System.out.println("Enter a memo:   ");
        memo = sc.nextLine();
        theUser.addAcctTransaction(toAcct,amount,memo);//do the withdrawl
    }
}

