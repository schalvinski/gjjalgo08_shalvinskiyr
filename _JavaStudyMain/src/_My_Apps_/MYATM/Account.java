package _My_Apps_.MYATM;

import java.util.ArrayList;

/**
 * Created by roman on 01.09.2016.
 */
public class Account {
    private String name;
    private String uuid;
    private User holder;

    private ArrayList<Transaction> transactions;

    public Account(String name, User holder, Bank theBank){
        this.name = name;
        this.holder=holder;
        this.uuid = theBank.getNewAccountUUID();
        this.transactions = new ArrayList<Transaction>();
    }
    public String getUUID(){//передает переменную uuid
        return this.uuid;
    }
    public String getSummaryLine(){
        double balance = this.getBalance();//get the account's balance
        if (balance>=0){   //format the summary line, depending on the weather the balance is negative
         return String.format("%s:$%.02f:%s",this.uuid,balance,this.name);//округление до 2ух знаков полсе запятой
        }else{
            return String.format("%s:$%(.02f):%s",this.uuid,balance,this.name);//округление до 2ух знаков полсе запятой
        }
    }
    public double getBalance(){//looping through all the transactions getting the amount
        double balance = 0;
        for (Transaction t: this.transactions){
            balance+=t.getAmount();
        }
        return balance;
    }
    public void printTransHistory(){
        System.out.printf("\nTransaction history for account %s\n", this.uuid);
        for (int t = this.transactions.size()-1;t>=0;t--){
            System.out.printf(this.transactions.get(t).getSummaryLine());
        }
        System.out.println();
    }
    public void addTransaction(double amount,String memo){  //create new transaction object and add it ot our list
        Transaction newTrans = new Transaction(amount,memo,this);
        this.transactions.add(newTrans);
    }
}
