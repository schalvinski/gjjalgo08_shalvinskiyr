package _My_Apps_.MYATM;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by roman on 01.09.2016.
 */
public class Bank {
    private String name;
    private ArrayList<User> users;
    private ArrayList<Account> accounts;

    public Bank(String name){//Create a new Bank object with empty lists of users and accounts
        this.name = name;
        this.users = new ArrayList<User>();//ниже два метода будут добавлять новых user и accounts
        this.accounts = new ArrayList<Account>();
    }
    public String getNewAccountUUID() {//content the same as for getNewUserUUID()
        //inits
        String uuid;
        Random rng = new Random();
        int len = 8;
        boolean nonUnique;
        do{                 // continue looping until we get a unique ID
            uuid = "";
            for (int c =0 ;c<len;c++){
                uuid+=((Integer)rng.nextInt(10)).toString();//генерирует числа от 0 до 9
            }
            nonUnique = false;
            for (Account a: this.accounts){
                if (uuid.compareTo(a.getUUID()) == 0){//check to make sure it's unique
                    nonUnique=true;
                    break;
                }
            }
        }while (nonUnique);
        return uuid;
    }
    public String getNewUserUUID() {
        //inits
        String uuid;
        Random rng = new Random();
        int len = 6;
        boolean nonUnique;
        do{                 // continue looping until we get a unique ID
            uuid = "";
            for (int c =0 ;c<len;c++){
                uuid+=((Integer)rng.nextInt(10)).toString();//генерирует числа от 0 до 9
            }
            nonUnique = false;
            for (User u: this.users){
                if (uuid.compareTo(u.getUUID()) == 0){//check to make sure it's unique
                nonUnique=true;
                break;
                }
            }
        }while (nonUnique);
        return uuid;
    }
    public void addAccount(Account anAcct) {//инкапсуляция
        this.accounts.add(anAcct);
}
    public User addUser(String firstName,String lastName, String pin){//переменные из User, но нет Bank theBank
    //create a new User object and add it to our list
    User newUser = new User(firstName,lastName,pin,this);//this - object of theBank
    this.users.add(newUser);
    //creat a savings aacount for the user
    Account newAccount = new Account("Savings", newUser,this);//
    this.addAccount(newAccount);
    newUser.addAccount(newAccount);

    return newUser;
    }
    public User userLogin(String userID, String pin){//create a user
        //search through list of users
        for (User u: this.users){
            if (u.getUUID().compareTo(userID) == 0 && u.validatePin(pin)) {
                return u;
            }
        }
        return null;
    }
    public String getName(){
        return this.name;
    }
}
