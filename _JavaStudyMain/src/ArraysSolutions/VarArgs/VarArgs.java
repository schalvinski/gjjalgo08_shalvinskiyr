package ArraysSolutions.VarArgs;

// Демонстрация использования списка аргументов переменной длины.
class VarArgs {
	// Теперь vaTest() использует список аргументов переменной длины.
	static void vaTest(int... v) {//тоже что и массив int[] v
		System.out.println("Количество аргументов: " + v.length + " Содержимое: ");
		for (int x : v) {
			System.out.println(x + " ");
		}
		System.out.println();
	}

	public static void main(String args[]) {
		// Обратите внимание на возможные способы вызова
		// vaTest() с переменным количеством аргументов.
		vaTest(10); // 1 аргумент
		vaTest(1, 2, 3); // 3 аргумента
		vaTest(); // без аргументов
	}
}
