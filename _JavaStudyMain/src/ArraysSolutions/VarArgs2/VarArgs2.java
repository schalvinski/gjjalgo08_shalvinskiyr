package ArraysSolutions.VarArgs2;

// Использование списка аргументов переменной длины совместно
// со стандартными аргументами.
class VarArgs2 {
	// В этом примере msg - обычный параметр,
	// а v - параметр vararg.
	static void vaTest(String msg, int ... v) {
		System.out.println(msg + v.length + " Содержимое: ");

		for (int x : v) {
			System.out.println(x + " ");
		}
		System.out.println();
	}

	public static void main(String args[]) {
		vaTest("Один параметр vararg: ", 10);
		vaTest("Три параметра vararg: ", 1, 2, 3);
		vaTest("Без параметров vararg: ");
	}
}
