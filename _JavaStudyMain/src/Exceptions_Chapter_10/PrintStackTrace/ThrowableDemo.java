package Exceptions_Chapter_10.PrintStackTrace;

/**
 * Created by user on 05.02.17.
 */
public class ThrowableDemo {

    public static void main(String[] args) {

        try {
            ExceptionFunc();
        }
        catch(Throwable e) {
            // prints stacktace for this Throwable Object
            e.printStackTrace();
        }
    }

    public static void ExceptionFunc() throws Throwable {

        Throwable t = new Throwable("This is new Exception...");
        StackTraceElement[] trace = new StackTraceElement[] {
                new StackTraceElement("ClassName","methodName","fileName",5)
        };

        // sets the stack trace elements
        t.setStackTrace(trace);
        throw t;
    }
}
