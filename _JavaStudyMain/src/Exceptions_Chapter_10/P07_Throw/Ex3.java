package Exceptions_Chapter_10.P07_Throw;

import java.util.Scanner;


public class Ex3 {

    public static String nTimes(int n,String name) throws Exception {
        if (n<0){
            throw new Exception("n must be a positive number n="+n);//пропускает весь код внизу до ближ catch
        }
        String result = "";
        for (int i=0;i<n;i++) {
            result += name;
        }
        return result;
    }
    public static void main(String[] args) {
        try {
            System.out.println(nTimes(-3,"YOU!"));//)catch можно удалить но будет ошибка
        } catch (Exception e2) {
            System.out.println("Fuck wrong");
            System.out.println(e2);//)печатает твою версию exception
        }
        //  System.out.println(nTimes(3,"YOU!")); тк throw не сочетается без try catch
        System.out.printf("Enter a number");
        try {
            int x =new Scanner(System.in).nextInt();
            System.out.println("You entered "+x);
        } catch (Exception e) {
            System.out.println("That is not a number.");//если ввести текст вместо числа
            // пропускает все что в трай вызывает
            //ошибку и сразу переходит к первой строке в кетч
            //          e.printStackTrace();
        }
    }



}
