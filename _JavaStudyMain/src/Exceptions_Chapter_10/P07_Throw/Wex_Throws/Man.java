package Exceptions_Chapter_10.P07_Throw.Wex_Throws;

//8?

/**
 * Created by user on 04.10.16.
 */
public class Man {

    String name;
    int weight;

    Man(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public static void main(String[] args) {
        String stroka = "Общий вес не должен превышать 200 кг";
        Man man1 = new Man("Олег0", 240);
        Man man2 = new Man("Олег1", 140);
        Man man3 = new Man("Олег2", 87);
        try {
            useLift(man1, man2, man3);
        } catch (Wex e) {
            System.out.println(e.getMessage());
            System.out.println(stroka);
        }
        try {
            useLift(man1, man2);
        } catch (Wex e) {
            System.out.println(e.getMessage());
            System.out.println(stroka);
        }
        try {
            useLift(man2, man3);
        } catch (Wex e) {
            System.out.println(e.getMessage());
            System.out.println(stroka);
        }
        System.out.println("");

    }

    public static void useLift(Man... mans) throws Wex {//в случае sum > 200 выполняется Wex
        int sum = 0;
        for (Man man : mans) {//для каждого mans в классе Man, man явл представителем
            sum = sum + man.weight;
        }
        if (sum > 200) {
            throw new Wex();
        } else {
            System.out.println("Lift poexal");
        }
    }
}
