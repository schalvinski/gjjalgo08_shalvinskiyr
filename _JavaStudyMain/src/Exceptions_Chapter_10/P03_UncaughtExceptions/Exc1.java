package Exceptions_Chapter_10.P03_UncaughtExceptions;
//что будет если не испольжзовать Exceptions
public class Exc1 {
     static void subroutine() {
        int d = 0;
        int a = 10 / d;
    }
    
    public static void main(String args[])   {


        try {
            Exc1.subroutine();
        }catch (Exception e){
            System.out.println(e +  "   На ноль делить нельзя.");
        }finally {
            System.out.println("Продолжение работы");
        }
    }
}
