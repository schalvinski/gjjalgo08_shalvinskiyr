package Exceptions_Chapter_10.P08_Throws;

import java.io.IOException;

class Testthrows1 {
    void m() throws IOException {
        throw new IOException("device error");//checked exception
    }

    void p() {
        try {
            throw new IOException("device error");
        } catch (Exception e) {
            System.out.println("exception handled");
        }
    }
    public static void main(String args[]) {
        Testthrows1 testthrows1 = new Testthrows1();
        testthrows1.p();
//        testthrows1.m(); если добавим, то в классе надо добавить проверку на Throws
        System.out.println("normal flow...");
    }
}
