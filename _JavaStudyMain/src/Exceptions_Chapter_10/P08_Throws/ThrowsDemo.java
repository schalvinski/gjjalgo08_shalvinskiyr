package Exceptions_Chapter_10.P08_Throws;

public class ThrowsDemo {
    static void throwOne() throws IllegalArgumentException, IllegalAccessException {
        System.out.println("Inside throwOne");
        throw new IllegalAccessException("demo");
    }
    
    public static void main(String args[]) {
        try {
            throwOne();
        } catch(IllegalAccessException e) {
            System.out.println("Caught: " + e);
        }
    }

    /*    public static void main(String args[]) throws IllegalAccessException {
        throwOne();
    }*/
}
