package Exceptions_Chapter_10.ConcurrentModificationException;

/**
 * Created by user on 08.12.16.
 */
import java.util.ArrayList;

import java.util.Iterator;

import java.util.List;



public class Example_v3 {



    public static void main(String[] args) {

        List<String> list = new ArrayList<String>();

        // Insert some sample values.

        list.add("Value1");

        list.add("Value2");

        list.add("Value3");

        // Get two iterators.

        Iterator<String> ite = list.iterator();

        Iterator<String> ite2 = list.iterator();

        // Point to the first object of the list and then, remove it.

        ite.next();
        ite.remove();


          /* The second iterator tries to remove the first object as well. The object does
           * not exist and thus, a ConcurrentModificationException is thrown.
           *
           * когда количество в итераторе не совпадает с фактическим количеством
           * */

        ite2.next();

        ite2.remove();

    }
}
