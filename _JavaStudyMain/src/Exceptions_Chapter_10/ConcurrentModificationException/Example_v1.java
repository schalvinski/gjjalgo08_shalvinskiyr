package Exceptions_Chapter_10.ConcurrentModificationException;

/**
 * Created by user on 08.12.16.
 */
import java.util.HashMap;
import java.util.Map;

public class Example_v1 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        // Insert some sample key-value pairs.
        map.put("Key1", 1);
        map.put("Key2", 2);
        map.put("Key3", 3);
          /* Remove a value of the map, while iterating over it.
           * The following code throws a java.util.ConcurrentModificationException.
           * если удалить элемент во время итерирования, то вызывется exception
           * */
        for(String key: map.keySet()) {
            if(map.get(key) == 1)
            map.remove(key);
        }
        System.out.println("Successfully removed a pair!");
    }
}