package Exceptions_Chapter_10.ConcurrentModificationException;

import java.util.*;

public class TestExample {

    public static void main(String[] args) {

        List<String> list = new ArrayList<String>(Arrays.asList("Nick","Kolia","Rich"));

        System.out.println("Original list content: "+list);

        ListIterator<String> itr = list.listIterator();

        while(itr.hasNext())
        {
            String s = itr.next();
//            if(s.equals("one"))
//                itr.remove();

            if(s.equals("Nick"))
                itr.add("four");
        }
        System.out.println("List content after operations:  "+list);

    }

}