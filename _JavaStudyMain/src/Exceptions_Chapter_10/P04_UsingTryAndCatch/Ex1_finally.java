package Exceptions_Chapter_10.P04_UsingTryAndCatch;

/**
 * Created by roman on 01.08.2016.
 */
public class Ex1_finally  {
    public static void main(String[] args) {
        int a =0;
        String stroka = "a=0, делить на 0 нельзя";
        String strokacount = "Продолжение работы с=";
        int b = 7;
        int c = 0;
        try{
            c=b/a;
        }catch (Exception e){
            System.out.println(e);
            System.out.println(stroka);//)если наступает трай, то выводится строка вместо  ошибки
            System.out.println("Except "+e);
            c=b;
        }
        finally {//)какая в итоге команда выполнится
            System.out.println(strokacount+c);
        }

    }
}
