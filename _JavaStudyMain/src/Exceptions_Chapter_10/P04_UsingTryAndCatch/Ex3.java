package Exceptions_Chapter_10.P04_UsingTryAndCatch;

import java.util.Scanner;


public class Ex3 {

    public static void main(String[] args) {
        try {
            int x = new Scanner(System.in).nextInt();
            System.out.println("You entered " + x);
        } catch (Exception e) {
            System.out.println(e);//сам выбирает ошибку
            System.out.println("That is not a number.");//пропускает все что в трай вызывает
            //ошибку и сразу переходит к первой строке в кетч
            //          e.printStackTrace();
        }
    }
}
