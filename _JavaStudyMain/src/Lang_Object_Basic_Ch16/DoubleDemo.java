// Double type wrapper

package Lang_Object_Basic_Ch16;

class DoubleDemo0 {
  public static void main(String args[]) {
    Double d1 = new Double(3.14159);
    Double d2 = new Double("314159E-5");//два типа записей
    
    System.out.println(d1 + " = " + d2 + " -> " + d1.equals(d2));
  }
}
