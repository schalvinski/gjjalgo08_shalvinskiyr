// using arraycopy().

package Lang_Object_Basic_Ch16;

class ACDemo_arraycopy {
  static byte a[] = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
  static byte b[] = { 77, 77, 77, 77, 77, 77, 77, 77, 77, 77 };
  static int [] fu = new int [] { 1, 2, 3, 0, 0, 07, 0, 0, 77, 77 };
  static int fu2[] = { 4, 4, 4, 5, 77, 77, 77, 77, 77, 77 };

  public static void main(String args[]) {
    System.out.println("a = " + new String(a));
    System.out.println("b = " + new String(b));
    
    System.arraycopy(a, 0, b, 0, a.length);
    
    System.out.println("a = " + new String(a));
    System.out.println("b = " + new String(b));
    
    System.arraycopy(a, 0, a, 1, a.length - 1);
    System.arraycopy(b, 1, b, 0, b.length - 1);
    
    System.out.println("a = " + new String(a));
    System.out.println("b = " + new String(b));

    System.arraycopy(fu,0,fu2,0,5);//изменился только fu2 на длину 5

    for (int i = 0; i < fu2.length; i++)
    {
      System.out.println("c =" + fu2[i]);
    }
  }
}   
