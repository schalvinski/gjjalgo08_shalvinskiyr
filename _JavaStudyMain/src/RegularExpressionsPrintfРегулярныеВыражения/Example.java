package RegularExpressionsPrintfРегулярныеВыражения;

/**
 * Created by user on 27.12.16.
 */
public class Example {
    public static void main(String[] args) {

        System.out.println(find(new int[]{1, 2, 3,4}, 2, 4, 3));
    }

    static int find(int[] a, int begin, int end, int key) {
        if (begin == end) return begin;
        int middle = (begin + end) / 2;
        if (key < a[middle]) {
            end = middle;
            return end;
        } else {
            begin = middle;
            return begin;
        }
    }
}
