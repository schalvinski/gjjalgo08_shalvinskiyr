package RegularExpressionsPrintfРегулярныеВыражения.ncedu.java.examples.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kolesnikov
 * quantifiers
 */
public class TestRegexp4 {

    public static void main(String[] agrs) {
        String sourceStr = "<p><i>Регулярные выражения</i> - <b>java</b>." +
                "<br> Ленивая и жадная квантификации.</p>";

        System.out.println("Ленивая политика: ");
        applyPattern("<.*?>", sourceStr);

        System.out.println("\nЖадная политика: ");
        applyPattern("<.*>", sourceStr);

        System.out.println("\nСверхжадная политика: ");
        applyPattern("<.*+>", sourceStr);

    }

    private static void applyPattern(String pattern, String source) {
        Pattern p2 = Pattern.compile(pattern);
        Matcher m2 = p2.matcher(source);
        while (m2.find())
            System.out.println(m2.group());
    }

}
