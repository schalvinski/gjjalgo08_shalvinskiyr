package RegularExpressionsPrintfРегулярныеВыражения.ncedu.java.examples.regexp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kolesnikov
 *         number, time, e-mail regexps
 */
public class TestRegexp0 {

    public static void main(String[] agrs) {
        String numberPattern = "[+-]?[0-9]+|0[Xx][0-9A-Fa-f]+";
        String timePattern = "1?[0-9]:[0-5][0-9][ap]m";
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        System.out.println("Number: ");
        System.out.println(applyPattern(numberPattern));

        System.out.println("Time:");
        System.out.println(applyPattern(timePattern));

        System.out.println("E-mail:");
        System.out.println(applyPattern(emailPattern));
    }

    private static boolean applyPattern(String pattern) {
        try {
            String sourceString = new BufferedReader(new InputStreamReader(System.in)).readLine();
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(sourceString);
            return m.matches();
        } catch (IOException e) {
            return false;
        }
    }

}
