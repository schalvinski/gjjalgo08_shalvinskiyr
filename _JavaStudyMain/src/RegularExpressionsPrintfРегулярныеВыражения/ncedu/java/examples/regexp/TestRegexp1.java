package RegularExpressionsPrintfРегулярныеВыражения.ncedu.java.examples.regexp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author kolesnikov
 *         IP address regexp
 */
public class TestRegexp1 {

    public static void main(String[] agrs) {
        try {
            System.out.println("Enter string:");
            String inputString = new BufferedReader(new InputStreamReader(System.in)).readLine();

            System.out.println("Result: ");

            //IP Address
            System.out.println(inputString.matches("\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}"));

            //IP Address Fixed
            System.out.println(inputString.matches(
                    "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"));

        } catch (IOException e) {
            System.out.println("Exception: " + e.toString());
        }

    }

}
