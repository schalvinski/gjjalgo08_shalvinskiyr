package RegularExpressionsPrintfРегулярныеВыражения.ncedu.java.examples.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kolesnikov
 *         phone number regexp
 */
public class TestRegexp3 {

    public static void main(String[] agrs) {

        String sourceString = "John’s phone number is 732-123-2-23, Maria’s one is 34-3232-1-1-2";

        Pattern p = Pattern.compile("((\\d+\\-?)*\\d+)");
        Matcher m = p.matcher(sourceString);

        while (m.find()) {
            System.out.println(m.group());
        }

        m.reset();
        System.out.println(m.replaceAll("[skypecall:$1]"));

    }

}
