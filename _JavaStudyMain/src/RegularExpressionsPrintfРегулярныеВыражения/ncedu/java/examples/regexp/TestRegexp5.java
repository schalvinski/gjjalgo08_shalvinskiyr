package RegularExpressionsPrintfРегулярныеВыражения.ncedu.java.examples.regexp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * @author kolesnikov
 */
public class TestRegexp5 {
    public static void main(String[] agrs) {
        try {
            System.out.println("Enter string:");
            String inputString = new BufferedReader(new InputStreamReader(System.in)).readLine();

            System.out.println("Split:");
            String[] splitted = inputString.split("-");
            for (String str : splitted)
                System.out.println(str);

            System.out.println("Enter string:");
            inputString = new BufferedReader(new InputStreamReader(System.in)).readLine();

            System.out.println("\nStringTokenizer:");
            StringTokenizer st = new StringTokenizer(inputString, "|");
            while (st.hasMoreTokens())
                System.out.println(st.nextToken());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}