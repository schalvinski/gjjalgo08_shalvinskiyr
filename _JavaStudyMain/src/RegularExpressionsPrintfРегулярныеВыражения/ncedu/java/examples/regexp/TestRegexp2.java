package RegularExpressionsPrintfРегулярныеВыражения.ncedu.java.examples.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kolesnikov
 *         groups regexp
 */
public class TestRegexp2 {

    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("(.*?) ([A-Z][a-z]*), ([\\d]*?),\\s(.*)");
        Matcher matcher = pattern.matcher("Ivanov Pavel, 25, Moscow");

        if (matcher.matches()) {
            System.out.println("First name: " + matcher.group(1));
            System.out.println("Last name: " + matcher.group(2));
            System.out.println("Age: " + matcher.group(3));
            System.out.println("City: " + matcher.group(4));
        }

    }
}
