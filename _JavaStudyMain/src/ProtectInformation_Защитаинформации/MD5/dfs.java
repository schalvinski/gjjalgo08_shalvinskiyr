package ProtectInformation_Защитаинформации.MD5;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by user on 06.02.17.
 */
public class dfs {
    public static void main(String[] args) {
        System.out.println(md5("asad"));

    }
    public static String md5(String input) { // статичный метод md5()
        String md5 = null;
        if(null == input) // если переменна input равна null
            return null; //ничего не возвращает
        try //
        {
            MessageDigest digest = MessageDigest.getInstance("MD5"); //получение хэш-суммы текста с использованием алгоритма MD5
            digest.update(input.getBytes(), 0, input.length());//Обновления дайджеста с помощью указанного массива байтов
            md5 = new BigInteger(1, digest.digest()).toString(16);//
            System.out.println(md5);
        }
        catch (NoSuchAlgorithmException e)  //перехват исключения
        {
            e.printStackTrace();
        }
        return md5; //возвращает значение переменной md5
    }
}
