package SWINGJFC.JavaGui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by user on 22.10.16.
 */

public class jtable extends JFrame {

    JTable jTable;

    public static void main(String[] args) {
        jtable gui = new jtable();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setSize(400, 400);
        gui.setVisible(true);
        gui.setTitle("My First Java Table");
    }

    public jtable() {//конструктор

        String[] columnNames = {"First Name", "Last Name",
                "Sport",
                "# of Years",
                "Vegetarian"};
        Object[][] data = {
                {"Kathy", "Smith", "Snowboarding", new Integer(5), new Boolean(false)},
                {"John", "Doe", "Rowing", new Integer(3), new Boolean(true)},
                {"Sue", "Black", "Knitting", new Integer(2), new Boolean(false)},
                {"Jane", "White", "Speed reading", new Integer(20), new Boolean(true)},
                {"Joe", "Brown", "Pool", new Integer(10), new Boolean(false)}
        };
        jTable = new JTable(data, columnNames);
        jTable.setPreferredScrollableViewportSize(new Dimension(400, 100));
        jTable.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(jTable);
        add(scrollPane);
    }


}
