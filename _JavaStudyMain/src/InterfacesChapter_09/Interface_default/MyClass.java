package InterfacesChapter_09.Interface_default;

/**
 * Created by user on 28.05.17.
 */
public class MyClass implements InterfaceA, InterfaceB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.sayHi();
        myClass.saySomething();
    }


    @Override
    public void saySomething() {
        System.out.println("Hello World");
    }

    @Override
    public void sayHi() {
        InterfaceA.super.sayHi();
//        sayHi();
    }

}

interface InterfaceA {

    public void saySomething();

    default public void sayHi() {
        System.out.println("Hi from InterfaceA");
    }

}

interface InterfaceB {

    default void sayHi() {
        System.out.println("Hi from InterfaceB");
    }
}