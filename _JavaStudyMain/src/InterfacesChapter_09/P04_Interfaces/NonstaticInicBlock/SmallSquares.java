package InterfacesChapter_09.P04_Interfaces.NonstaticInicBlock;

/**
 * Created by user on 20.11.16.
 */
//вызов в статитстическом поле, а не в конструкторе
public class SmallSquares {

    private static final int LIMIT = 10;
    private static final int[] square = new int[LIMIT];

    static {
        for (int i = 0; i < LIMIT; i++) {
            square[i] = i * i;
        }
    }

    public static int getSquare(int i) {
        // Нет обработки ошибки, предположим,0<=i<limit
        return square[i];
    }

    public static void main(String[] args) {
        System.out.println("3 squared is " + getSquare(3));
    }
}