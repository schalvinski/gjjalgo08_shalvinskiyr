package InterfacesChapter_09.P04_Interfaces.NonstaticInicBlock;

public class Foo {

    private static final int LIMIT = 10;
    private static final int[] square = new int[LIMIT];

    public Foo() { // не пишите такой код
        for (int i = 0; i < LIMIT; i++) {
            square[i] = i * i;
        }
    }

    public static int getSquare(int i) {
        // Нет обработки ошибки, предположим, 0<=i<limit
        return square[i];
    }

    public static void main(String[] args) {
        new Foo();
        System.out.println("3 squared is " +
                getSquare(3));
    }
}