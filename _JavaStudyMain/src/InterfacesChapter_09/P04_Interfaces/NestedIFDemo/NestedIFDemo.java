// Пример вложенного интерфейса.
package InterfacesChapter_09.P04_Interfaces.NestedIFDemo;
// Этот класс содержит интерфейс-член.
class A {
	// это вложенный интерфейс
	public interface NestedIF {
		boolean isNotNegative(int x);//)нет тела метода
	}
}

// Класс B реализует вложенный интерфейс.
class B implements A.NestedIF {
	public boolean isNotNegative(int x) {
		return x < 0 ? false : true; // boolean возвращает либо true либо false
	}//)есть тело метода
}
@Deprecated

class NestedIFDemo {
	public static void main(String args[]) {
		// использует ссылку на вложенный интерфейс
		B nif = new B();

		if (nif.isNotNegative(10)) {
			System.out.println("10 не является отрицательным");
		}

		if (nif.isNotNegative(-12)) {
			System.out.println("это не будет отображаться");
		}
	}
}
