package static_P07_UnderstandingStatic.staticBlock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 20.11.16.
 */

public class MovieData {
    public static List<String> titles = new ArrayList<>(),
            directors = new ArrayList<>();
    static {
        add("Jurassic Park", "Steven Spielberg");
        add("The Dark Night", "Christopher Nolan");
        add("Titanic", "James Cameron");
    }
    private static void add(String title, String director) {
        titles.add(title);
        directors.add(director);
    }
    public static void main(String[] args) {
        System.out.println(titles);
        System.out.println(directors);
    }
}