package static_P07_UnderstandingStatic;

// Demostrate static variables, methods, and blocks.
public class UseStatic {
    static int a = 3;
    static int b;
    
    static void meth(int x) {
        System.out.println("x = " + x);
        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }

    //выполняется первым, не требует вызов метрда
    static {
        System.out.println("Static block initialized.");
        b = a * 4;
    }
    void pring(String a){
        System.out.println(a);
    }
    
    public static void main(String args[]) {
        meth(42);
        UseStatic aStatic = new UseStatic();
        aStatic.meth(42);//можно не через инстанс а напрямую
        aStatic.pring("123");
        UseStatic.meth(24);
    }
}
