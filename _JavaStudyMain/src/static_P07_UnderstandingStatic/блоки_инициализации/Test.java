package static_P07_UnderstandingStatic.блоки_инициализации;

/**
 * Created by user on 20.11.16.
 */
public class Test {
    public static void main(String[] args) {
        new Test();
    }

    static {
        System.out.println("Static");
    }

    {
        System.out.println("Non-static block");
    }

    public Test() {
        System.out.println("Constructor");
    }
}

/*
Поля с модификатором static могут быть использованы даже если не было создано не одного экземпляра класса
(ни одного объекта) - и они являются общими для всех экземпляров класса.

static методы соответственно могут взаимодействовать только со static полями.

В конструкторе вы можете задавать значения переменным инстанса(объекта). Под каждый инстанс выделяется
отдельная память в которой и хранятся переменные инстанса, и они свои для каждого инстанса.

Кроме static блока инициализации если еще не статик блок инициализации - он выполняется до конструктора, например:
 */