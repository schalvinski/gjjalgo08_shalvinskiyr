package Ch13;

/**
 * Created by user on 18.11.16.
 */
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import java.util.Hashtable;

public class Cont {
    public static void main(String[] args) throws Exception {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        env.put(Context.PROVIDER_URL, "file:/tmp/marketing");

        Object item = null;

        Context initCtx = new InitialContext(env);
        NamingEnumeration nl = initCtx.list("reports");

        if (nl == null)
            System.out.println("\nNo items in name list");
        else
            while (nl.hasMore()) {
                item = nl.next();
                System.out.println("item's class is " + item.getClass().getName());
                System.out.println(item);
                System.out.println("");
            }
    }
}
