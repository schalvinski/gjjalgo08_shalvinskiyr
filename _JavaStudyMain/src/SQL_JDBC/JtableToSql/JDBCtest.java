package SQL_JDBC.JtableToSql;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by user on 22.10.16.
 */
public class JDBCtest {
    public static void main(String[] args) {



        List<Product> pr = new ArrayList<>();
        pr.add(new Product(1,"Первый продукт",Calendar.getInstance().getTime()));
        pr.add(new Product(2,"Второй продукт",Calendar.getInstance().getTime()));
        pr.add(new Product(3,"Третий продукт",Calendar.getInstance().getTime()));
        pr.add(new Product(4,"Четвертый продукт",Calendar.getInstance().getTime()));
        pr.add(new Product(5,"Пятый продукт",Calendar.getInstance().getTime()));


        ProductDAO dao = new ProductDAO();

        dao.removeAll();
        dao.fillProducts(pr);
        dao.printTable();

    }

}
