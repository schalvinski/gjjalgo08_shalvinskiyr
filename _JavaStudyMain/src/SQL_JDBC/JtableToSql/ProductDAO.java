package SQL_JDBC.JtableToSql;

import java.sql.*;
import java.util.List;

/**
 * Created by user on 29.10.16.
 */
public class ProductDAO {

    public void fillProducts(List<Product> pr) {
        Connection conn;
        PreparedStatement ps;
        try {
            conn = DBUtil.getConnection();
            ps = conn.prepareStatement("insert into product values(?,?,?)");
            for (Product p : pr) {
                ps.setInt(1, p.getNumber());
                ps.setString(2, p.getName());
                ps.setTimestamp(3, new Timestamp(p.getDate().getTime()));
                ps.executeUpdate();
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }
    }

    public void removeAll() {
        Connection conn;
        PreparedStatement ps;
        try {
            conn = DBUtil.getConnection();

            ps = conn.prepareStatement("delete from product");//

            ps.executeUpdate();

        } catch (SQLException exc) {
            exc.printStackTrace();
        }
    }
    public void printTable(){
        Connection conn;
        Statement ps = null;
        ResultSet res = null;
//        PreparedStatement ps;
        try {
            conn = DBUtil.getConnection();

//            ps = conn.prepareStatement("select*from product");//
            ps = conn.createStatement();
            res = ps.executeQuery("select*from product");



            while (res.next()) {
                int id = res.getInt(1);
                System.out.println("res    " + res.getString("id") + " " + res.getString(2) + "  " + id);

            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }
    }

}
