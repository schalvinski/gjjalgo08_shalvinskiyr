package SQL_JDBC.JtableToSql;

import java.sql.*;
import java.util.Properties;

/**
 * Created by user on 22.1  0.16.
 */
public class DBUtil {

    public static  final String URL ="jdbc:postgresql://localhost:5432/postgres";
    public static  final String USER ="postgres";
    public static  final String PASSWORD ="123";

    public static Connection getConnection() throws SQLException {
        Properties p = new Properties();
        p.setProperty("user", USER);
        p.setProperty("password", PASSWORD);
        Connection conn = DriverManager.getConnection(URL, p);
        return conn;
    }

//    public static void closeAll(ResultSet rs, Statement s, Connection c) {
//        try {
//            if (rs != null) rs.close();
//            if (s != null) s.close();
//            if (c != null) c.close();
//        } catch (SQLException ex) {
//
//        }
//    }
}
