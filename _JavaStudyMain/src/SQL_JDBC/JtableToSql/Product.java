package SQL_JDBC.JtableToSql;

import java.util.Date;

/**
 * Created by user on 29.10.16.
 */
public class Product {

    int number;
    String name;
    private Date date;


    public Product(int number, String name, Date date) {
        this.number=number;
        this.name=name;
        this.date=date;
    }

    public int getNumber(){
        return number;
    }

    public String getName(){
        return name;
    }

    public Date getDate(){
        return date;
    }

    public void setName(String name){
        this.name=name;
    }
    public void setDate(Date date){
        this.date=date;
    }


//
}
