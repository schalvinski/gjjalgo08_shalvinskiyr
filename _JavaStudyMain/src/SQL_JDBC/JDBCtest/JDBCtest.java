package SQL_JDBC.JDBCtest;


import java.sql.*;

/**
 * Created by user on 22.10.16.
 */
public class JDBCtest {
    public static void main(String[] args) {
        Connection conn = null;
        Statement s = null;
        ResultSet res = null;
        try {
            conn = DBUtil.getConnection();
            System.out.println(conn.getMetaData().getDatabaseProductName());
            System.out.println(conn.getMetaData().getDatabaseProductVersion());


            DatabaseMetaData meta = conn.getMetaData();
            s = conn.createStatement();
            res = s.executeQuery("select*from prod");

            while (res.next()) {
                int id = res.getInt(1);
                System.out.println("res    " + res.getString("id") + " " + res.getString(2) + "  " + id);

            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        } finally {
            DBUtil.closeAll(res, s, conn);
        }
    }
}
