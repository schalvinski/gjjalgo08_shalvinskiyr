package SQL_JDBC;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by user on 12.11.16.
 */

public class DBUtil {
    public static Connection getConnection(){
        Connection connection = null;
        try {
            InitialContext context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup("jdbc/Product");
            connection = dataSource.getConnection();
        }catch(NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
    }
        return connection;
    }

}