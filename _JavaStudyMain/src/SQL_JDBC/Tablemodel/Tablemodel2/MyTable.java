package SQL_JDBC.Tablemodel.Tablemodel2;

/**
 * Created by user on 05.11.16.
 */
import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MyTable {
    public static void main(String[] argv) throws Exception {
        JTable table = new JTable(myModel());

        JFrame f = new JFrame();
        f.setSize(300, 300);
        f.add(new JScrollPane(table));
        WindowListener wClose = new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        };
        f.setVisible(true);

    }

    public static FillTable myModel() throws Exception{
//        try {
//            Class.forName ("oracle.jdbc.driver.OracleDriver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }

        Connection con = DriverManager.getConnection
                ("jdbc:postgresql://localhost:5432/postgres","postgres","123");

        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM product");
        FillTable model = new FillTable(rs);
        return model;
    }
}