package SQL_JDBC.Spec_JDBCtoSQL2.Spec_JDBCtoSQL;


import java.sql.*;
import java.util.Properties;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Student1
 */
public class DBUtil {
    
    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USER = "postgres";
    private static final String PASSWORD = "123";
    
    
    
    public static Connection getConnection() throws SQLException {
        
     Properties p = new Properties();
     p.setProperty("user", USER);
    //  Для url  не надо !!! p.setProperty("url", URL);
     p.setProperty("password", PASSWORD);
     Connection conn = DriverManager.getConnection(URL, p);
     
     return conn;

}
     public static void closeAll(ResultSet rs, Statement s, Connection c) throws SQLException{
     
           if(rs!=null) rs.close();
           if(s!=null) s.close();
           if(c!=null) c.close();
         
     } 
    
    

}
