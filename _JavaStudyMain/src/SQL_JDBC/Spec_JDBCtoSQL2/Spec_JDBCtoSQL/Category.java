/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL_JDBC.Spec_JDBCtoSQL2.Spec_JDBCtoSQL;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Student1
 */
public class Category {
 
    private int id;
    private String name;
    List<Product> lpr = new ArrayList<>();

    public Category(int id, String name, List<Product> lpr) {
        this.id = id;
        this.name = name;
        this.lpr.addAll(lpr);
//
//        lpr.stream().forEach((l) -> {
//            l.setCatId(id);
//        });
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Product> getLpr() {
        return lpr;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLpr(List<Product> lpr) {
        this.lpr.addAll(lpr);
    }
    
    
    
    
}
