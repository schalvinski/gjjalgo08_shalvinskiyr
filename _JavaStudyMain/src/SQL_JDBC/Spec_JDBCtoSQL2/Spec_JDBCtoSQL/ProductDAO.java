/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL_JDBC.Spec_JDBCtoSQL2.Spec_JDBCtoSQL;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Student1
 */
public class ProductDAO {

    public void fillProducts(List<Product> pr, Connection conn) {


        PreparedStatement ps = null;

        try {
            conn = DBUtil.getConnection();

            ps = conn.prepareStatement("insert into product values (?, ?, ?)");
            for (Product p : pr) {

                ps.setInt(1, p.getId());
                ps.setString(2, p.getName());
                ps.setTimestamp(3, new Timestamp(p.getDat().getTime()));
//                ps.setInt(4, p.getCatId());
                ps.executeUpdate();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();

        } finally {
            try {
                DBUtil.closeAll(null, ps, conn);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void removeAll() {

        Connection conn = null;
        PreparedStatement ps = null;

        try {

            conn = DBUtil.getConnection();

            ps = conn.prepareStatement("delete from product");
            ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();

        } finally {
            try {

                DBUtil.closeAll(null, ps, conn);

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }

    }

    public Product getProduct(int id) {

        Product pr = new Product();
        Connection conn = null;
        PreparedStatement ps = null;

        try {

            conn = DBUtil.getConnection();

            ps = conn.prepareStatement("select id, name from product where id = ?");
            ps.setLong(1, id);

            ResultSet res = ps.executeQuery();

            if (res.next()) {
                pr.setId(id);
                pr.setName(res.getString(2));
            }

            return pr;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return pr;

        } finally {
            try {

                DBUtil.closeAll(null, ps, conn);

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }

    }

    public List<Product> findProduct(){

        List<Product> lpr = new ArrayList<Product>();

        Connection conn = null;
        PreparedStatement ps = null;

        try {

            conn = DBUtil.getConnection();

            ps = conn.prepareStatement("select id, name, create_date from product");
            ResultSet res = ps.executeQuery();

           while (res.next()) {

                lpr.add(new Product(res.getInt(1), res.getString(2), res.getTime(3)));

            }

            System.out.println("----------");

            return lpr;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return lpr;

        } finally {
            try {

                DBUtil.closeAll(null, ps, conn);

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }


    }

    public void AddCategory(Category cat) {



       Connection conn = null;
       PreparedStatement ps = null;

        try {
            conn = DBUtil.getConnection();
            conn.setAutoCommit(false);
             
            ps = conn.prepareStatement("insert into category values (?, ?)");
           
            ps.setInt(1, cat.getId());
            ps.setString(2, cat.getName());

            ps.executeUpdate();
            
            fillProducts(cat.getLpr(), conn);
            
            conn.commit();
           

        } catch (SQLException ex) {
            ex.printStackTrace();

        } finally {
            try {

                DBUtil.closeAll(null, ps, conn);

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        }






  }

}
