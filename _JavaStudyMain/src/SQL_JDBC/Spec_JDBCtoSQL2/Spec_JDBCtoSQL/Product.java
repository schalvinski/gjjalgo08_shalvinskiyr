/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL_JDBC.Spec_JDBCtoSQL2.Spec_JDBCtoSQL;

import java.text.SimpleDateFormat;

/**
 *
 * @author Student1
 */
public class Product {
    
    private int id;
    private String name;
    private java.util.Date dat;
//    private int cat_id;

    public Product(int id, String name, java.util.Date dat) {
        this.id = id;
        this.name = name;
        this.dat = dat;
//        this.cat_id = cat_id;
    }
    
    public Product() {
      
    }
    
    public String getCol(int columnIndex) {
        
        String res = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy hh:mm"); 
        
        if(columnIndex == 0 ){res = String.valueOf(id);}
        if(columnIndex == 1 ){res = name;}
        if(columnIndex == 2 ){res = sdf.format(dat);}
//        if(columnIndex == 3 ){res = String.valueOf(cat_id);}
       
        return res;
        
    }
    

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public java.util.Date getDat() {
        return dat;
    }
    
//     public int getCatId() {
//        return cat_id;
//    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
    
     public void setDat(java.util.Date dat) {
        this.dat = dat;
    }
     
//    public void setCatId(int cat_id) {
//        this.cat_id = cat_id;
//    }
    
}
