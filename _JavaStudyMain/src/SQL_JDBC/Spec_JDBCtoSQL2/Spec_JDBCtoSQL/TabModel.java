/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL_JDBC.Spec_JDBCtoSQL2.Spec_JDBCtoSQL;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 *
 * @author Student1
 */
//класс строит таблицу
public class TabModel extends AbstractTableModel{
    
    ProductDAO dao = new ProductDAO();
    List<Product> pr = null;

    public TabModel() {
            pr = dao.findProduct();
                   
     }

    @Override // что оверрайдят
    public int getRowCount() {
        return pr.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       return pr.get(rowIndex).getCol(columnIndex);
    }
    
    @Override
    public String getColumnName(int column){
    
        //Заменить на массив
        String res = null;
        
        if(column == 0 ){res = "Идентификатор";}
        if(column == 1 ){res = "Название";}
        if(column == 2 ){res = "Дата";}
//        if(column == 3 ){res = "Категория";}
       
        return res;

   }
    public boolean isCellEditable(int row,int col){
        return true;
    }
    public void setValueAt(Object Value, int row, int col){
        fireTableCellUpdated(row,col);
    }

    
}
