package Annotations.ncedu.java.test;

public class SquareCalculator {
	public double calcSquare(Double r) {
		if (r < 0)
			throw new IllegalArgumentException("Negative radius.");
		return Math.PI*r*r;
	}
}
