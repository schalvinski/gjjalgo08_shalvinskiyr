package Annotations.ncedu.java.test;

import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class TestExample {
	
	protected static Object calculator;
	protected static Method method = null;

	@BeforeClass
	public static void setBeforeClass() throws Exception {
		Class<?> clazz = null;
		try {
			clazz = Class.forName("ru.ncedu.java.test.SquareCalculator");
		} catch (Exception e) {
			fail("Class not found");
		}
		try {
			method = clazz.getMethod("calcSquare", new Class[] {Double.class});
		} catch (Exception e) {
			fail("Method not found");
		}
		try {
			calculator = clazz.newInstance();
		} catch (Exception e) {
			fail("Object couldn't be created");
		}
	}

	@Test
	public void testPositive() {
		try {
			double positive1 = (double) method.invoke(calculator, new Object[] {1.});
			assertEquals("Calculation mistake", Math.PI, positive1, 0.0000001);
			double positive2 = (double) method.invoke(calculator, new Object[] {2.});
			assertTrue("Calculation mistake", Math.PI*4 == positive2);
		} catch(Exception e) {
			fail("Calculation error");
		}
	}
	
	@Test(expected = InvocationTargetException.class)
	public void testNegative() throws InvocationTargetException {
		try {
			method.invoke(calculator, new Object[] {-3.});
		} catch (IllegalAccessException e) {
			fail("Calculation error");
		} catch (IllegalArgumentException e) {
			fail("Calculation error");
		}
	}
}
