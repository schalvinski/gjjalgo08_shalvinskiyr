package Annotations.ncedu.java.tasks;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

/**
 * Example of realization.
 * 
 * @author Korchak Anton
 */
public class ReflectionsImpl implements Reflections {

	@Override
	public Object getFieldValueByName(Object object, String fieldName)
			throws NoSuchFieldException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> getProtectedMethodNames(Class clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Method> getAllImplementedMethodsWithSupers(Class clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Class> getExtendsHierarchy(Class clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Class> getImplementedInterfaces(Class clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Class> getThrownExceptions(Method method) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Метод создает экземпляр класса SecretClass с помощью конструктора по умолчанию, 
	 * после чего вызвает его метод foo()
	 * @return результат, который возвращает метод foo()
	 * */
	@Override
	public String getFooFunctionResultForDefaultConstructedClass() {
//		Class<?> clazz = Class.forName("ru.ncedu.java.tasks.Reflections$SecretClass");
		Class<?> clazz;
		try {
			clazz = Class.forName("ru.ncedu.java.tasks.Reflections");
			clazz = clazz.getClasses()[0];
			
			Constructor<?> constructor = clazz.getDeclaredConstructor(new Class<?>[0]);
			constructor.setAccessible(true);
			
			Object secretClassInstance = constructor.newInstance(new Object[0]);
			
			Method method = clazz.getDeclaredMethod("foo", new Class<?>[0]);
			
			method = clazz.getDeclaredMethod("foo", new Class<?>[] {String.class, Integer[].class});
			
			method.setAccessible(true);
			
			String result = (String) method.invoke(secretClassInstance, new Object[] {"Sum", new Integer[] {1, 2, 3, 4, 5}});
			
			return result;
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("Class was not found", e); 
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException("Method or constructor was not found", e);
		} catch (SecurityException e) {
			throw new IllegalStateException("Method is private", e);
		} catch (InstantiationException e) {
			throw new IllegalStateException("Constructor error", e);
		} catch (IllegalAccessException e) {
			throw new IllegalStateException("Constructor error", e);
		} catch (IllegalArgumentException e) {
			throw new IllegalStateException("Constructor error", e);
		} catch (InvocationTargetException e) {
			throw new IllegalStateException("Constructor error", e);
		}
	}

	@Override
	public String getFooFunctionResultForClass(String constructorParameter,
			String string, Integer... integers) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(new ReflectionsImpl().getFooFunctionResultForDefaultConstructedClass());
	}
}
