package Annotations.ex_1;

/**
 * Created by user on 18.02.17.
 */


import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationParsing {

    public static void main(String[] args) throws SecurityException, ClassNotFoundException, Throwable {
        for (Method method : AnnotationParsing.class.getClassLoader()
                .loadClass(("Annotations.AnnotationExample")).getMethods()) {
            // checks if MethodInfo annotation is present for the method
            if (method.isAnnotationPresent(MethodInfo.class)) {
                // iterates all the annotations available in the method
                for (Annotation anno : method.getDeclaredAnnotations()) {
                    System.out.println("Annotation in Method '" + method + "' : " + anno);
                }
                MethodInfo methodAnno = method.getAnnotation(MethodInfo.class);
                if (methodAnno.revision() == 1) {
                    System.out.println("Method with revision no 1 = " + method);
                }

            }
        }
    }

}