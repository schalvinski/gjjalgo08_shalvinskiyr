package Class_Loader;

/**
 * Created by user on 09.02.17.
 */
class ClassLoader {
    public static void main(String[] args) {
        new Test1();
    }
}

class Test {
    static {
        System.out.println("1");
    }

    {
        System.out.println("3");
    }

    Test() {
        System.out.println("4");
    }
}

class Test1 extends Test {
    static {
        System.out.println("2");
    }

    {
        System.out.println("5");
    }

    Test1() {
        System.out.println("6");
    }
}



