package Getters_setters.Ex1;

/**
 * Created by roman on 04.09.2016.
 */
public class Home {
    public String town;
    private String street;
    public int home;
    private int flat;


    public void setStreet(String street){
        this.street=street;
    }
    public String getStreet(){
        return street;
    }

    public int getFlat() {
        return flat;
    }

    public void setFlat(int f) {
        this.flat = f;
    }
}
