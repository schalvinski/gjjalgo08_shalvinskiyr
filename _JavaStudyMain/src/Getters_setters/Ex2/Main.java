package Getters_setters.Ex2;

/**
 * Created by roman on 04.09.2016.
 */
public class Main {
    public static void main(String[] args) {
        Box my = new Box();

        my.setNumber(10);

        System.out.println("the number is "+my.getNumber());
    }
}
