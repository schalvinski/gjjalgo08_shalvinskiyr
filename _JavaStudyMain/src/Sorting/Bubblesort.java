package Sorting;

/**
 * Created by roman on 19.11.2016.
 */

class Bubblesort<E extends Comparable<E>> {
    E[] bubbleSort(E[] arr) {
        E tmp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        return arr;
    }
}
