package Sorting;


import static Sorting.util.Assert.assertEquals;

/**
 * Created by roman on 19.11.2016.
 */
public class BubbleSortTest {
    public static void main(String[] args) {
        test();
    }

    public static void test() {
        Bubblesort<String> stringBubblesort = new Bubblesort<>();
        String[] testStr = new String[]{"b", "b","b","a", "d", "x", "c", "q", "m"};
        String[] resultStr = new String[]{"a", "b", "c", "d", "m", "q", "x"};
        assertEquals("testBubbleSort", resultStr, stringBubblesort.bubbleSort(testStr));
        System.out.println();
        Bubblesort<Integer> integerBubblesort = new Bubblesort<>();
        Integer[] testInt = new Integer[]{7, 4, 1, 8, 5, 2, 9, 6, 3};
        Integer[] resultInt = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertEquals("testBubbleSort", resultInt, integerBubblesort.bubbleSort(testInt));
    }
}
