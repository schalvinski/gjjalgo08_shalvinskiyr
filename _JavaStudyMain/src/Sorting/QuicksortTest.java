package Sorting;

import static Sorting.util.Assert.assertEquals;

/**
 * Created by roman on 19.11.2016.
 */
public class QuicksortTest {
    public static void main(String[] args) {
        test();
    }

    public static void test() {
        Quicksort<String> stringQuicksort = new Quicksort<>();
        String[] testStr = new String[]{"b", "a", "d", "x", "c", "q", "m"};
        String[] resultStr = new String[]{"a", "b", "c", "d", "m", "q", "x"};
        stringQuicksort.sort(testStr);
        assertEquals("Quicksort", resultStr, testStr);

        Quicksort<Integer> integerQuicksort = new Quicksort<>();
        Integer[] testInt = new Integer[]{7, 4, 1, 8, 5, 2, 9, 6, 3};
        Integer[] resultInt = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        integerQuicksort.sort(testInt);
        assertEquals("Quicksort", resultInt, testInt);
    }
}
