package Sorting;

/**
 * Created by roman on 19.11.2016.
 */
public class Quicksort<E extends Comparable<E>> {

    public void sort(E[] arr) {
        sort(arr, 0, arr.length - 1);
    }

    public void sort(E[] arr, int left, int right) {
        if (left < right) {
            int pivot = Partition(arr, left, right);
            if (pivot > 1)
                sort(arr, left, pivot - 1);
            if (pivot + 1 < right)
                sort(arr, pivot + 1, right);
        }
    }

    public int Partition(E[] numbers, int left, int right) {
        E pivot = numbers[left];
        while (true) {
            while (numbers[left].compareTo(pivot) < 0)
                left++;

            while (numbers[right].compareTo(pivot) > 0)
                right--;

            if (left < right) {
                E temp = numbers[right];
                numbers[right] = numbers[left];
                numbers[left] = temp;
            } else {
                return right;
            }
        }
    }

    private void swap(Object[] array, int i, int j) {
        Object tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}
