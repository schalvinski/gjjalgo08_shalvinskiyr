package Sorting;

import static Sorting.util.Assert.assertEquals;

/**
 * Created by roman on 19.11.2016.
 */
public class MergeSortTest {
    public static void main(String[] args) {
        test1();
        test2();
    }

    public static void test1() {
        MergeSort<String> stringMerge_sort = new MergeSort<>();
        String[] testStr = new String[]{"b", "a", "d", "x", "c", "q", "m"};
        String[] resultStr = new String[]{"a", "b", "c", "d", "m", "q", "x"};
        stringMerge_sort.sort(testStr);
        assertEquals("merge_sortTest", resultStr, testStr);
    }

    public static void test2() {
        MergeSort<Integer> integerMerge_sort = new MergeSort<>();
        Integer[] testInt = new Integer[]{7, 4, 1, 8, 5, 2, 9, 6, 3};
        Integer[] resultInt = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        integerMerge_sort.sort(testInt);
        assertEquals("merge_sortTest", resultInt, testInt);
    }
}
