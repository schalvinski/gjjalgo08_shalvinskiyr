package Inheritance_CH08Chapter_08.P02_UsingSuper;

// Using super to overcome name hiding.
class A {
    int i;
}

// Create a subclass by extending class A.
class B extends A {
    int i; // This i hides i in A.

    B(int a, int b) {
        super.i = a; // i in A.использует i в супер классе
        i = b; // i in B.
    }

    void show() {
        System.out.println("i in superclass: " + super.i);// i in A.использует i в супер классе
        System.out.println("i in subclass: " + i);
    }
}

public class UseSuper {
    public static void main(String args[]) {
        B subOb = new B(1, 2);
        subOb.show();
    }
}
