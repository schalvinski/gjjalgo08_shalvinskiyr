package Inheritance_CH08Chapter_08.P02_UsingSuper;

/**
 * Created by user on 17.07.16.
 */
public class SuperConstuctor {
    public static void main(String[] args) {
        //        A a = new A();
        //печатет только конструкто А
//        Bb a = new Bb();
        //печатет А и Б
        Cc c = new Cc();
        //печатет А и Б C


        Bb a = new Bb(5,10);


    }

}

class Aa{ //Суперкласс
    int integerAa = 10;
    public Aa(){//констрактор нужен чтоб давать значения переменным
        System.out.println("Inside A's constructor");
    }

    public Aa(int no){
        System.out.println("inside A's constructor which takes one argument");
//        integerAa = 2*no;
    }

}

class Bb extends Aa {
    int integerBb = 20;

    public Bb(){ //конструктор не знает как работать с переменными к-рые он наследует от А, а метод знает
        System.out.println("Inside B's constructor");
    }

    public Bb(int no1,int no2)//значения для IntegerAa IntegerBb
    {
//        integerAa = no1; //в данном было бы  no1=5 из new Bb(5,10);
        super(no1);//вызвать конструктор суперкласс передает ему значение integerAa = 10
        integerBb = no2;
        System.out.println("IntegerAa = " + integerAa);
        System.out.println("IntegerBb = " + integerBb);
    }
}

class Cc extends Bb {
    int integerCc = 30;
    public Cc(){
        System.out.println("Inside C's constructor");
    }

}
