package Inheritance_CH08Chapter_08;

/**
 * Created by user on 17.07.16.
 */
public class FinalDemo {
    public static void main(String[] args) {
        Test test=new Test();
        System.out.println(test.pi);
    }

    static class Test{
        public final double pi = 3.14;//final = константа и не может быть изменена
        Test(){
//           pi=2.14;//если поставили final, то пишет ошибку
        }
    }
}
