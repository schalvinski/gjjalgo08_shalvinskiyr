package Inheritance_CH08Chapter_08.P01_InheritanceBasics1;

// Here, Box is extended to include color.
public class ColorBox extends Box {
    int color; // Color of box.

    ColorBox(double w, double h, double d, int c) {
        width = w;
        height = h;
        depth = d;
        color = c;
    }
}
