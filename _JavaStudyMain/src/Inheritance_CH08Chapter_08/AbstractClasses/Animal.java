package Inheritance_CH08Chapter_08.AbstractClasses;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by user on 20.11.16.
 */
public class Animal {
    public static void main(String[] args) {
        anonymousClassTest();

    }

    static Integer classAreaVar2 = 25;

    public static void anonymousClassTest() {
        final Integer[] localAreaVar = {25};
        //Анонимный класс
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //можно использовать переменные класса без указания final
                classAreaVar2 += 25;

                //нельзя использовать локальные переменные, если они не final;
                /*Local variable is accessed from within inner class: needs to be declared final */
                localAreaVar[0] = localAreaVar[0] +5;
            }
        };
    }
}
