package Inheritance_CH08Chapter_08.AbstractClasses;

// A simple demostration of abstract.
abstract class A {//незавершенный класс с незавершенными методами
    abstract void callme();//почему не используется почему абстрактный?
    // Concrete methods are still allowed in abstract classes.
    void callmetoo() {
        System.out.println("This is a concrete method");
    }
}

class B extends A {
    void callme() {
        System.out.println("B's implementation of callme");//завершает callme который был не завершен
    }
}

public class AbstractDemo {
    public static void main(String args[]) {
        B b = new B();

        b.callme();
        b.callmetoo();//тк этот метод также принадлеж классу B
    }
}
