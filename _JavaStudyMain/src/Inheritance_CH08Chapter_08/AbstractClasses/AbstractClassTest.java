package Inheritance_CH08Chapter_08.AbstractClasses;

/**
 * Created by user on 17.07.16.
 */
public class AbstractClassTest {

    public static void main(String[] args) {
//        MySuperClass m = new MySuperClass(); абстрактный класс не может быть объектом
//        Test t = new Test();// почему то не работает на intellIj
    }

    abstract class MySuperClass { //суперкласс

        abstract public void printInfo();//незавершенный не содержит тела

//    public void printInfo(){  //так выглядел бы без abstract
//        System.out.println("I'm inside super class.");
    }

    abstract class MySubClass extends MySuperClass { //субкласс
        abstract public void sayHi();
//            @Override
//            public void printInfo() {
////            super.printInfo();
//                System.out.println("I'm inside super class.");
    }

    class Test extends MySubClass {

        @Override
        public void printInfo() {
            System.out.println("Hi");
        }

        @Override
        public void sayHi() {
            System.out.println("Hello");
        }
    }
}

