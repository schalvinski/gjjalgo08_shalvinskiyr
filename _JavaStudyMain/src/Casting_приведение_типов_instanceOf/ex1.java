package Casting_приведение_типов_instanceOf;

/**
 * Created by user on 23.01.17.
 */
public class ex1 {
    public static void main(String[] args) {
        System.out.println(2+3); // выведет 5
        System.out.println(0x100000000l);
        System.out.println(10%3); // выведет 1
        System.out.println(12%3); // выведет 0
        System.out.println(9/2); // выведет 4
        System.out.println(9/2.0); // выведет 4.5
        System.out.println(9d/2); // выведет 4.5
        System.out.println((double)7/2); // выведет 3.5
        System.out.println((double)(7/2)); // выведет 3.0, ведь к целым преобразуется уже результат деления нацело
        System.out.println((float)7/2); //
    }
}
