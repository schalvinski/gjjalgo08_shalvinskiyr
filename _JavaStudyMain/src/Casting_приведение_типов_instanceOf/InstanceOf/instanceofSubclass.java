package Casting_приведение_типов_instanceOf.InstanceOf;

/**
 * Created by user on 18.11.16.
 */
class Parent {
    public Parent() {

    }
}

class Child extends Parent {
    public Child() {
        super();
    }
}

public class instanceofSubclass {
    public static void main(String[] a) {

        Child child = new Child();
        if (child instanceof Parent) {
            System.out.println("true");
        }

    }

}