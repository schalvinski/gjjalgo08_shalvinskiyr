package Casting_приведение_типов_instanceOf.InstanceOf;// Пример использования операторf instanceof.

class A {
	int i, j;
}

class B {
	int i, j;
}

class C extends A {
	int k;
}

class D extends A {
	int k;
}

class InstanceOf {
	public static void main(String args[]) {
		A a = new A();
		B b = new B();
		C c = new C();
		D d = new D();

		if (a instanceof A) {
			System.out.println("a есть экземпляр A");
		}
		if (b instanceof B) {
			System.out.println("b есть экземпляр B");
		}
		if (c instanceof C) {
			System.out.println("с есть экземпляр C");
		}
		if (c instanceof A) {
			System.out.println("c не может быть приведен к A");
		}
		if (a instanceof C) {
			System.out.println("a может быть приведен к C");
		}

		System.out.println();

		// Сравнение типов с порожденными типами
		A ob;

		ob = d; // Ссылка на d
		System.out.println("ob теперь ссылается на d");
		if (ob instanceof D) {
			System.out.println("ob есть экземпляр D");
		}

		System.out.println();

		ob = c; // ссылка на c
		System.out.println("ob теперь ссылается на c");

		if (ob instanceof D) {
			System.out.println("ob может быть приведен к D");
		}
		else {
			System.out.println("ob не может быть приведен к D");
		}

		if (ob instanceof A) {
			System.out.println("ob может быть приведен к A");
		}

		System.out.println();

		// все объекты 	могут быть приведены к Object
		if (a instanceof Object) {
			System.out.println("a может быть приведен к Object");
		}
		if (b instanceof Object) {
			System.out.println("b может быть приведен к Object");
		}
		if (c instanceof Object) {
			System.out.println("c может быть приведен к Object");
		}
		if (d instanceof Object) {
			System.out.println("d может быть приведен к Object");
		}
	}
}