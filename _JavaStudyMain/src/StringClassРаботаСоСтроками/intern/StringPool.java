package StringClassРаботаСоСтроками.intern;

/**
 * Created by user on 16.01.17.
 *Когда метод intern() вызван, если пул строк уже содержит строку, эквивалентную к нашему объекту,
 *  что подтверждается методом equals(Object), тогда возвращается ссылка на строку из пула.
 *  В противном случае объект строки добавляется в пул и ссылка на этот объект возвращается.
 Этот метод всегда возвращает строку, которая имеет то же значение, что что и текущая строка, но гарантирует что это будет строка из пула уникальных строк.
 Ниже приведен пример работы метода intern()
 */

public class StringPool {
    public static void main(String[] args) {
        String a = "string a";
        String b = new String("string a");
        String c = b.intern();

        System.out.println(a == b);
        System.out.println(b == c);
        System.out.println(a == c);
    }
}

