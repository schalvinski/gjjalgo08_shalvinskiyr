package StringClassРаботаСоСтроками.ToStringDemo;

// Переопределение метода toString() для класса Box.
class Box {
    double width;
    double height;
    double depth;

    Box(double w, double h, double d) {
        width = w;
        height = h;
        depth = d;
    }

    public String toString() { //если в классе есть метод toString то можно этот клас распечатать в методе main
        return "Размеры " + width + " на " + depth + " на " + height + ".";
    }
}

class ToStringDemo {
    public static void main(String args[]) {
        Box b = new Box(10, 12, 14);
        String s = "Box b: " + b; // конкатенация объекта Box
        System.out.println(b); // преобразование Box в строку
        System.out.println(s);
    }
}
