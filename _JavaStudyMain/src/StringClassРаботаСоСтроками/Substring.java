package StringClassРаботаСоСтроками;

/**
 * Created by user on 07.11.16.
 */
public class Substring {
    public static void main(String args[]) {
        String str= new String("quick brown fox jumps over the lazy dog");
        System.out.println("quick brown fox jumps over the lazy dog");
        System.out.println("Substring starting from index 15:");
        System.out.println(str.substring(15));
        System.out.println("Substring starting from index 15 and ending at 20:");
        System.out.println(str.substring(15, 20));
    }
}

//Output:
//
//        Substring starting from index 15:
//        jumps over the lazy dog
//        Substring starting from index 15 and ending at 20:
//        jump
