package StringClassРаботаСоСтроками.Uncode;

/**
 * Created by user on 28.01.17.
 */
public class App2 {
    public static void main(String[] args) throws Exception {

        System.out.println("ЭЮЯ".getBytes("UTF-8").length);
        System.out.println("ЭЮЯ".getBytes("latin1").length);
    /*
    * >> 6 — каждый символ кодирован 2мя байтами т.к. символы «ЭЮЯ» выходят за пределы однобайтного представления в UTF-8.
>> 3 — каждый символ кодирован 1м байтом т.к. символы «ЭЮЯ» кодируются cp1251 — широко используемая 8-ми битная кодировка (подобно ASCII) для кириллических символов.
    * */
    }
}
