package StringClassРаботаСоСтроками.Uncode;

import java.util.Arrays;

/**
 * Created by user on 28.01.17.

 */

public class App3 {
    public static void main(String[] args) throws Exception {
        System.out.println(Arrays.toString("A".getBytes("UTF-8")));
        System.out.println(Arrays.toString("A".getBytes("UTF-16")));
        System.out.println(Arrays.toString("A".getBytes("UTF-16BE")));
        System.out.println(Arrays.toString("A".getBytes("UTF-16LE")));
        System.out.println(Arrays.toString("A".getBytes("UTF-32")));
        System.out.println(Arrays.toString("A".getBytes("UTF-32BE")));
        System.out.println(Arrays.toString("A".getBytes("UTF-32LE")));
    }
}

/*
>>
        >> [65]
        >> [-2, -1, 0, 65]
        >> [0, 65]
        >> [65, 0]
        >> [0, 0, 0, 65]
        >> [0, 0, 0, 65]
        >> [65, 0, 0, 0]*/


/*
* >> [65] — использована кодировка UTF-8 — 8-ми битный формат преобразования Юникода, что для символа «A» являет собой один байт и совпадает со значением кодировки ASCII. Первые символы Unicode полностью соответствуют кодировке ASCII.
>> [-2, -1, 0, 65] — кодировка UTF-16 — 16-ти битный формат преобразования Юникода. UTF-16 имеет представления UTF-16BE и UTF-16LE, что значит Bid Endian и Little Endian — по сути требование компьютерной архитектуры, которая использует многобайтные значения со старшим байтом в начале (Little Endian) или старшим байтом в конце (Big Endian). BE и LE — метки порядка байт или понятным языком byte order mark — BOM.
По умолчанию UTF-16 использует Big Endian, чтобы неявно это обозначить, в начало строки (в нашем случае «A») вставляется два байта обозначающие то самое Big Endian. Эти байты [-2, −1] в начале сроки, U+FEFF и U+FFFE в шестнадцатеричном представлении — непечатные символы в Unicode.
>> [0, 65] >> [65, 0] — теперь легко можно понять порядок байт и отсутствие BOM в начале строки.
>> [0, 0, 0, 65] >> [0, 0, 0, 65] >> [65, 0, 0, 0] — такая же ситуация как и в случае с использованием кодировки UTF-16 за исключением того что кодировка UTF-32 — система кодирования фиксированной длины Fixed width encoding — каждый символ Юникода должен быть представлен в виде 32 бит. Преимущество UTF-32 заключается в том что каждый code point представляет конкретный code point юникода. Тем не менее, UTF-32 достаточно тяжеловесный по тем же причинам.
* */
