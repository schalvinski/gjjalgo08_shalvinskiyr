package StringClassРаботаСоСтроками.Uncode;

/**
 * Created by user on 28.01.17.
 */
public class App4 {
    public static void main(String[] args) throws Exception {
        char ch0 = 55378;
        char ch1 = 56816;
        char ch2 = 5378;
        char ch3 = 6816;
        String str = new String(new char[]{ch0, ch1, ch2,ch3});
        System.out.println(str);
        System.out.println(str.length());
        System.out.println(str.codePointCount(0, 2));
        System.out.println(str.charAt(0));
        System.out.println(str.charAt(1));
        System.out.println(str.charAt(2));
    }
}
