package StringClassРаботаСоСтроками.Uncode;

/**
 * Created by user on 28.01.17.
 */
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("A".getBytes("UTF-16").length);
        System.out.println("AA".getBytes("UTF-16").length);
        System.out.println("AAA".getBytes("UTF-16").length);
        System.out.println("ЭЮЯ".getBytes("UTF-8").length);
        System.out.println("ЭЮЯ".getBytes("latin1").length);
        //>> 4 — очевидно почему 4 байта(неявное указание BE занимает первые 2 байта).
        // 6 — каждый следующий символ кодируется 2-мя байтами, как это и предусмотрено системой кодирования UTF-16, поэтому добавление следующего такого же символа добавляет всего два байта.
    }
}
