package ObjectClass.equals;

/**
 * Created by user on 29.01.17.
 */
public class EqualsVsOperator {
    public static void main(String[] args) {
        String str1 = new String("javainterviewpoint");
        String str2 = new String("javainterviewpoint");
        System.out.println((str1 == str2));
        System.out.println(str1.equals(str2));

        String str3 = new String("javainterviewpoint");
        String str4 = str3;
        System.out.println((str3 == str4));
        System.out.println((str3.equals(str4)));

        int i = 5;
        int k = 5;
        System.out.println(i == k);

        int j = 2212;
        int m = 2212;
        System.out.println(j == m);


    }
}
