package ObjectClass.hash;

/**
 * Created by user on 20.11.16.
 */
import java.util.Objects;
/*from   ww  w  .  j  ava 2s. c o m*/
public class hashCode {
    public static void main(String[] args) {
        // Compute hash code for two integers, a char, and a string
        int hash = Objects.hash(10, 800, '\u20b9', "Hello");
        System.out.println("Hash Code is " + hash);

    }
}