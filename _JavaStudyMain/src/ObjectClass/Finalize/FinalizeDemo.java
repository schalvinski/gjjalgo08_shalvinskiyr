package ObjectClass.Finalize;

/**
 * Created by user on 19.11.16.
 */
import java.util.*;

public class FinalizeDemo extends GregorianCalendar {

    public static void main(String[] args) {
        try {
            // create a new ObjectDemo object
            FinalizeDemo cal = new FinalizeDemo();

            // print current time
            System.out.println("" + cal.getTime());

            // finalize cal
            System.out.println("Finalizing...");
            cal.finalize();//закрывает объект cal
            System.out.println("Finalized.");

        } catch (Throwable ex) {
            ex.printStackTrace();
        }

    }
}
