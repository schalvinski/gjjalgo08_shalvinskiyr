package ObjectClass.Finalize;

/**
 * Created by user on 18.11.16.
 */
class Finalize {//w w w . jav  a  2 s.  c o  m
    private int x;

    public Finalize(int x) {
        this.x = x;
    }

    public void finalize() {
        System.out.println("Finalizing " + this.x);
    }
}

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 20000; i++) {
            new Finalize(i);
        }
    }
}
