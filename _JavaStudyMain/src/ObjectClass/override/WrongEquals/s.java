package ObjectClass.override.WrongEquals;

import java.util.HashSet;

/**
 * Created by user on 24.01.17.
 */
public class s {
    public static void main(String[] args) {


//
//        System.out.println(a.equals(b));
//        System.out.println(b.equals(a));

        HashSet<Object> hashSet = new HashSet<>();

        WrongEquals a = new WrongEquals("xyzzy");
        WrongEquals b = new WrongEquals("xyzzy");
        WrongEquals c = new WrongEquals("xyzzy");

//        String a = new String("abc");
//        String b = new String("abc");
//        String c = new String("abc");
//        ThirdPartyType b = new ...;
//        ThirdPartyType c = new ...;
//        hashSet.add(b);
//        hashSet.add(a);
//        hashSet.add(c);
//        System.out.println(hashSet.size()+" "+b.equals(a)+" "+a.hashCode()+" "+b.hashCode()); // 3

        hashSet.add(a);
        hashSet.add(b);
        hashSet.add(c);
        System.out.println(hashSet.size() + " " + b.equals(a) + a.equals(b) + " " + a.hashCode() + " " + b.hashCode()); // 2 - todo when is it possible?
    }
}
