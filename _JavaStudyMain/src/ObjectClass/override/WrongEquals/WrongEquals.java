package ObjectClass.override.WrongEquals;

/**
 * Created by user on 24.01.17.
 */

public class WrongEquals {

    private final String variable;

    public WrongEquals(String variable) {
        this.variable = variable;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (o instanceof String) {
//            return variable.equals((String) o);
//        }
//        if (o instanceof WrongEquals) {
//            return variable.equals(((WrongEquals) o).variable);
//        }
//        return false;
//    }
//
//    @Override
//    public int hashCode() {
//        return (variable == null ? 0 : variable.hashCode());
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WrongEquals that = (WrongEquals) o;

        return variable != null ? variable.equals(that.variable) : that.variable == null;
    }

    @Override
    public int hashCode() {
        return variable != null ? variable.hashCode() : 0;
    }
}
