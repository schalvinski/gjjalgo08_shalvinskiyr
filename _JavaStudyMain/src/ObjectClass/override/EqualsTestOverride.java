package ObjectClass.override;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 24.01.17.
 */
public class EqualsTestOverride {
    public static void main(String[] args) {
        Employee1 e1 = new Employee1();
        Employee1 e2 = new Employee1();

        e1.setId(100);
        e2.setId(100);
        //Печатает false в консоли
        System.out.println(e1.equals(e2));Set employees = new HashSet();
        employees.add(e1);
        employees.add(e2);
        //Печатает два объекта
        System.out.println(employees);

    }
}
class Employee1
{
    private Integer id;
    private String firstname;
    private String lastName;
    private String department;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }
//
//    public boolean equals(Object o) {
//        if(o == null)
//        {
//            return false;
//        }
//        if (o == this)
//        {
//            return true;
//        }
//        if (getClass() != o.getClass())
//        {
//            return false;
//        }
//        Employee1 e = (Employee1) o;
//        return (this.getId() == e.getId());
//    }
//
//    @Override
//    public int hashCode()
//    {
//        final int PRIME = 31;
//        int result = 1;
//        result = PRIME * result + getId();
//        return result;
//    }



}
