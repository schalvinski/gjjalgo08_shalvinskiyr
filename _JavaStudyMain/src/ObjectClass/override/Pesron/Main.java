package ObjectClass.override.Pesron;

/**
 * Created by user on 24.01.17.
 */

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Main {


    public static void main(String args[]) {
        Set<Person> hashSet = new HashSet<Person>();
        Person a = new Person("alice", 45);
        Person b = new Person("bob", 41);
        Person c = new Person("charlie", 48);
        hashSet.add(a);
        a.name = "bob";
        hashSet.add(b);
        hashSet.add(c);


        for (Iterator<Person> iterator = hashSet.iterator(); iterator.hasNext(); ) {

            System.out.println(iterator.next());

        }
        hashSet.add(b);
        hashSet.add(a);
        hashSet.add(c);
        System.out.println(hashSet.size());


    }


}
