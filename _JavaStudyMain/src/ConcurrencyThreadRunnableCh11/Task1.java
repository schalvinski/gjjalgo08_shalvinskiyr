package ConcurrencyThreadRunnableCh11;

/**
 * Created by user on 15.10.16.
 */
public class Task1 implements Runnable {

    public void run() {
        {
            try {
                System.out.println("+++");
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                boolean value = Thread.interrupted();
                ex.printStackTrace();
            }
        }
    }
}