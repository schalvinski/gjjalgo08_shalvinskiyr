package ConcurrencyThreadRunnableCh11.Lambda;


/**
 * Created by user on 21.10.16.
 */
public class Lamdaex1changedtolamda {

    public static void main(String[] args) {
        execute(new Runnable() {
            @Override
            public void run() {
                System.out.printf("Hello!!!");
            }
        });
    }

    private static void execute(Runnable runnable){
        System.out.println("Start runner");
        runnable.run();
        System.out.println("End runner");
    }

}
