package ConcurrencyThreadRunnableCh11.P07_Synchronization;

/**
 * Created by user on 22.10.16.
 */
public class Sinchronized {

    int balance = 0;

    public static void main(String[] args) {
        Sinchronized s = new Sinchronized();
        s.goingThroughLife();

    }
    public void goingThroughLife(){
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100000; i++) {
                    add();
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100000; i++) {
                    subtract();
                }
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Balance : "+balance);
    }
    public  synchronized void add(){//если убрать синхронизацию , то ноль не будет получаться
        balance++;
    }
    public  synchronized void subtract(){
        balance--;
    }
}
