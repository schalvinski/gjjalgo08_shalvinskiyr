package ConcurrencyThreadRunnableCh11.SyncThread;

/**
 * Created by user on 15.10.16.
 */
public class CounterTask implements Runnable {

    private CounterService srv;

    public CounterTask(CounterService srv) {
        this.srv=srv;
    }


    public void run(){//500 раз будет вызываться inc();
        for (int i = 0; i < 500; i++) {
            srv.inc();
        }
    }
}
