package ConcurrencyThreadRunnableCh11.SyncThread;

/**
 * Created by user on 15.10.16.
 */
public class CounterService {

//    private Object lock = new Object();
//    private Object lock2 = new Object();

    private int counter;
//    private int value;  //для каждой переменной можно использовать свой lock


    public synchronized void inc(){
        counter++;
    }
        public int getCounter() {//тк приватная переменная
        return counter;
    }


//        public void inc () {
//            synchronized (lock) {//lock - объект блокировки
//                counter++;
//            }
//        }

//    public int getCounter() {//тк приватная переменная
//        return counter;
//    }
//
//    public void setValue(int value) {
//        synchronized (lock2) {//this - объект блокировки
//            this.value = value;
//        }
//    }
}





