package ConcurrencyThreadRunnableCh11.SyncThread;

/**
 * Created by user on 15.10.16.
 */
public class Test {

    public static void main(String[] args) throws InterruptedException {

        CounterService srv = new CounterService();

        int theCount = 4000;
        for (int i = 0; i < theCount; i++) {

            Thread th = new Thread(new CounterTask(srv));
            th.start();

        }
        Thread.sleep(8000);
        int counterValue = srv.getCounter();
        System.out.println("counterValue ="+counterValue);

    }
}
