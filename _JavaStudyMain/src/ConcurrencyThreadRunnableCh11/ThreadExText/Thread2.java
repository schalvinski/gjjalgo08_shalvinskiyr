package ConcurrencyThreadRunnableCh11.ThreadExText;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by user on 15.10.16.
 */
public class Thread2 implements Runnable {

    public void run() {
        File file = new File("/Users/user/Documents/dev/projects/getjavajob/_StudyJava/src/ConcurrencyThreadRunnableCh11/file2.txt");
        try (FileInputStream fis = new FileInputStream(file)) {
            int content;
            while ((content = fis.read()) != -1) {
                // convert to char and display it
                System.out.print((char) content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}