package ConcurrencyThreadRunnableCh11.ThreadExText;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by user on 15.10.16.
 */
public class WriteToFile implements Runnable{

    public void run() {
        try {
            File file = new File("/Users/user/Documents/dev/projects/getjavajob/_StudyJava/src/ConcurrencyThreadRunnableCh11/file3.txt");
            FileWriter fileWriter = new FileWriter(file);//
            Thread1 thread1 = new Thread1();
            System.out.println(thread1);
//            fileWriter.write();
            fileWriter.write("a test");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            //через getResult
        }
    }
}
