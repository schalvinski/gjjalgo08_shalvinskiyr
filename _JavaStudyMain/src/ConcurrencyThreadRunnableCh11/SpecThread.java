package ConcurrencyThreadRunnableCh11;

/**
 * Created by user on 15.10.16.
 */
public class SpecThread {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread();
        thread.start();
        thread.getName();
        System.out.println(thread.getName());
        System.out.println(thread.getId());
        System.out.println(thread.currentThread());
        System.out.println(thread.isAlive());
        System.out.println(thread.getPriority());
        Thread thread1 = Thread.currentThread();     //выполняется перменная типа Thread, ей присвайвается метод current
        System.out.println(thread1.isAlive());

        Thread thread2 = new Thread();
        thread.setName("Thread");
        thread2.start();//метод запускает метод run
        System.out.printf("isAlive?" + thread2.isAlive());

        System.out.println();

        Thread.sleep(100);
//        thread2.start();
        Thread.State state = thread.getState();//в зависимости от какого потока
        System.out.println(state);



        Thread thread5 = new Thread(new Task1(),"Thread from Runnable");//самый распостраненный способ вызов потока
        thread5.start();
        thread.sleep(5000);
        System.out.println(thread5.getName());
        thread5.interrupt();

        /*
        Создать n потоков в каждом будет вызов метода объекта CounterService;

        inc()
         */

    }
}
