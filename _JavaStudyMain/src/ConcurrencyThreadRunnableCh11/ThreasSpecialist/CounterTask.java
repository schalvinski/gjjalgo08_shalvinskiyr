/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConcurrencyThreadRunnableCh11.ThreasSpecialist;

/**
 *
 * @author Student1
 */
public class CounterTask implements Runnable {
    
    private CouterService srv; 
    
    public CounterTask(CouterService srv){
        this.srv = srv; 
    }
    @Override
    public void run(){
        for(int i = 0; i < 500; i++){
            srv.inc();            
        }        
    }    
}
