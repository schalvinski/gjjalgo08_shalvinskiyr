/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConcurrencyThreadRunnableCh11.ThreasSpecialist;

/**
 *
 * @author Student1
 */
public class CouterService {
    
    private Object lock1 = new Object();    
    private Object lock2 = new Object();
    
    private int counter;     
    private int value; 
        
    // 1
    /*
    public synchronized void inc(){
        counter++;
    }*/
    //
    /*
    public synchronized void inc(){
        counter++;
    } 
    */
    
    public void inc(){
        //     
        synchronized(lock1){
            counter++;
        }
    } 
    
    public int getCounter(){
        return counter;
    }
    
    public void setValue(int value){
        //     
        synchronized(lock2){
            this.value = value;
        }
    } 
        
}
