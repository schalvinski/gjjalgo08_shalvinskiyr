/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConcurrencyThreadRunnableCh11.ThreasSpecialist;

/**
 *
 * @author Student1
 */
public class Test {
    
    public static void main(String[] args) throws InterruptedException {
        
        CouterService service = new CouterService(); 
        
        int thCount = 4000; 
        for(int i = 0; i < thCount; i++) {
            Thread th = new Thread(new CounterTask(service)); 
            th.start();
        }
        // 
        Thread.sleep(8000);
        // 
        int counterValue = service.getCounter(); 
        System.out.println("counterValue=" + counterValue);        
    }    
}
