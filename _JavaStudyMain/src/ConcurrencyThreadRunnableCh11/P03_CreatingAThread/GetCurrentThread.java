package ConcurrencyThreadRunnableCh11.P03_CreatingAThread;

/**
 * Created by user on 17.10.16.
 */
public class GetCurrentThread implements Runnable {
    Thread th;

    public GetCurrentThread(String threadName) {
        th = new Thread(this,"Name for T"); //<----DOUBT
        System.out.println("get threadname "+th.getName());
        th.start();
    }

    public void run() {
        System.out.println(th.getName()+" is starting.....");
        System.out.println("Current thread name : " + Thread.currentThread().getName());
    }

    public static void main(String args[]) {
        System.out.println("Current thread name : " + Thread.currentThread().getName());
        new GetCurrentThread("1st Thread");
        //new GetCurrentThread("2nd Thread");
    }
}