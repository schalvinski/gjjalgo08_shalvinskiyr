/**
 * Created by roman on 28.07.2016.
 */
public abstract class Thread {

    public static void main(String[] args)  {
        System.out.println("start");
        Thread t = new Thread() {
            @Override
            public void run() {
                System.out.println("new thread");
            }
        };

        t.run();
        t.join();


    }

    private void join() {
    }

    public abstract void run();
}