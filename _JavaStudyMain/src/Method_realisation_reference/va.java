package Method_realisation_reference;//метод с аргументами переменной длины


class va{

    public static void main(String[] args) {
        System.out.println(sum(1, 1, 2, 3));
    }
        //метод с аргументами переменной длины
        public static int sum(int i, int...arg ){
            int sum = i;//нача льное значение
            for(int x : arg){
                sum += x;
            }
            return sum;
        }
    }


