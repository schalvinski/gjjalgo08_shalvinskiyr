package SchildtStudy;

/**
 * Created by user on 19.02.16.
 */
// Demonstrate method overloading.
public class OverloadDemo {
    public void test() {
        System.out.println("No parameters");
    }
    // Overload test for one integer parameter.
    public void test(int a) {
        System.out.println("a: " + a);
    }
    // Overload test for two integer parameters.
    public void test(int a, int b) {
        System.out.println("a and b: " + a + " " + b);
    }
    // Overload test for a double parameter
    public double test(double a) {
        System.out.println("double a: " + a);
        return a*a*a; }
}


