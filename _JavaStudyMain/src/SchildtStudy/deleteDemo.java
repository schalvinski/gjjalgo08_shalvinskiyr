package SchildtStudy;

/**
 * Created by user on 27.02.16.
 */
// Demonstrate delete() and deleteCharAt()
class deleteDemo {
    public static void main(String args[]) {
        StringBuffer sb = new StringBuffer("This is a test.");
        sb.delete(4, 7);
        System.out.println("After delete: " + sb);
        sb.deleteCharAt(1);
        System.out.println("After deleteCharAt: " + sb);
    }
}