package SchildtStudy;

// Demonstrate a two-dimensional array.
class TwoDArray {
    public static void main(String args[]) {
        int twoD[][]= new int[4][7];
        int i, j, k = 75;
        for(j=0; j<7; j++)
            for(i=0; i<4; i++) {
                twoD[i][j] = k; k++; }
        for(i=0; i<4; i++) {
            for(j=0; j<7; j++)
                System.out.print(twoD[i][j] + " ");
            System.out.println();
        }
    }
}