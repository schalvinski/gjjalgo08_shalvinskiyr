package SchildtStudy;

/**
 * Created by user on 01.03.16.
 */
/* Создать класс Dog
Создать класс Dog (собака) с тремя инициализаторами:
- Имя
- Имя, рост
- Имя, рост, цвет
*/

public class Dog
{
    private String MyDog;
    public void initialize(String name){
        this.MyDog = name;
    }
    public void initialize(String name, int height){
        this.MyDog = name+height;
    }
    public void initialize(String name,int height,String color){
        this.MyDog = name+height+color;
    }
}


