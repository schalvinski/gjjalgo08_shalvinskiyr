package SchildtStudy;

/**
 * Created by user on 27.02.16.
 */

public class StringBuilderExample {
    public static void main(String args[]) {
        StringBuilder builder = new StringBuilder();

        builder.append("boolean: ");
        builder.append(true);
        System.out.println(builder);

        builder.append("double: ");
        builder.append(1.0);
        System.out.println(builder);

        builder.insert(13, ",");
        System.out.println(builder);

        builder.delete(0, 9);
        System.out.println(builder);

        builder = new StringBuilder();

        builder.append ("boolean: ");
        builder.append (true);
        builder.append ("double: ");
        builder.append (1.0);
        builder.insert (13, ",");
        System.out.println(builder);
    }
}
