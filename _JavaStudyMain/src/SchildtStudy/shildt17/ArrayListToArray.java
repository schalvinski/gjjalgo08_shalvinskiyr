package SchildtStudy.shildt17;

/**
 * Created by user on 20.03.16.
 */

import java.util.*;
class ArrayListToArray {
    public static void main(String args[]) {
        // Create an array list.
        ArrayList<Integer> al = new ArrayList<Integer>();
        // Add elements to the array list.
        al.add(1);
        al.add(2);
        al.add(3);
        al.add(4);
        System.out.println("Contents of al: " + al);
        // Get the array.
        Integer ia[] = new Integer[al.size()];
        ia = al.toArray(ia);
        int sum = 0;
        for (int v : ia)
            System.out.println(v);

        // Sum the array.
        for(int i : ia) sum += i;
        System.out.println("Sum is: " + sum);


    } }
