package SchildtStudy;

/**
 * Created by user on 19.02.16.
 */
class Overload {
    public static void main(String args[]) {

        OverloadDemo obs = new OverloadDemo ();

        // call all versions of test()
        obs.test();
        obs.test(10);
        obs.test(10, 20);
        double result = obs.test(123.25);
        System.out.println("Result of ob.test(123.25): " + result);
    }


}
