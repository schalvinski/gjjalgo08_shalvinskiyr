package SchildtStudy.Chapter_09.P04_Interfaces;

public interface IntStack {
    void push(int item);
    int pop();
}
