package NestedAndInnerClasses.AnonymousClass;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by user on 28.05.17.
 */
public class Animal {


    public static void main(String[] args) {
        Animal animal =new Animal();
        System.out.println(animal.classAreaVar2);
        animal.doSmth();
    }

    Integer classAreaVar2 = 25;

    void doSmth(){
        new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("2we3");
            }
        };
    };

    public void anonymousClassTest() {
        Integer[] localAreaVar = {25};

        //Анонимный класс
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //можно использовать переменные класса без указания final
                int classAreaVar3 = classAreaVar2 + 25;


                //нельзя использовать локальные переменные, если они не final;
                /*Local variable is accessed from within inner class: needs to be declared final */
                localAreaVar[0] = localAreaVar[0] +5;
            }
        };
    }
}

