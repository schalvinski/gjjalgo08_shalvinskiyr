// Demonstrate iterators.

package CollectionsCH17.ListIterator;

import java.util.*;

class IteratorDemo {
	public static void main(String args[]) {
		// Create an array list.
		ArrayList al = new ArrayList<>();
		
		// Add elements to the array list.
		al.add("C");
		al.add("A");
		al.add("E");
		al.add("B");
		al.add("D");
		al.add("F");
		
		// Use iterator to display contents of al.
		System.out.print("Original contents of al: ");
		Iterator<String> itrSsilkaIterator = al.iterator();//iterator - пробегается по всей коллекции
		while(itrSsilkaIterator.hasNext()) {
			String element = itrSsilkaIterator.next();
			System.out.print(element + " ");
		}
		System.out.println();
		
		// Modify objects being iterated.
		ListIterator<String> litrSsilkaIterator = al.listIterator();
		while(litrSsilkaIterator.hasNext()) {
			String element = litrSsilkaIterator.next();
			litrSsilkaIterator.set(element + "+");
		}
		System.out.print("Modified contents of al: ");
		itrSsilkaIterator = al.iterator();
		while(itrSsilkaIterator.hasNext()) {
			String element = itrSsilkaIterator.next();
			System.out.print(element + " ");
		}
		System.out.println();
		
		// Now, display the list backwards.
		System.out.print("Modified list backwards: ");
		while(litrSsilkaIterator.hasPrevious()) {
			String element = litrSsilkaIterator.previous();
			System.out.print(element + " ");
		}
		System.out.println();
		
	}
}
