// Demonstrate TreeSet.

package CollectionsCH17;

import java.util.*;

class TreeSetDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a TreeSet.
		TreeSet<String> ts = new TreeSet<String>();
		
		// Add elements to the tree set.
		ts.add("C");
		ts.add("A");
		ts.add("B");
		ts.add("E");
		ts.add("F");
		ts.add("D");

		
		System.out.println(ts);
		System.out.println(ts.subSet("C", "F"));//[C, D, E]
		System.out.println(ts.size());
//		System.out.println(ts.toString(ts.);

	}
//	private static void testToString() {
//		BalanceableTree<Integer> tree = new BalanceableTree<>();
//		Node<Integer> myTop = tree.addRoot(67);
//		NodeImpl<Integer> node = (NodeImpl<Integer>) myTop;
//		tree.addLeft(node, 36);
//		tree.addRight(node.left, 34);
//		tree.addLeft(node.left, 12);
//		tree.addRight(node.left.left, 14);
//		tree.addRight(node, 72);
//		System.out.println(tree.toString(tree.head));
//
//		tree.printPlease(tree.head);
//	}
//
//	public static void testRotate() {
//		BalanceableTree<Integer> tree = new BalanceableTree<>();
//		Node<Integer> myTop = tree.addRoot(67);
//		NodeImpl<Integer> node = (NodeImpl<Integer>) myTop;
//		tree.addLeft(node, 36);
//		tree.addRight(node.left, 34);
//		tree.addLeft(node.left, 12);
//		tree.addRight(node.left.left, 14);
//		tree.addRight(node, 72);
//
//		System.out.println("***First Tree***");
//		tree.printPlease(tree.head);
//		tree.rotate(node.left); //right
//		System.out.println("***After rotate***");
//		tree.printPlease(tree.head);
//		System.out.println("***Second rotate***");
//		tree.rotate(tree.head.right); // left
//
//		tree.printPlease(tree.head);
//		System.out.println("***After rotate***");
//		tree.rotate(tree.head.left);
//		tree.printPlease(tree.head);
//	}
//
//	public static void testRotateTwice() {
//		BalanceableTree<Integer> tree = new BalanceableTree<>();
//		Node<Integer> myTop = tree.addRoot(67);
//		NodeImpl<Integer> node = (NodeImpl<Integer>) myTop;
//		tree.addLeft(node, 36);
//		tree.addRight(node.left, 44);
//		tree.addRight(node.left.right, 54);
//		tree.addLeft(node.left.right, 39);
//		tree.addLeft(node.left, 12);
//		tree.addRight(node.left.left, 14);
//		tree.addRight(node, 72);
//
//		tree.printPlease(tree.head);
//
//		tree.rotateTwice(node.left.right);
//		tree.printPlease(tree.head);
//	}
}
