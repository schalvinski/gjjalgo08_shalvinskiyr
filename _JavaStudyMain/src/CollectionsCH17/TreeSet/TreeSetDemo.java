package CollectionsCH17.TreeSet;

/**
 * Created by user on 04.11.16.
 */
import java.util.TreeSet;
import java.util.Iterator;

public class TreeSetDemo {
    public static void main(String[] args) {
        // creating a TreeSet
        TreeSet <Integer>treeadd = new TreeSet<Integer>();
        TreeSet <Integer>treesubset;

        // adding in the tree set
        treeadd.add(1);
        treeadd.add(2);
        treeadd.add(3);
        treeadd.add(4);
        treeadd.add(5);
        treeadd.add(6);
        treeadd.add(7);
        treeadd.add(8);

        // creating subset
        treesubset= (TreeSet) treeadd.subSet(3,7);

        System.out.println(treesubset);

        // create iterator
        Iterator iterator = treesubset.iterator();

        // displaying the Tree set data
        System.out.println("Tree subset data: ");
        while (iterator.hasNext()){
            System.out.println(iterator.next() + " ");
        }
    }
}
