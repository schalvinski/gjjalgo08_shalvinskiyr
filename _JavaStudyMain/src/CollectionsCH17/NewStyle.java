// Modern, generic version of Collections.
package CollectionsCH17;

import java.util.*;

class NewStyle {
	public static void main(String args[]) {
		// Now, list holds references of type String.
		ArrayList<String> list = new ArrayList<String>();
		
		list.add("one");
		list.add("two");
		list.add("three");
		list.add("four");
		
		// Notice, that Iterator_waytoiterate is also generic.
		Iterator<String> itrSsilkaIterator = list.iterator();
		
		// The following statement will now cause a compile-time error.
	//	Iterator_waytoiterate<Integer> itrSsilkaIterator = list.iterator(); // Error!
		
		while(itrSsilkaIterator.hasNext()) {
			String str = itrSsilkaIterator.next(); // no cast needed
			
			// Now, the following line is a compile-time,
			// rather than run-time, error.
	//		Integer i = itrSsilkaIterator.next(); // this won't compile
			
			System.out.println(str + " is " + str.length() + " chars long.");
		}
	}

}
