package CollectionsCH17.Hashmap;

/**
 * Created by user on 18.11.16.
 */

/**
 *Output:
 D: 99.22
 A: 3434.34
 C: 1378.0
 B: 123.22
 E: -19.08

 B's new balance: 1123.22
 */

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class printHashMap {
    public static void main(String args[]) {
        ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<String, Integer>();
        map.put("A", 1);
        map.put("B", 2);
        map.put("C", 3);

        for (String key : map.keySet()) {
            System.out.println(key + " " + map.get(key));
        }

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String key = entry.getKey().toString();
            Integer value = entry.getValue();
            System.out.println("key, " + key + " value " + value);
        }
    }
}

