package CollectionsCH17.Comparator;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by user on 04.11.16.
 */
public class Example {

    public static void main(String[] args) {
        Set<String> ex = new TreeSet<>(new Comp());
        ex.add(new String("Stive Global"));
        ex.add(new String("Stive Cooper"));
        ex.add(new String("Nancy Summer"));
        ex.add(new String("Aaron Eagle"));
        ex.add(new String("Barbara Smith"));

        Iterator<String> i = ex.iterator();

        while (i.hasNext()) {
            String ts = i.next();
            System.out.println("Str: " + ts);
        }
    }

}
