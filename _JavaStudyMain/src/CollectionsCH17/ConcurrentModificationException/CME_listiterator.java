package CollectionsCH17.ConcurrentModificationException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by user on 28.01.17.
 */
public class CME_listiterator {
    public static void main(String[] args) {
        List<String> bAndGirls = new ArrayList<>(Arrays.asList("Ваня", "Таня", "Саша", "Петя", "Даша"));
        ListIterator<String> itr = bAndGirls.listIterator();
        while (itr.hasNext()) {
            String boyOrGirl = itr.next();
            if ("Саша".equals(boyOrGirl)) {
                itr.add("Маша");
            } //todo две грабли почему не 6
        }
        System.out.println(bAndGirls.size());

        for (String s : bAndGirls) {
            System.out.println(s);
        }
    }

}
