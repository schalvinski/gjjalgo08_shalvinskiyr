package CollectionsCH17.ConcurrentModificationException;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by user on 28.01.17.
 */

public class FailSafeExample {
    public static void main(String[] args) {
        ConcurrentHashMap<String, String> premiumPhone =
                new ConcurrentHashMap<String, String>();
        premiumPhone.put("Apple", "iPhone");
        premiumPhone.put("HTC", "HTC one");
        premiumPhone.put("Samsung", "S5");

        Iterator iterator = premiumPhone.keySet().iterator();

        while (iterator.hasNext()) {
            System.out.println(premiumPhone.get(iterator.next()));
            premiumPhone.put("Sony", "Xperia Z");
        }
    }
}
