package CollectionsCH17.ConcurrentModificationException;

/**
 * Created by user on 28.01.17.
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FailFastExample {


    public static void main(String[] args) {
        Map<String, String> premiumPhone = new HashMap<String, String>();
        premiumPhone.put("Apple", "iPhone");
        premiumPhone.put("HTC", "HTC one");
        premiumPhone.put("Samsung", "S5");

        Iterator iterator = premiumPhone.keySet().iterator();

        while (iterator.hasNext()) {
            System.out.println(premiumPhone.get(iterator.next()));
            premiumPhone.put("Sony", "Xperia Z");
        }

    }

}