package CollectionsCH17;

import java.util.HashSet;
import java.util.Iterator;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class HashSet_remove
{
    public static void main(String[] args)
    {
         createSet();
        System.out.println(createSet());
        removeAllNumbersMoreThan10(createSet());
        System.out.println(createSet());
    }


    public static HashSet<Integer> createSet()
    {
        //Напишите тут ваш код
        HashSet<Integer> result = new HashSet<Integer>();

        for (int i = 0; i < 20; i++)
        {
            result.add(i);
        }

        return result;
    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        //Напишите тут ваш код
        Iterator<Integer> iterator = set.iterator();

        while ( iterator.hasNext() )
        {
            if ( iterator.next() > 10 )
            {
                iterator.remove();
            }
        }

        return set;
    }
}
