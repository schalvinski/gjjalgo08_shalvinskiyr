package CollectionsCH17.Comparable.CompReverseOrderSurname;

/**
 * Created by user on 04.11.16.
 */
class Comp implements Comparable {

    String str;
    int number;

    Comp(String str, int number) {
        this.str = str;
        this.number = number;
    }

    @Override
    public int compareTo(Object obj) {
        Comp entry = (Comp) obj;

// сначала ищем позицию пробела  с помощью функции indexOf(),
//после начиная с найденной позиции выкусываем подстроку
        String str1 = str.substring(str.indexOf(" "));
        String str2 = entry.str.substring(entry.str.indexOf(" "));

        // после получения подстрокой производим сравнение
        int result = str1.compareTo(str2);
        if(result != 0) {
            return result;
        }

        result = number - entry.number;
        if(result != 0) {
            return (int) result / Math.abs( result );
        }
        return 0;
    }


}
