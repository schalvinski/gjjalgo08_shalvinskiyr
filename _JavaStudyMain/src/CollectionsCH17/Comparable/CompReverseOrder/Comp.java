package CollectionsCH17.Comparable.CompReverseOrder;

/**
 * Created by user on 04.11.16.
 */
class Comp implements Comparable {

    String str;
    int number;

    Comp(String str, int number) {
        this.str = str;
        this.number = number;
    }

    @Override
    public int compareTo(Object obj) {
        Comp entry = (Comp) obj;

        int result = entry.str.compareTo(str); // значения меняются местами
        if(result != 0) {
            return result;
        }

        result = entry.number - number; // значения меняются местами
        if(result != 0) {
            return (int) result / Math.abs( result );
        }
        return 0;
    }

}
