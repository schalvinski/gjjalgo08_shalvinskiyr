package CollectionsCH17.Comparable.CompReverseOrder;

import java.util.TreeSet;

/**
 * Created by user on 04.11.16.
 */

public class Example {

    public static void main(String[] args) {
        TreeSet<Comp> ex = new TreeSet<>();
        ex.add(new Comp("Stive Global", 121));
        ex.add(new Comp("Romann Shalvin", 221));
        ex.add(new Comp("Nancy Summer", 3213));
        ex.add(new Comp("Aaron Eagle", 3123));
        ex.add(new Comp("Barbara Smith", 88786));
        ex.add(new Comp("Zorro Smith", 88786));
        ex.add(new Comp("Viagro Smith", 88786));

        for(Comp e : ex) {
            System.out.println("Str: " + e.str + ", number: " + e.number);
        }
    }

}