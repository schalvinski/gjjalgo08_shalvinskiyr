package CollectionsCH17.HashSet;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by user on 28.05.17.
 */
public class HashMapHashSetExample {

    public static void main(String[] args) {

        HashSet<String> hashsetobj = new HashSet<String>();
        hashsetobj.add("Alive is awesome");
        hashsetobj.add("Love yourself");
        hashsetobj.add("Love yourself1");
        hashsetobj.add("Love yourself2");
        hashsetobj.add("Love yourself4");
        hashsetobj.add("Love yourself5");
        hashsetobj.add("Love yourself6");
        System.out.println("HashSet object output :"+ hashsetobj);



        HashMap hashmapobj = new HashMap();
        hashmapobj.put("Alive is ", "awesome");
        hashmapobj.put("Love", "yourself");
        System.out.println("HashMap object output :"+hashmapobj);

    }
}
