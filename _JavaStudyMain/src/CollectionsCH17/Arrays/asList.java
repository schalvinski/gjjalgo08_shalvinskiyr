package CollectionsCH17.Arrays;

/**
 * Created by user on 19.12.16.
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class asList {
    public static void main(String args[]) {

        // create an array of strings
        String a[] = new String[]{"abc", "klm", "xyz", "pqr"};

        List list1 = new LinkedList(Arrays.asList("abc", "klm", "xyz", "pqr"));
        //      List list1 = Arrays.asList(a);   - тоже самое

        // printing the list
        System.out.println("The list is:" + list1);
    }
}
