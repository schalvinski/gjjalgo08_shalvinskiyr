package CollectionsCH17.TreeMap;

/**
 * Created by user on 18.11.16.
 */

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
        import javax.swing.tree.DefaultMutableTreeNode;

public class JTree extends JFrame
{
    private javax.swing.JTree tree;
    public JTree()
    {
        //create the root node
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");
        //create the child nodes
        DefaultMutableTreeNode vegetableNode = new DefaultMutableTreeNode("Vegetables");
        DefaultMutableTreeNode fruitNode = new DefaultMutableTreeNode("Fruits");
        DefaultMutableTreeNode fruitNode1 = new DefaultMutableTreeNode("Fruits1");

        //add the child nodes to the root node
        root.add(vegetableNode);
        root.add(fruitNode);
        root.add(fruitNode1);

        //create the tree by passing in the root node
        tree = new javax.swing.JTree(root);
        add(tree);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("JTree Example");
        this.pack();
        this.setVisible(true);
    }

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new JTree();
            }
        });
    }
}
