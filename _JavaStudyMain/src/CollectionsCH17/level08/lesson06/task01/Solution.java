package CollectionsCH17.level08.lesson06.task01;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* Создать два списка LinkedList и ArrayList
Нужно создать два списка – LinkedList и ArrayList.
*/

public class Solution
{
    public static Object createArrayList()
    {
        //Напишите тут ваш код
        List<String> array_list = new ArrayList<String>();
        return array_list;
    }

    public static Object createLinkedList()
    {
        //Напишите тут ваш код
        List<String> linked_list = new LinkedList<String>();
        return linked_list;
    }
}
