package CollectionsCH17.level08.lesson08.task01;

import java.util.HashSet;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class HashSet_add
{
    public static HashSet<String> createSet()
    {
        //Напишите тут ваш код
        HashSet<String> result = new HashSet<String>();

        for (int i = 0; i < 20; i++)
        {
            result.add("Л" + i);
        }

        return result;
    }
}
