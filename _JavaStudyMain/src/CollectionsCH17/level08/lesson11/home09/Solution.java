package CollectionsCH17.level08.lesson11.home09;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true,
если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args)
    {
        System.out.println(isDateOdd("JANUARY 3 2001"));
    }

    public static boolean isDateOdd(String date)
    {
        DateFormat df = new SimpleDateFormat("MMM d yyyy", Locale.US);
        Date givenDay = new Date();
        Date yearBegin = new Date();

        try
        {
            givenDay = df.parse(date);

            Calendar calendar = Calendar.getInstance();
            Calendar cal = Calendar.getInstance();
            calendar.setTime(givenDay);

            cal.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
            cal.set(Calendar.DAY_OF_YEAR, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);

            yearBegin = cal.getTime();
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return (TimeUnit.DAYS.convert((givenDay.getTime() - yearBegin.getTime()), TimeUnit.MILLISECONDS) + 1) % 2 == 0;
    }
}
