package CollectionsCH17.level08.lesson11.bonus01;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here - напиши код тут
        Scanner scanner = new Scanner(System.in);

        String month = scanner.nextLine();

        List<String> months = new ArrayList<String>();
        months.add( "January" );
        months.add( "February" );
        months.add( "March" );
        months.add( "April" );
        months.add( "May" );
        months.add( "June" );
        months.add( "July" );
        months.add( "August" );
        months.add( "September" );
        months.add( "October" );
        months.add( "November" );
        months.add( "December" );

        if ( months.contains( month ) )
        {
            int monthNumber = months.indexOf( month ) + 1;
            System.out.println( month + " is " + monthNumber + " month" );
        }
        else
        {
            System.out.println( month + " is't month" );
        }
    }

}
