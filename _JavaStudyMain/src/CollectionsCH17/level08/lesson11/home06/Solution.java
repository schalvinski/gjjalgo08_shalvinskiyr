package CollectionsCH17.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        //Написать тут ваш код
        Human c1 = new Human("c1", true, 14);
        System.out.println(c1);
        Human c2 = new Human("c2", false, 12);
        System.out.println(c2);
        Human c3 = new Human("c3", false, 11);
        System.out.println(c3);

        Human p1 = new Human("f1", true, 32);
        Human p2 = new Human("m1", false, 33);

        p1.children.add(c1);
        p1.children.add(c2);
        p1.children.add(c3);
        p2.children.add(c1);
        p2.children.add(c2);
        p2.children.add(c3);

        System.out.println(p1);
        System.out.println(p2);

        Human gp1 = new Human("gf1", true, 55);
        Human gp3 = new Human("gf2", true, 65);
        Human gp2 = new Human("gm1", false, 53);
        Human gp4 = new Human("gm2", false, 56);

        gp1.children.add(p1);
        gp2.children.add(p1);
        gp3.children.add(p2);
        gp4.children.add(p2);

        System.out.println(gp1);
        System.out.println(gp2);
        System.out.println(gp3);
        System.out.println(gp4);
    }

    public static class Human
    {
        //Написать тут ваш код
        public String name;
        public boolean sex;
        public int age;
        public ArrayList<Human> children;

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }

        public Human(String name, boolean sex, int age){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = new ArrayList<Human>();
        }
    }

}
