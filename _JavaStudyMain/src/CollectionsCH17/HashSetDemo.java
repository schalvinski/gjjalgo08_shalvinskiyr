// Demonstrate HashSet.

package CollectionsCH17;

//
//class HashSetDemo {
//
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		// Create a hash set.
//		LinkedHashSet<String> hs = new LinkedHashSet<String>();
//
//		// Add elements to the hash set.
//		hs.add("A");
//		hs.add("B");
//		hs.add("C");
//		hs.add("D");
//		hs.add("E");
//		hs.add("F");
//
//		System.out.println(hs);
//	}
//
//}
import java.util.HashSet;
import java.util.Iterator;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class HashSetDemo
{
	public static void main(String[] args)
	{
		createSet();
		System.out.println(createSet());
		removeAllNumbersMoreThan10(createSet());
		System.out.println(createSet());
	}

	public static HashSet<Integer> createSet()
	{
		HashSet<Integer> result = new HashSet<Integer>();

		for (int i = 0; i < 20; i++)
		{
			result.add(i);
		}

		return result;//напишите тут ваш код

	}

	public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
	{
		Iterator<Integer> iterator = set.iterator();

		while ( iterator.hasNext() )
		{
			if ( iterator.next() > 9 )
			{
				iterator.remove()	;
			}
		}

		return set; //напишите тут ваш код

	}
}