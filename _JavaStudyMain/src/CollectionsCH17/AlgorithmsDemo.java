// Demonstrate various algorithms.

package CollectionsCH17;

import java.util.*;

class AlgorithmsDemo {
	public static void main(String args[]) {
		// Create and initialize linked list.
		LinkedList<Integer> ll = new LinkedList<>();
		ll.add(-8);
		ll.add(20);
		ll.add(-20);
		ll.add(8);
		
		// Create a reverse order comparator.
		Comparator<Integer> r = Collections.reverseOrder();
		
		// Sort list by using the comparator.
		Collections.sort(ll, r);

		
		System.out.print("List sorted in reverse: ");
		for(int i : ll) //i последовательно принимает все значения массива
			System.out.print(i + " ");
		
		System.out.println();
		
		// Shuffle list. Различные варианты, которые можно выполнять с collections;
		Collections.shuffle(ll);
		
		// Display randomized list.
		System.out.print("List shuffled: ");
		for(int i : ll)
			System.out.print(i + " ");
		
		System.out.println();
		
		System.out.println("Minimum: " + Collections.min(ll));
		System.out.println("Maximum: " + Collections.max(ll));
	}
}
