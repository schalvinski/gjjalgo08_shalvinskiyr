package CollectionsCH17.BinaryTree.Traversals;

/**
 * Created by user on 18.05.17.
 */

import java.util.Stack;

public class BinaryTree_preorder {

//созадем класс TreeNode и конструктор с переменной int data
    public static class TreeNode {
        int data;
        TreeNode left;
        TreeNode right;
        TreeNode(int data) {
            this.data = data;
        }
    }
    // Recursive Solution
    public void preorder(TreeNode root) {
        if (root != null) {
            //Visit the node-Printing the node data
            System.out.printf("%d ", root.data);
            preorder(root.left);
            preorder(root.right);
        }
    }
    // Итеративный проход по дереву по способу преордер( когда отсчет начинается из НОДА РУТ, потом идет крайний левый нод, потом его ребенок крайний левый
    // если нету чайлнодов, то отсчет переходит в соседний самый нижний нод, получается , что мы как будто обволакиваем девево не пересекая не одну линию связи
    //создаем метод с аргументом TreeNode startnode, переменной startnode типа  Treenode
    public void preorderIter(TreeNode startnode) {

        if (startnode == null)//startnode - это переменная вида TreeNode которая имеет переменные значение int data; переменную Treenode left и TreeNode right
            //которые являются ссылками на левый и правый НОДЫ, которую в свою очередь тоже имеют такие же переменные
            return;//return не вызывается, тк startnode заполнен

        Stack stack = new Stack();//создаем STACK куда будем складывать переменные типа Treenode, в Stack значения FIFO , который положили, такой сразу и забираем
        stack.push(startnode);//кладем в начало Stack переменную startnode , которая является рутнодом нашего дерева
        while (!stack.empty()) {//создаем цикл который будет работать, пока stack полный
            TreeNode n = (TreeNode) stack.pop();//метод pop удаляет объект из начала стека и передает ее в переменную n типа Treenode
            System.out.printf("%d ", n.data); // выводим на печать int переменную нода, который мы взяли из Stacka
            if (n.right != null) {//в случае если у переменной которую мы взяли из Stacka (startnode например) есть чайлднод, то мы их кладем первыми в stack
                stack.push(n.right);
            }
            if (n.left != null) {
                stack.push(n.left);
            }
            // так как у нас в стеке уже три элемента то мы поочереди будем также доставать каждый из этих элементов распечатывать значение
            // НОДА и класть его и его детей в stack. Особенности Stack будут нам довать нам свойства preorder, тк будет браться всегда внешний нод левый чайлд, затем правый
        }
    }

    public static void main(String[] args) {
        BinaryTree_preorder bi = new BinaryTree_preorder();
        // Creating a binary tree
        TreeNode rootNode = createBinaryTree();
        System.out.println("Using Recursive solution:");

//        bi.preorder(rootNode);

        System.out.println();
        System.out.println("-------------------------");
        System.out.println("Using Iterative solution:");

        bi.preorderIter(rootNode);
    }

    public static TreeNode createBinaryTree() {

        TreeNode rootNode = new TreeNode(40);
        TreeNode node20 = new TreeNode(20);
        TreeNode node10 = new TreeNode(10);
        TreeNode node30 = new TreeNode(30);
        TreeNode node60 = new TreeNode(60);
        TreeNode node50 = new TreeNode(50);
        TreeNode node70 = new TreeNode(70);

        rootNode.left = node20;
        rootNode.right = node60;

        node20.left = node10;
        node20.right = node30;

        node60.left = node50;
        node60.right = node70;

        return rootNode;
    }
}