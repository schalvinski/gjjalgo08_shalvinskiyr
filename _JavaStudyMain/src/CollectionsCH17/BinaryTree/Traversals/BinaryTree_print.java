package CollectionsCH17.BinaryTree.Traversals;

/**
 * Created by user on 04.11.16.
 */
import java.util.Queue;
import java.util.LinkedList;

public class BinaryTree_print {


    public static class TreeNode
    {
        int data;
        TreeNode left;
        TreeNode right;

        TreeNode(int data)
        {
            this.data=data;
        }
    }

    // prints in level order
    public static void levelOrderTraversal(TreeNode startNode) {//передаем нод начальный ( а он уже имеет ссылки )
        Queue<TreeNode> queue=new LinkedList<TreeNode>();
        queue.add(startNode);
        while(!queue.isEmpty())
        {
            TreeNode tempNode=queue.poll();//удаляет (забирает) последний элемент из очереди
            System.out.printf("%d ",tempNode.data);
            if(tempNode.left!=null)
                queue.add(tempNode.left);//добавляет левый элемент в очередь
            if(tempNode.right!=null)
                queue.add(tempNode.right);
        }
    }
    public static void main(String[] args)
    {
        BinaryTree_print bi=new BinaryTree_print();
        // Creating a binary tree
        TreeNode rootNode=createBinaryTree();
        System.out.println("Level Order traversal of binary tree will be:");
        levelOrderTraversal(rootNode);
    }

    public static TreeNode createBinaryTree()
    {

        TreeNode rootNode =new TreeNode(40);
        TreeNode node20=new TreeNode(20);
        TreeNode node10=new TreeNode(10);
        TreeNode node30=new TreeNode(30);
        TreeNode node60=new TreeNode(60);
        TreeNode node50=new TreeNode(50);
        TreeNode node70=new TreeNode(70);

        rootNode.left=node20;
        rootNode.right=node60;

        node20.left=node10;
        node20.right=node30;

        node60.left=node50;
        node60.right=node70;

        return rootNode;
    }
}

