package CollectionsCH17.BinaryTree.Traversals;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by user on 18.05.17.
 */
public class BinarTree_printMY {

    static class TreeNode {
        TreeNode left;
        TreeNode right;
        int val;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        TreeNode rootNode = createBinaryTree();
        System.out.println(rootNode);
        leverordertraversal(rootNode);
    }

    private static void leverordertraversal(TreeNode startnode) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(startnode);
        while (!queue.isEmpty()) {
            TreeNode tempNode = queue.poll();
            System.out.println(tempNode.val);
            if (tempNode.right != null) {
                queue.add(tempNode.right);
            }
            if (tempNode.left != null) {
                queue.add(tempNode.left);
            }
        }


    }

    private static TreeNode createBinaryTree() {
        TreeNode rootNode = new TreeNode(10);
        TreeNode node2 = new TreeNode(20);
        TreeNode node3 = new TreeNode(30);
        TreeNode node4 = new TreeNode(40);
        TreeNode node5 = new TreeNode(50);
        TreeNode node6 = new TreeNode(60);
        TreeNode node7 = new TreeNode(70);
        rootNode.left = node2;
        rootNode.right = node3;
        node2.left = node4;
        node2.right = node5;
        node3.left = node6;
        node3.right = node7;


        return rootNode;
    }


}
