
package CollectionsCH17.Iterator_waytoiterate;

        import java.util.HashMap;
        import java.util.Iterator;
        import java.util.Map;
        import java.util.Map.Entry;

public class LoopMap {

    public static void main(String[] args) {

        // initial a Map
        Map<String, String> mapinstance = new HashMap<String, String>();
        mapinstance.put("1", "Jan");
        mapinstance.put("2", "Feb");
        mapinstance.put("3", "Mar");
        mapinstance.put("4", "Apr");
        mapinstance.put("5", "May");
        mapinstance.put("6", "Jun");

        // Map -> Set -> Iterator -> Map.Entry -> не рекомендуется
        System.out.println("\nExample 1...");
        Iterator<Entry<String,String>> iterator = mapinstance.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String,String> entry = (Map.Entry<String,String>) iterator.next();
            System.out.println("Key : " + entry.getKey() + " Value :" + entry.getValue());
        }

        // more elegant way, this should be the standard way, recommend!
        System.out.println("\nExample 2...");
        for (Map.Entry<String, String> entry : mapinstance.entrySet()) {
            System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
        }

        // weired, but works anyway, not recommend!
        System.out.println("\nExample 3...");
        for (Object key : mapinstance.keySet()) {
            System.out.println("Key : " + key.toString() + " Value : " + mapinstance.get(key));
        }

        //Java 8 only, forEach and Lambda. recommend!
        System.out.println("\nExample 4...");
        mapinstance.forEach((k,v)->System.out.println("Key : " + k + " Value : " + v));

        System.out.println();

        mapinstance.entrySet().stream().forEach(e -> System.out.println(e.getKey() + e.getValue()) );

    }

}
