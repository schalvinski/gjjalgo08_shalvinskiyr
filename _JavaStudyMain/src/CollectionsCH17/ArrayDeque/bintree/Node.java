package CollectionsCH17.ArrayDeque.bintree;

public interface Node<T> {
    public T key();
    public Node<T> left();
    public Node<T> right();
    public Node<T> setLeft(Node<T> node);
    public Node<T> setRight(Node<T> node);
}
