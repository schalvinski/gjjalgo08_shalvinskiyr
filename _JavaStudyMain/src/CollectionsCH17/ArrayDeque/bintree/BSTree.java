//package CollectionsCH17.ArrayDeque.bintree;
//
//import io.github.traib.dsa.bintree.BinTree;
//import io.github.traib.dsa.bintree.BinTree.NodeImpl;
//
//public final class BSTree {
//
//    public static <T extends Comparable<? super T>> boolean isHelper(Node<T> node, T floorAncestor, T ceilingAncestor) {
//        if (node == null) {
//            return true;
//        }
//        if (floorAncestor != null && node.key().compareTo(floorAncestor) <= 0) {
//            return false;
//        }
//        if (ceilingAncestor != null && node.key().compareTo(ceilingAncestor) >= 0) {
//            return false;
//        }
//        return isHelper(node.left(), floorAncestor, node.key()) &&
//                isHelper(node.right(), node.key(), ceilingAncestor);
//    }
//    public static <T extends Comparable<? super T>> boolean is(Node<T> node) {
//        return isHelper(node, null, null);
//    }
//
//    public static void main(String[] args) {
//        @SuppressWarnings("unchecked")
//        NodeImpl<Character>[] charNodes = new NodeImpl[26];
//        for (char c = 'A'; c <= 'Z'; ++c) {
//            charNodes[c - 'A'] = new NodeImpl<>(c);
//        }
//        charNodes[16].attach(charNodes[2], charNodes[17]);
//        charNodes[2].attach(charNodes[1], charNodes[13]);
//        charNodes[13].attach(charNodes[3], charNodes[14]);
//        charNodes[17].attach(null, charNodes[24]);
//        charNodes[24].attach(charNodes[21], null);
//        System.out.println(BinTree.toString(charNodes[16]));
//        System.out.println(BSTree.is(charNodes[16]));
//    }
//
//}
