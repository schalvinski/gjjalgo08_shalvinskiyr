package CollectionsCH17.ArrayDeque;

/**
 * Created by user on 17.11.16.
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;

public class ArrayDeque_constrct {
    public static void main(String[] args) {
        Collection<Integer> col = new ArrayList<Integer>();
        col.add(0);
        Collection<Integer> col1 = new ArrayList<Integer>();
        col1.add(0);
        col1.add(1);
        col1.add(2);
        Deque<Integer> deque = new java.util.ArrayDeque<Integer>(col);

        deque.add(3);
        deque.add(18);
        deque.add(25);
        deque.add(18);
        deque.addAll(col1);

        System.out.println(deque);


    }
}