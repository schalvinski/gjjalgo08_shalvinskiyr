package CollectionsCH17.UnmodifiableCollection;



/**
 * Created by user on 21.11.16.
 */

import java.util.*;

public class CollectionsDemo {
    public static void main(String[] args) {
        // create array list
        List<Character> list = new ArrayList<Character>();

        // populate the list
        list.add('X');
        list.add('Y');

        System.out.println("Initial list: "+ list);

        Collection<Character> immutablelist = Collections.unmodifiableCollection(list);//после нельзя менять коллекцию

        // try to modify the list
        immutablelist.add('Z');
    }
}