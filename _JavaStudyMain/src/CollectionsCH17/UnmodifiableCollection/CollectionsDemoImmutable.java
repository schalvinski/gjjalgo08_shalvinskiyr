package CollectionsCH17.UnmodifiableCollection;

/**
 * Created by user on 21.11.16.
 */

        import java.util.*;

public class CollectionsDemoImmutable {
    public static void main(String[] args) {
        // create array list
        List<Character> list = new ArrayList<Character>();

        // populate the list
        list.add('X');
        list.add('Y');

        System.out.println("Initial list: "+ list);

        Collection<Character> immutablelist = Collections.unmodifiableCollection(list);

        // try to modify the list
        immutablelist.add('Z');

//        //Let us compile and run the above program, this will produce the following result.
//
//        Initial list: [X, Y]
//        Exception in thread "main" java.lang.UnsupportedOperationException
    }
}
