package CollectionsCH17.Stack;

import java.util.Objects;

/**
 * Created by ������� on 04.11.2015.
 * on 19:02
 */
public class ExpressionCalculator {

    class Priority {
        String operator;
        int priority;

        public Priority(String operator, int priority) {
            this.operator = operator;
            this.priority = priority;
        }
    }

    LinkedListQueue<String> exit = new LinkedListQueue<>();
    LinkedListStack<String> stack = new LinkedListStack<>();

    Priority[] priorityMap = new Priority[]{
            new Priority("+", 1), new Priority("-", 1),
            new Priority("/", 2), new Priority("*", 2),
    };

    /**
     * убирает пробелы
     * @param expression
     * @return
     */
    String rmSpace(String expression) {
        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) != ' ') {
                result.append(expression.charAt(i));
            }
        }
        return result.toString();
    }

    boolean isOperand(char symbol) {
        return symbol > 'a' - 1 && symbol < 'z' + 1;
    }

    void bracketsParse() {
        while (!Objects.equals(stack.get(0), "(")) {
            exit.add(stack.pop());
        }
        stack.pop();//remove '('
    }

    void priorityParse(String pushing) {
        while (!stack.isEmpty() && !priorityComparison(pushing, stack.get(0))) {
            exit.add(stack.pop());
        }
    }

    void finalParse() {
        while (!stack.isEmpty()) {
            exit.add(stack.pop());
        }
    }

    boolean priorityComparison(String pushing, String pushed) throws IllegalArgumentException {//
        if (Objects.equals(pushing, "(") || Objects.equals(pushed, "(")) {
            return true;
        }
        int pushingPrior = -1;
        int pushedPrior = -1;
        for (Priority aPriorityMap : priorityMap) {
            if (Objects.equals(pushing, aPriorityMap.operator)) {
                pushingPrior = aPriorityMap.priority;
            }
            if (Objects.equals(pushed, aPriorityMap.operator)) {
                pushedPrior = aPriorityMap.priority;
            }
        }

        if (pushedPrior == -1 || pushingPrior == -1) {
            throw new IllegalArgumentException("wrong operator");
        }

        return pushingPrior > pushedPrior;
    }

    void converter(String expression) {
        expression = rmSpace(expression);
        System.out.println(expression);
        for (int i = 0; i < expression.length(); i++) {
            if (isOperand(expression.charAt(i))) {//if operand
                exit.add(String.valueOf(expression.charAt(i)));//add to exit
            } else {
                if (expression.charAt(i) == ')') {//if ')' parse stack next to '(' to exit
                    bracketsParse();//parse stack next to '(' to exit
                } else if (!stack.isEmpty() && priorityComparison(String.valueOf(expression.charAt(i)), stack.get(0))) {
                    stack.push(String.valueOf(expression.charAt(i)));//push into the stack
                } else if (!stack.isEmpty() && !priorityComparison(String.valueOf(expression.charAt(i)), stack.get(0))) {
                    priorityParse(String.valueOf(expression.charAt(i)));//because of priority parse from stack to exit
                    stack.push(String.valueOf(expression.charAt(i)));//push into the stack
                } else {
                    stack.push(String.valueOf(expression.charAt(i)));//push into the stack
                }
            }
        }
        finalParse();
//        exit.print();
//        stack.print();
    }

    public class OperandValue {
        String operand;
        String value;

        public OperandValue(String operand, String value) {
            this.operand = operand;
            this.value = value;
        }
    }

    public OperandValue[] valueMap = new OperandValue[]{
            new OperandValue("a", "1"), new OperandValue("b", "2"),
            new OperandValue("c", "3"), new OperandValue("d", "4")
    };

    void replacer() {
        for (int i = 0; i < exit.size(); i++) {
            for (OperandValue val : valueMap) {
                if (exit.get(i).equals(val.operand)) {
                    exit.set(i, val.value);
                }
            }
        }
//        exit.print();
    }

    boolean isOperator(String str) {
        boolean flag;
        flag = str.length() <= 1;
        String operators = "-+/*";
        return flag && operators.indexOf(str.charAt(0)) >= 0;
    }

    void count(int i) {
        double res = 0;
        if (exit.get(i + 2).equals("+")) {
            res = Double.parseDouble(exit.get(i).replace(",", ".")) + Double.parseDouble(exit.get(i + 1).replace(",", "."));
        } else if (exit.get(i + 2).equals("-")) {
            res = Double.parseDouble(exit.get(i).replace(",", ".")) - Double.parseDouble(exit.get(i + 1).replace(",", "."));
        } else if (exit.get(i + 2).equals("*")) {
            res = Double.parseDouble(exit.get(i).replace(",", ".")) * Double.parseDouble(exit.get(i + 1).replace(",", "."));
        } else if (exit.get(i + 2).equals("/")) {
            res = Double.parseDouble(exit.get(i).replace(",", ".")) / Double.parseDouble(exit.get(i + 1).replace(",", "."));
        }
        exit.remove(i + 1);
        exit.remove(i + 1);
        exit.set(i, String.valueOf(res));
    }

    void counter() {
        for (int i = 0; i < exit.size() - 2; i++) {
            if (!isOperator(exit.get(i)) &&
                    !isOperator(exit.get(i + 1)) &&
                    isOperator(exit.get(i + 2))) {
                count(i);
//                    System.out.print(i + ":  "); exit.print();
                counter();
                break;
            }
        }
    }

    double calculate(String expression) {
        converter(expression);
        replacer();
        counter();
        return Double.valueOf(exit.get(0));
    }

}