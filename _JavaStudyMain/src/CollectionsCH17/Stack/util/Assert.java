package CollectionsCH17.Stack.util;

import java.util.Arrays;

/**
 * Created by Velikohatko Nicolay
 * on 29.12.2014.
 */
public class Assert {
    public static void assertEquals(int result, int expected) {
        if (expected == result) {
            System.out.println("[PASSED] actual: \n" + result);
        } else {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
        }
    }

    public static void assertEquals(double result, double expected) {
        if (expected == result) {
            System.out.println("[PASSED] actual: \n" + result);
        } else {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
        }
    }

    public static void assertEquals(float result, float expected) {
        if (expected == result) {
            System.out.println("[PASSED] actual: \n" + result);
        } else {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
        }
    }

    public static void assertEquals(String result, String expected) {
        if (expected.equals(result)) {
            System.out.println("[PASSED] actual: \n" + result);
        } else {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
        }
    }

    public static boolean assertEquals(Object result, Object expected) {
        if(result == null && expected == null){
            System.out.println("[PASSED] actual: \n" + result);
            return true;
        }
        if(result == null && expected != null){
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
            return false;
        }
        if (result != null && expected == null) {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
            return false;
        }
        if (expected.equals(result)) {
            System.out.println("[PASSED] actual: \n" + result);
            return true;
        } else {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
            return false;
        }
    }

    public static void assertEquals(char result, char expected) {
        if (expected == result) {
            System.out.println("[PASSED] actual: \n" + result);
        } else {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
        }
    }

    public static void assertEquals(boolean result, boolean expected) {
        if (expected == result) {
            System.out.println("[PASSED] actual: \n" + result);
        } else {
            System.out.println("[FAILED] actual: " + result + ", expected: " + expected);
        }
    }

    public static void assertEquals(int[] result, int[] expected) {
        if (Arrays.equals(expected, result)) {
            System.out.println("[PASSED] actual:");
            for (int re : result) System.out.print(re + " ");
        } else {
            System.out.println("[FAILED] actual:");
            for (int re : result) System.out.print(re + " ");
            System.out.println("expected: ");
            for (int ex : expected) System.out.print(ex + " ");
        }
        System.out.println();
    }

    public static void assertEquals(double[] result, double[] expected) {
        if (Arrays.equals(expected, result)) {
            System.out.println("[PASSED] actual:");
            for (double re : result) System.out.print(re + " ");
        } else {
            System.out.println("[FAILED] actual:");
            for (double re : result) System.out.print(re + " ");
            System.out.println("expected: ");
            for (double ex : expected) System.out.print(ex + " ");
        }
        System.out.println();
    }

    public static void assertEquals(int[][] result, int[][] expected) {
        boolean equ = true;
        for(int i = 0; i < result.length; i++){
            equ = equ && Arrays.equals(result[i], expected[i]);
        }
        if (equ) {
            System.out.println("\n[PASSED] actual:");
            for (int i = 0; i < result.length; i++) {
                for (int j = 0; j < result[0].length; j++) {
                    System.out.printf("%d \t", result[i][j]);
                }
                System.out.println();
            }
        }
        else {
            System.out.println("\n[FAILED] actual:");
            for (int i = 0; i < result.length; i++) {
                for (int j = 0; j < result[0].length; j++) {
                    System.out.printf("%d \t", result[i][j]);
                }
                System.out.println();
            }
            System.out.println("expected: \n");
            for (int i=0;i < expected.length; i++){
                for (int j = 0; j<expected[0].length;j++){
                    System.out.printf("%d \t", result[i][j]);
                }
                System.out.println();
            }
        }
    }

    public static void fail(String mgs){
        throw new AssertionError(mgs);
    }
}
