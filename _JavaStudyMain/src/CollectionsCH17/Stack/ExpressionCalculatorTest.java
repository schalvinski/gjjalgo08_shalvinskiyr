package CollectionsCH17.Stack;

import CollectionsCH17.Stack.util.Assert;
/**
 * Created by ������� on 05.11.2015.
 * on 0:24
 */
public class ExpressionCalculatorTest {

    public static void rmSpacetest(){
        ExpressionCalculator d = new ExpressionCalculator();
        String a = " t e st ";
        Assert.assertEquals(d.rmSpace(a), "test");
    }

    public static void isOperandtest(){
        ExpressionCalculator d = new ExpressionCalculator();
        Assert.assertEquals(d.isOperand('a'), true);
        Assert.assertEquals(d.isOperand('z'), true);
        Assert.assertEquals(d.isOperand('p'), true);
        Assert.assertEquals(d.isOperand('+'), false);
        Assert.assertEquals(d.isOperand('/'), false);
    }

    public static void priorityComparisontest(){
        ExpressionCalculator d = new ExpressionCalculator();
        Assert.assertEquals(d.priorityComparison("+", "-"), false);
        Assert.assertEquals(d.priorityComparison("+", "/"), false);
        Assert.assertEquals(d.priorityComparison("/", "-"), true);
        Assert.assertEquals(d.priorityComparison("*", "+"), true);
    }

    public static void isOperatortest() {
        ExpressionCalculator d = new ExpressionCalculator();
        Assert.assertEquals(d.isOperator("nyet"), false);
        Assert.assertEquals(d.isOperator("a"), false);
        Assert.assertEquals(d.isOperator("b"), false);
        Assert.assertEquals(d.isOperator("+"), true);
        Assert.assertEquals(d.isOperator("/"), true);
    }

    public static void test1(){
        ExpressionCalculator d = new ExpressionCalculator();
        double res;
        res = d.calculate("a - b * ( c + d ) - c");
        Assert.assertEquals(res, -16.0);
    }

    public static void test2(){
        ExpressionCalculator d = new ExpressionCalculator();
        d.valueMap[0].value = String.valueOf(10);//a
        double res;
        res = d.calculate("a + (b * ( c + d ) - c * b) / d");
        Assert.assertEquals(res, 12.0);
    }

    public static void main(String[] args) {
        isOperatortest();
        priorityComparisontest();
        isOperandtest();
        rmSpacetest();
        test1();
        test2();
    }
}
