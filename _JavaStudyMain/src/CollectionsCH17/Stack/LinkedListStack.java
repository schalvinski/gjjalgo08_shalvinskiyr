package CollectionsCH17.Stack;

import java.util.EmptyStackException;

/**
 * Created by ������� on 25.10.2015.
 * on 16:28
 */

interface Stack<E> {
    void push(E e); // add element to the top
    E pop(); // removes element from the top
}

public class LinkedListStack<E> extends SinglyLinkedList<E> implements Stack<E> {

    @Override
    public void push(E o) {
        add(0, o);
    }

    @Override
    public E pop() throws EmptyStackException {
        if(isEmpty()){
            throw new EmptyStackException();
        }
        E res;
        res = get(0);
        remove(0);
        return res;
    }
}
