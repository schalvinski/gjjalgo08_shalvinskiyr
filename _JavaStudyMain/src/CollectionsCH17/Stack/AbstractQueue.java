package CollectionsCH17.Stack;

/**
 * Created by ������� on 28.10.2015.
 * on 12:10
 */
public abstract class AbstractQueue<E> extends SinglyLinkedList<E> implements java.util.Queue<E> {
    @Override
    abstract public boolean offer(E e);

    @Override
    public E remove() {
        if(isEmpty()){
            return null;
        }
        return remove(size() - 1);
    }

    @Override
    abstract public E poll();

    @Override
    abstract public E element();

    @Override
    abstract public E peek();
}
