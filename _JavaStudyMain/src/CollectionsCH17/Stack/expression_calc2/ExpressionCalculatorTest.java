package CollectionsCH17.Stack.expression_calc2;

/**
 * Created by taala on 24.08.2015.
 */
public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        test();
    }

    public static void test(){
        String expression = "(1+2)*(3+4)/(12-5)";
        ExpressionCalculator convert = new ExpressionCalculator(expression);
        Assert.assertEquals("testExpressionCalcualtor","12+34+*125-/", convert.infixToPostfix());
    }
}
