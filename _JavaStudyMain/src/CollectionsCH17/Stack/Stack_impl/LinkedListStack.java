package CollectionsCH17.Stack.Stack_impl;

/**
 * Created by paul on 23.11.16.
 */
public class LinkedListStack<E> {

    private int top = -1;
    private Node<E> head;

    static class Node<E> {
        public Node<E> next;
        public E val;
    }
    public void push(E e) {
        Node node = new Node();
        node.val = e;
        node.next = head;
        head = node;
    }
    public E pop() {
        Node<E> temp = head;
        head = head.next;
        return temp.val;
    }
    @Override
    public String toString() {
        String str = "";
        Node<E> node = head;
        while (node != null) {
            str += node.val + " ";
            node = node.next;
        }
        return str;
    }
}
