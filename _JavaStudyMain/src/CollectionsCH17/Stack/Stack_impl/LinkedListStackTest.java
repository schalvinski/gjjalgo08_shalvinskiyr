package CollectionsCH17.Stack.Stack_impl;

import static CollectionsCH17.Stack.Stack_impl.Assert.assertEquals;

/**
 * Created by paul on 10.08.16.
 */
public class LinkedListStackTest {

    public static void main(String[] args) {
        testPop();
        testPush();
    }

    private static void testPop() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.pop();
        assertEquals("LinkedListStackRemoveRemove", "8 7 ", stack.toString());
    }

    private static void testPush() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        stack.push(7);
        stack.push(8);
        stack.push(9);
        assertEquals("LinkedListStackAddTest", "9 8 7 ", stack.toString());
    }
}
