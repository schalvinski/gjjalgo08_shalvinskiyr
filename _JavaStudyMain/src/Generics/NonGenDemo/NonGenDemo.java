package Generics.NonGenDemo;

// NonGen - функциональный эквивалент NonGen,
// не использующий обобщений
class NonGen {
	Object ob; // ob теперь имеет тип Object

	// передать конструктору ссылку на объект типа Object
	NonGen(Object o) {
		ob = o;
	}

	Object getob() {
		return ob;
	}

	// показать тип ob
	void showType() {
		System.out.println("Типом ob являтся " + ob.getClass().getName());
	}
}

// Демонстрация необобщенного класса
class NonGenDemo {
	public static void main(String args[]) {

		NonGen iOb = new NonGen(88);

		// Создать объект NonGen и сохранить
		// Integer в нем. Автоупаковка используется.

		// показать тип данных, используемый iOb
		iOb.showType();

		// Получить значение iOb.
		// На этот раз приведение необходимо
		int v = (Integer) iOb.getob();
		System.out.println("Значение: " + v);
		System.out.println();

		// Создать другой объект NonGen и
		// сохранить в нем String
		NonGen strOb = new NonGen("Текст без обобщений");

		// Показать тип данных, используемый strOb
		strOb.showType();

		// Получить значение strOb
		// опять же - приведение необходимо
		String str = (String) strOb.getob();
		System.out.println("Значение: " + str);

		// Это компилируется, но концептуально неверно!
		//iOb = strOb;
		//v = (Integer) iOb.getob(); // ошибка времени выполнения!
	}
}
