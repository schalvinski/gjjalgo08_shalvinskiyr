package Generics.GenericEx2_car;

/**
 * Created by roman on 05.09.2016.
 */
public class BMW {
    String name = "BMW";
    String model;
    int year;

    BMW(int year,String n){
        this.year = year;
        model = n;
    }

    public String cars(){
        String s = "Автомобиль " + name + model + " year - "+" Germany";
        return s;
    }
}
