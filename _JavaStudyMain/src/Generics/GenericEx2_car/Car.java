package Generics.GenericEx2_car;

/**
 * Created by roman on 05.09.2016.
 */
public class Car<T> {
    T car;

    Car(T ob){
        car=ob;
    }


    public static void main(String[] args) {
        Car<BMW> car1 = new Car<BMW>(new BMW(2011," x5"));
        System.out.println(car1.car.cars());
        Car<KIA> car3 = new Car<>(new KIA(2011," solaris"));
        System.out.println(car3.car.cars());
    }
}