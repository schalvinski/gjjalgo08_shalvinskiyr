package Generics.HierDemo3;

// Использование оператора instanceof с иерархией обобщенных классов
class Gen<T> {
	T ob;

	Gen(T o) {
		ob = o;
	}

	// возвращает ob
	T getob() {
		return ob;
	}
}

// подкласс Gen
class Gen2<T> extends Gen<T> {
	Gen2(T o) {
		super(o);
	}
}

// Демонстрация определения идентификатора
// типа в иерархии обобщенных классов
class HierDemo3 {
	public static void main(String args[]) {
		// создать объект Gen для Integer
		Gen<Integer> iOb = new Gen<Integer>(88);

		// создать объект Gen2 для Integer
		Gen2<Integer> iOb2 = new Gen2<Integer>(99);

		// создать объект Gen2 для String
		Gen2<String> strOb2 = new Gen2<String>("Обобщенный текст");

		// проверить, является ли iOb2 какой-то из форм Gen2
		if (iOb2 instanceof Gen2<?>) {
			System.out.println("iOb2 является экземпляром Gen2");
		}

		// проверить, является ли iOb2 какой-то из форм Gen
		if (iOb2 instanceof Gen<?>) {
			System.out.println("iOb2 является экземпляром Gen");
		}

		System.out.println();

		// проверить, является ли strOb2 объектом Gen2
		if (strOb2 instanceof Gen2<?>) {
			System.out.println("strOb2 является экземпляром Gen2");
		}

		// проверить, является ли strOb2 объектом Gen
		if (strOb2 instanceof Gen<?>) {
			System.out.println("strOb2 является экземпляром Gen");
		}

		System.out.println();

		// проверить, является ли iOb экземпляром Gen2, что не так
		if (iOb instanceof Gen2<?>) {
			System.out.println("iOb является экземпляром Gen2");
		}

		// проверить, является ли iOb экземпляром Gen2, что так и есть
		if (iOb instanceof Gen<?>) {
			System.out.println("iOb является экземпляром Gen");
		}

		// Следующее не скомпилируется, потому что информация
		// об обобщенном типе во время выполнения отсутствует
		// if (iOb2 instanceof Gen2<Integer>) {
		// 		System.out.println("iOb2 является экземпляром Gen2<Integer>");
		// }
	}
}