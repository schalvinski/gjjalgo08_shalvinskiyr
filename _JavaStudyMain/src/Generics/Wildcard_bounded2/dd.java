package Generics.Wildcard_bounded2;

/**
 * Created by user on 28.01.17.
 */

class NaturalNumber {

    private int i;

    public NaturalNumber(int i) { this.i = i; }


    // ...
}

class EvenNumber extends NaturalNumber {

    public EvenNumber(int i) { super(i); }
    // ...
}