package Generics.Wildcard_bounded2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28.01.17.
 */
public class ee {
    List<EvenNumber> le = new ArrayList<>();
    List<? extends NaturalNumber> ln = le;
//    ln.add(new NaturalNumber(35));  // compile-time error
}
