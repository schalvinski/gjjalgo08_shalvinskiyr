package Generics._Hierarhy;

import java.util.Arrays;

/**
 * Created by user on 26.01.17.
 */
class Stats<T extends Integer> {
    T[] nums;

    Stats(T... o) {
        nums = o;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "nums=" + Arrays.toString(nums) +
                '}';
    }

    double average() {
        double sum = 0.0;
        for (int i = 0; i < nums.length; i++)
            sum += nums[i].doubleValue();
        return sum / nums.length;
    }

    public static void main(String[] args) {
        Stats<Integer> stats =  new Stats<>(1,2,3);//Number уже нельзя
        System.out.println(stats +""+stats.average());
    }
}

