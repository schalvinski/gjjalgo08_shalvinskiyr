package Generics.Wildcard_bounde_interface;

/**
 * Created by user on 03.02.17.
 */

import java.util.LinkedList;
import java.util.List;

public class Test {

    static interface A {
        public int getSomething();
    }

    static interface B {
        public int getSomethingElse();
    }

    static class AandB implements A, B {
        public int getSomething() { return 1; }
        public int getSomethingElse() { return 2; }
    }

    // Notice the multiple bounds here. This works.
    static class AandBList<T extends A & B> {
        List<T> list;

        public List<T> getList() { return list; }
    }

    public static void main(String [] args) {
        AandBList<AandB> foo = new AandBList<AandB>(); // This works fine!
        foo.getList().add(new AandB());
        List<? extends A> bar = new LinkedList<AandB>(); // This is fine too
        // This last one fails to compile!
//        List<? extends A & B> foobar = new LinkedList<AandB>();
    }
}