package Generics.GenDemo1;
//8
//дженерики ограничивают зможности написать не то что надо
// Простой обобщенный класс.
// Здесь T - это параметр типа,
// который будет заменен реальным типом
// при создании объекта класса Gen.
class Gen<T> {
	T ob; // объявление объекта типа T

	// Передать конструктору ссылку
	// на объект типа T.
	Gen(T o) {
		ob = o;
	}

	// Вернуть ob.
	T getob() {
		return ob;
	}

	// Показать тип T.
	void showType() {
		System.out.println("Типом T является " + ob.getClass().getName());
	}
}

// Демонстрация обобщенного класса.
class GenDemo {
	public static void main(String args[]) {
		// Создать Gen-ссылку для Integer.
		Gen<Integer> iOb  = new Gen<>(88);
		iOb.showType();
		int v = iOb.getob();//getob() используем из предыдущего класса
		System.out.println("значение: " + v);
		System.out.println();

		// Создать объект Gen для String.
		Gen<String> strOb = new Gen<String>("Обобщенный тест");
		strOb.showType();
		String str = strOb.getob();
		System.out.println("Значение: " + str);
	}
}