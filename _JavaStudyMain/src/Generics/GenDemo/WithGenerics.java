package Generics.GenDemo;

/**
 * Created by roman on 28.07.2016.
 */
public class WithGenerics {
    public static void main(String[] args) {

        Integer[] iray = {1,2,3,4};
        Character[] cray = {'a','b','c','d'};
        printMe(iray);
        printMe(cray);
    }
    public static <T> void printMe( T[] i){               // public static void printMe( Integer[] i){ как до дженерика выглядело
        for (T x:i)                                       //     for (Integer x:i)
            System.out.printf("%s",x);
        System.out.println();
    }
}