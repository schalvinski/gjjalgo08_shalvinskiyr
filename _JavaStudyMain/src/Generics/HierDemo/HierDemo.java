package Generics.HierDemo;

// Подкласс может добавлять собственные параметры типа
class Gen<T> {//вместо Т можно подставлять другие объекты
	T ob; // Объявление объекта типа T

	// передача конструктору
	// ссылки на объект типа T
	Gen(T o) {
		ob = o;
	}

	// возвращает ob
	T getob() {
		return ob;
	}
}

// подкласс Gen2, котоый определяет
// второй параметр типа по имени V
class Gen2<T, V> extends Gen<T> {
	V ob2;

	Gen2(T o, V o2) {
		super(o);
		ob2 = o2;
	}

	V getob2() {
		return ob2;
	}
}

// создание объекта типа Gen2
class HierDemo {
	public static void main(String args[]) {
		// создание объектов Gen2 для String и Integer
		Gen2<String, Integer> x = new Gen2<String, Integer>("Значение равно: ", 99);
		System.out.print(x.getob());
		System.out.println(x.getob2());
	}
}