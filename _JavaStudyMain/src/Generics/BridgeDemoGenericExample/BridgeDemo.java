/* Schildt Ch14
   A situation that creates a bridge method.
*/
//*
package Generics.BridgeDemoGenericExample;

class Gen<T> {
  T ob;  // declare an object of type T
  
  // Pass the constructor a reference to an object of type T.
  Gen(T o) {
    ob = o;
  }
  
  // Return ob.
  T getob() {
    return ob;
  }
}

// A subclass of Gen.
class Gen2 extends Gen<String> {//вместо Т подставляем Стринг

  Gen2(String o) {
    super(o);
  }//вместо Т подставляем Стринг
  // A String-specific override of getob().
  String getob() {//вместо Т подставляем Стринг
    System.out.print("you called String getob(): ");
    return ob;
  }
}
class Gen3 extends Gen<Integer> {//вместо Т подставляем Integer

  Gen3(Integer o) {
    super(o);
  }//вместо Т подставляем Стринг
  // A String-specific override of getob().
  Integer getob() {//вместо Т подставляем Стринг
    System.out.print(":");
    return ob;
  }
}
// Demonstrate a situation that requires a bridge method.
class BridgeDemo {
  public static void main(String args[]) {
    // Create a Gen2 object for Strings.
    Gen2 strOb2 = new Gen2("Generics test.");
    System.out.println(strOb2.getob());
    Gen3 strOb3 = new Gen3(12);
    System.out.println(strOb3.getob());
  }
}
