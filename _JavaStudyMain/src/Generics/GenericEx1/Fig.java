package Generics.GenericEx1;

/**
 * Created by roman on 04.09.2016.
 */
@Deprecated
public class Fig<T,E> {
    T figure;
    E number;

    Fig(T figure, E number){
        this.figure=figure;
        this.number=number;
    }
    public static void main(String[] args) {
        Circ circle1 = new Circ(5);
        Rect rectangle1 = new Rect("Прямоугольник",12,7);
        Circ circle2 = new Circ(4);
        Fig<Circ,Integer>figure1 =  new Fig<>(circle1,5);
        Fig<Circ,Integer>figure3 =  new Fig<>(circle2,5);
        Fig<Rect,Integer>figure2 = new Fig<>(rectangle1,10);
        System.out.println(figure1.getClass().getName());
        System.out.println(figure1.figure.getClass().getName()+","+figure1.number+"pc");
        System.out.println("Общая площадь"+figure1.figure.getSquare()*figure1.number);
        System.out.println("Общая площадь"+figure2.figure.getSquare()*figure2.number);
        System.out.println("Общая площадь"+figure3.figure.getSquare()*figure3.number);
    }
}
