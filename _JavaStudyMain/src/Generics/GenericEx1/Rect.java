package Generics.GenericEx1;

/**
 * Created by roman on 04.09.2016.
 */
public class Rect {
    String name;
    int length;
    int width;

    Rect(String name,int width,int length){
       this.name=name;
        this.length=length;
        this.width=width;
    }
    public double getSquare(){
        double area=length*width;
        return area;
    }
}
