package Generics.GenericEx1;

/**
 * Created by roman on 04.09.2016.
 */
public class Circ {
    String name = "Круг";
    int radius;

    Circ(int radius){
        this.radius=radius;
    }
    public double getSquare(){
        double area=radius*radius*Math.PI;
        return area;
    }

}
