package Generics.HierDemo2;// Необобщенный класс может быть суперклассом
// для обобщенного подкласса

// Нуобобщенный класс
class NonGen {
	int num;

	NonGen(int i) {
		num = i;
	}

	int getnum() {
		return num;
	}
}

// Обобщенный подкласс
class Gen<T> extends NonGen {
	T ob; // объявление объекта типа T

	// передать конструктору объект
	// типа T
	Gen(T o, int i) {
		super(i);
		ob = o;
	}

	// возвращает ob
	T getob() {
		return ob;
	}
}

// создать объект Gen
class HierDemo2 {
	public static void main(String args[]) {
		// создать объект Gen для String
		Gen<String> w = new Gen<String>("Добро пожаловать", 47);

		System.out.print(w.getob() + " ");
		System.out.println(w.getnum());
	}
}