package Generics.OverrideDemo;

/**
 * Created by user on 17.07.16.
 */

public class Override_demo {
    public static void main(String[] args) {
        Cycle cycle = new Cycle();
        cycle.printinfo();
        Bicycle bicycle = new Bicycle();
        bicycle.printinfo();
    }
}

class Cycle{
    int weight = 10;
    public void printinfo(){
        System.out.println("superclass"+weight);
    }
}

class Bicycle extends Cycle{ //в распоряжении Bicycle два метода pringtinfo но он использует второй
    // (более новую версию тк Override
    String name = "heroes";

    @Override
    public void printinfo(){
        System.out.println("subclass "+name);
    }
}