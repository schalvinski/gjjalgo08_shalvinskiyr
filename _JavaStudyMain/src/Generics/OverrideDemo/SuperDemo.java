package Generics.OverrideDemo;

@Deprecated

/**
 * Created by user on 17.07.16.
 */
public class SuperDemo {
    public static void main(String[] args) {
    Student student = new Student();
        student.print();//работает Override (использует из класса Student перезаписывает
        //метод super класс  Person)
    }
}

class Person{
    String name = "Vivien";
    public void print() {
        System.out.println("Class Person's name " + name);
    }
}

class Student extends Person{
    int marks = 100;
    public void print(){
        System.out.println(marks);
        System.out.println(super.name);
        super.print(); //- распечатывает из супер класса
// сначала   student.print() вызывет print потом снова вызывает и по новой - перегрузка памяти
    }
}