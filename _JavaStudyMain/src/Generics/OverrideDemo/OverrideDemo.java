package Generics.OverrideDemo;
// Переопределение обобщенного метода в обобщенном классе

class Gen<Tod> {
	Tod ob; // объявление объекта типа T

	// передать конструктору ссылку
	// на объект типа T
	Gen(Tod o) {
		ob = o;
	}

	// возвращение ob
	Tod getob() {
		System.out.print("getob() класса Gen: ");
		return ob;
	}
}

// Подкласс Gen2, переопределяющий getob()
class Gen2<T> extends Gen<T> {
	Gen2(T o) {
		super(o);
	}

	// переопределение getob()
	T getob() {
		System.out.print("getob() класса Gen2: ");
		return ob;
	}
}

// Демонстрация переопределения обобщенных методов
class OverrideDemo {
	public static void main(String args[]) {
		// создание объекта Gen для Integer
		Gen<Integer> iOb = new Gen<Integer>(88);

		// создание объекта Gen2 для Integer
		Gen2<Integer> iOb2 = new Gen2<Integer>(99);

		// создание объекта Gen2 для String
		Gen2<String> strOb2 = new Gen2<String>("Обобщенный тест");
		System.out.println(iOb.getob());
		System.out.println(iOb2.getob());
		System.out.println(strOb2.getob());
	}
}