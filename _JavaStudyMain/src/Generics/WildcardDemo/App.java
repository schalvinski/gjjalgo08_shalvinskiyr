package Generics.WildcardDemo;


import java.util.ArrayList;

/**
 * Created by roman on 30.07.2016.
 */

class Machine{
    @Override
    public String toString() {
        return "I am machine";
    }
}
class   Camera extends Machine{//)extend smay not place
    @Override
    public String toString() {
        return "I am camera";
    }
}
public class App {

    public static void main(String[] args) {


        ArrayList<Machine> list1 = new ArrayList<>();
        list1.add(new Machine());
        list1.add(new Machine());

        showList(list1);

        ArrayList<Camera> list2 = new ArrayList<>();
        list2.add(new Camera());
        list2.add(new Camera());

        showList(list2);

    }
    public static void showList(ArrayList<?> list) {//)может принимать любой тип класса
        for (Object value : list) {//)вместо Object было String
            System.out.println(value);
        }
    }
}
//    public static void main(String[] args) {
//
//
//        ArrayList<String> list = new ArrayList<>();
//        list.add("one");
//        list.add("two");
//
//        showList(list);
//
//    }


