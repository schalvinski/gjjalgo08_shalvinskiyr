package Generics.WildcardDemo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 26.01.17.
 */
class Test {
    static void printList(List<?> list) {
//        list.add();
        for (Object l : list)
            System.out.println("{" + l + "}");
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(100);
        printList(list);
        List<String> strList = new ArrayList<>();
        strList.add("10");
        strList.add("100");
        printList(strList);

    }
}
