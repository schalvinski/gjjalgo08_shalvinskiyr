package Generics.WildcardDemo.Wrapper.Wrapper;

/**
 * Created by user on 26.01.17.
 */
public class wrapper_with_generic {
    static class Wrapper<T> {
        T object;

        Wrapper(T object) {
            this.object = object;
        }

        T getObject() {
            return object;
        }

        void showType() {
            System.out.println(object.getClass().getName());
        }

        @Override
        public String toString() {
            return "Wrapper{" +
                    "object=" + object +
                    '}';
        }
    }

        public static void main(String[] args) {

            Wrapper<Integer> intWrapper = new Wrapper<>(88);
            int intVal = (Integer)intWrapper.getObject();
            System.out.println(intWrapper);
            Wrapper<String> stringWrapper = new Wrapper<>("Value"); //runtime exception
            String stringVal = stringWrapper.getObject();

//            intWrapper = stringWrapper; //компилятор (а не рантайм выдает ошибку

//            int intVal3 = (Integer)intWrapper.getObject(); //тк здесь лежит строка


        }
    }
