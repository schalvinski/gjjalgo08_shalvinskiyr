package Generics.WildcardDemo.Wrapper.Wrapper;

/**
 * Created by user on 26.01.17.
 */
public class wrapper {
    static class Wrapper {
        Object object;

        Wrapper(Object object) {
            this.object = object;
        }

        Object getObject() {
            return object;
        }

        void showType() {
            System.out.println(object.getClass().getName());
        }

        @Override
        public String toString() {
            return "Wrapper{" +
                    "object=" + object +
                    '}';
        }
    }

        public static void main(String[] args) {

            Wrapper intWrapper = new Wrapper(88);
            int intVal = (Integer)intWrapper.getObject();
            System.out.println(intWrapper);
            Wrapper stringWrapper = new Wrapper("Value"); //runtime exception
            String stringVal = (String)stringWrapper.getObject();

            intWrapper = stringWrapper;

            int intVal3 = (Integer)intWrapper.getObject(); //тк здесь лежит строка


        }
    }
