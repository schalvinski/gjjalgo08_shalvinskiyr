package Generics.WildcardDemo.boxprinter.boxprinter.java;

/**
 * Created by user on 26.01.17.
 */


class BoxPrinter_2<T> {
    private T val;

    public BoxPrinter_2(T arg) {
        val = arg;
    }


    public String toString() {
        return "{" + val + "}";
    }

    public T getValue() {
        return val;
    }
}

class Test {
    public static void main(String[] args) {
        BoxPrinter_2<Integer> value1 = new BoxPrinter_2<Integer>(new Integer(10));
        System.out.println(value1);
        Integer intValue1 = value1.getValue();
        BoxPrinter_2<String> value2 = new BoxPrinter_2<String>("Hello world");
        System.out.println(value2);

        // Здесь повторяется ошибка предыдущего фрагмента кода

//        Integer intValue2 = value2.getValue();
    }
}
