package Generics.WildcardDemo.boxprinter.boxprinter.java;

/**
 * Created by user on 26.01.17.
 */

class BoxPrinter {
    private Object val;

    public BoxPrinter(Object arg) {
        val = arg;
    }

//    public String toString() {
//        return "{" + val + "}";
//    }


    @Override
    public String toString() {
        return "BoxPrinter{" +
                "val=" + val +
                '}';
    }

    public Object getValue() {
        return val;
    }

}

class Test2 {
    public static void main(String[] args) {
        BoxPrinter value1 = new BoxPrinter(new Integer(10));
        System.out.println(value1);
        Integer intValue1 = (Integer) value1.getValue(); //не печатается
        BoxPrinter value2 = new BoxPrinter("Hello world");
        System.out.println(value2);

        // Здесь программист допустил ошибку, присваивая
        // переменной типа Integer значение типа String.
//        Integer intValue2 = (Integer) value2.getValue();
    }
}
