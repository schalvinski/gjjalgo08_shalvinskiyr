package Generics.WildcardDemo;

import java.util.ArrayList;

/**
 * Created by roman on 30.07.2016.
 */

class Machine1{
    @Override
    public String toString() {
        return "I am machine";
    }
    public void start(){
        System.out.println("Machine starting");
    }
}
class   Camera1 extends Machine1{//)extend smay not place
    @Override
    public String toString() {
        return "I am camera";
    }
    public void snap(){
        System.out.println("Machine snapping");
    }

}
public class App2 {

    public static void main(String[] args) {


        ArrayList<Machine1> list1 = new ArrayList<Machine1>();
        list1.add(new Machine1());
        list1.add(new Machine1());

        showList(list1);

        ArrayList<Camera1> list2 = new ArrayList<>();
        list2.add(new Camera1());
        list2.add(new Camera1());

        showList(list2);
        showList2(list1);

    }
    public static void showList(ArrayList<? extends Machine1> list) {//)может принимать любой тип класса
        for (Object value : list) {//)вместо Object было String
            System.out.println(value);

        }
    }
    public static void showList2(ArrayList<? super Camera1> list) {//)может принимать любой тип класса
        for (Object value : list) {//)вместо Object было String
            System.out.println(value);
        }
    }
}
//    public static void main(String[] args) {
//
//
//        ArrayList<String> list = new ArrayList<>();
//        list.add("one");
//        list.add("two");
//
//        showList(list);
//
//    }


