package ConstructorBoxDemo6.BoxDemo4;

// ������ ��⮤ volume() �����頥� ��ꥬ ��ࠫ����������.
class Box {
	double width;
	double height;
	double depth;

	// ���᫥��� � �����饭�� ��ꥬ�
	double volume() {
		return width * height * depth;
	}
}

class BoxDemo4 {
	public static void main(String args[]) {
		Box mybox1 = new Box();
		Box mybox2 = new Box();
		double vol;

		// ��ᢠ������ ���祭�� ��६����� ������� mybox1
		mybox1.width = 10;
		mybox1.height = 20;
		mybox1.depth = 15;

		// ��ᢠ������ ��㣨� ���祭�� ��६���� ������� mybox2
		mybox2.width = 3;
		mybox2.height = 6;
		mybox2.depth = 9;

		// значение переменной volume
		vol = mybox1.volume();
		System.out.println("��ꥬ ࠢ��: " + vol);

		//
		vol = mybox2.volume();
		System.out.println("��ꥬ ࠢ��: " + vol);
	}
}