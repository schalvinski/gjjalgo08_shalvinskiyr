package ConstructorBoxDemo6.BoxDemo7;

/* � �⮩ �ணࠬ�� ����� Box �ᯮ���� ��������� � ��ࠬ��ࠬ�
	��� ���樠����樨 ࠧ��஢ ��ࠫ���������� */
class Box {
	double width;
	double height;
	double depth;

	Box(double w, double h, double d) {
		width = w;
		height = h;
		depth = d;
	}

	double volume() {
		return width * height * depth;
	}
}

class BoxDemo7 {
	public static void main(String args[]) {
		// �������, १�ࢨ஢���� � ���樠������ ��ꥪ⮢ Box
		Box mybox1 = new Box(10, 15, 20);
		Box mybox2 = new Box(3, 6, 9);
		double vol;

		vol = mybox1.volume();
		System.out.println("��ꥬ ࠢ��: " + vol);

		vol = mybox2.volume();
		System.out.println("��ꥬ ࠢ��: " + vol);
	}
}