package ConstructorBoxDemo6;
//)
/**
 * Created by user on 17.07.16.
 */
public class SuperConstuctor {
    public static void main(String[] args) {
        //        A a = new A();
        //печатет только конструкто А
        B a = new B();
        //печатет А и Б
        C c = new C();
        //печатет А и Б C


    }

}

class A{ //Суперкласс

    public A(){//констрактор нужен чтоб давать значения переменным
        System.out.println("Inside A's constructor");
    }

}

class B extends A{

    public B(){ //конструктор не знает как работать с переменными к-рые он наследует от А, а метод знает
        System.out.println("Inside B's constructor");
    }

}

class C extends B{

    public C(){
        System.out.println("Inside C's constructor");
    }

}
