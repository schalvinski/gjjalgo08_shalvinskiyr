package ConstructorBoxDemo6.Ex5changlengthargument;

/**
 * Created by roman on 06.09.2016.
 */
public class Task {
    public static void main(String[] args) {
        System.out.println("Забито голов за тренировку по дням.");
        System.out.println("Понедельник");
        System.out.println(Task.hokkey(true,false,true,false));
        System.out.println("Вторник");
        System.out.println(Task.hokkey(false,true,false));
        System.out.println("Среда");
        System.out.println(Task.hokkey(true,false,false));
        System.out.println("Четверг");
        System.out.println(Task.hokkey(true,false,true,false,false,true));
        System.out.println("Пятница");
        System.out.println(Task.hokkey(true,false,true,false));
        System.out.println("Суббота");
        System.out.println(Task.hokkey(true,false,true,false,true,true,false));
        System.out.println("Восересение");
        System.out.println(Task.hokkey(true,false,true));
    }

    public static int hokkey(boolean...v){
        int countGoals = 0;
        for(boolean a : v){
            if (a){
                countGoals = countGoals + 1;
            }
        }
        return countGoals;
    }
}
