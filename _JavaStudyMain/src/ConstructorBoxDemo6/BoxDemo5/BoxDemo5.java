package ConstructorBoxDemo6.BoxDemo5;


// This program uses a parameterized method.
class Box {
	double width;
	double height;
	double depth;

	// ���᫥��� � �����饭�� ��ꥬ�
	double volume() {
		return width * height * depth;
	}

	// ��⠭���� ࠧ��஢ ��ࠫ����������
	void setDim(double w, double h, double d) {
		width = w;
		height = h;
		depth = d;
	}
}

class BoxDemo5 {
	public static void main(String args[]) {
		Box mybox1 = new Box();
		Box mybox2 = new Box();
		double vol;

		// ���樠������ ������� ������� Box
		mybox1.setDim(10, 15, 20);
		mybox2.setDim(3, 6, 9);

		// ����祭�� ��ꥬ� ��ࢮ�� ��ࠫ����������
		vol = mybox1.volume();
		System.out.println("��ꥬ ࠢ��: " + vol);

		// ����祭�� ��ꥬ� ��ண� ��ࠫ����������
		vol = mybox2.volume();
		System.out.println("��ꥬ ࠢ��: " + vol);
	}
}