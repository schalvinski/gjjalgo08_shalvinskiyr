package ConstructorBoxDemo6.ThisSuper;


/**
 * Created by user on 06.02.17.
 */
class Ok {
    String a;
    String b;
    int no = 1;
    public void displayNo(){
        int no = 5;
        System.out.println(no);//this.np = 1 ; no = 5
    }
    Ok(){
    }
    Ok(String ab,String bc){
        this.a=ab;
        this.b=bc;
    }
}
class Ok1{
    public static void main(String[] args) {
        Ok ok = new Ok();
        ok.displayNo();

        Ok ok1 = new Ok("sda","asd");
        ok1.displayNo();

    }
}
