package Tests_quizful;

/**
 * Created by user on 24.05.17.
 */
public class SsilkaZnachenie {
    static String str = "Value 1";

    public static void changeIt(String s) {
        s = "Value 2";
    }

    public static void main(String[] args) {
        changeIt(str);
        System.out.println(str);
    }
}


/*Ответ - Value 1.

Раньше я думал, что раз String не примитив, то будет происходить передача по ссылке и
новое значение ("Value 2") будет успешно присвоено.

В Java все передается по значению: и примитивы, и ссылки.
Т.о. внутрь функции передается копия ссылки, которая ссылается на "Value 1".
Присваивание данной копии значения другой ссылки (после чего она будет ссылаться на "Value 2")
не влияет на объект, на который она изначально ссылалась.

Объект можно изменить по ссылке (если он не immutable как String):*/