package Lambda.javabrains_with_lambda;


/**
 * Created by user on 16.05.17.
 */


public class Greeter {
    void greet(Greeting greeting) {
        greeting.perform();
    }

    public static void main(String[] args) {
        Greeter greeter = new Greeter();
        HelloWorldGreeting helloWorld = new HelloWorldGreeting();
        greeter.greet(helloWorld);//нельзя убрать интерфейс//

        Greeting myLambdaFunction = () -> {
            System.out.println("Hello World1111!");
        };
        myLambdaFunction.perform();
        Addfunction addfunction = (int a, int b) -> a + b;
    }
}


interface Addfunction {
    int add(int x,int y);
}