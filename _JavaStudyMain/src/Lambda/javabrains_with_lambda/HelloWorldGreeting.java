package Lambda.javabrains_with_lambda;

/**
 * Created by user on 16.05.17.
 */
public class HelloWorldGreeting implements Greeting {

    @Override
    public void perform() {
        System.out.println("Hello world");
    }
}
