package Lambda.lambda;

import java.util.function.BooleanSupplier;

/**
 * Created by user on 29.05.17.
 */
public class lambda_without_parameters {
    public static void main(String[] args) {
        BooleanSupplier bs = () -> false; // интерфейс
        System.out.println(bs.getAsBoolean());
        int x = 3, y = 1;
        bs = () -> x > y;//тк всего один метод
        /* bs = new BooleanSupplier() {
            @Override
            public boolean getAsBoolean() {
                return x > y;
            }
        };x*/
        System.out.println(bs.getAsBoolean());
        int z = 1, v = 0;
        bs = () -> z > v;
        System.out.println(bs.getAsBoolean());
    }
}