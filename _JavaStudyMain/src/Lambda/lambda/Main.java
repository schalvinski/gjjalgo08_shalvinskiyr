package Lambda.lambda;

/**
 * Created by user on 29.05.17.
 */
public class Main {
    /*w  w  w. ja  va  2  s .  com*/
    public static void main(String[] args) {
        MyIntegerCalculator myIntegerCalculator = (Integer s1) -> s1 * 2;

//        MyIntegerCalculator myIntegerCalculator = (s1) -> s1 * 2;//можно убрать тип переменной

        System.out.println("1- Result x2 : " + myIntegerCalculator.calcIt(5));


        /*        MyIntegerCalculator myIntegerCalculator = new MyIntegerCalculator() { //добавляется анонимный класс
            @Override
            public Integer calcIt(Integer s1) {//убирается название метода
                return s1 * 2;//вместо return "стрелочка"
            }
        };
*/
    }
}

interface MyIntegerCalculator {
    public Integer calcIt(Integer s1);
}
