package Lambda.lambda;

/**
 * Created by user on 29.05.17.
 */
public class LambdaBlock {
    public static void main(String[] argv) {
        Processor stringProcessor = (String str) -> str.length();
        String name = "Java Lambda";
        int length = stringProcessor.getStringLength(name);
        System.out.println(length);// www . j a  va 2  s. co m
//The following lambda expression uses a block statement and use return statement to return the sum.
    }
}

@FunctionalInterface
interface Processor {
    int getStringLength(String str);
}
