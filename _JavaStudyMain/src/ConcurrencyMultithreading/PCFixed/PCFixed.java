package ConcurrencyMultithreading.PCFixed;

// Правильная реализация поставщика и потребителя.
class Home {
    int n;
    boolean valueSet = false;

    synchronized int get() {
        while (valueSet == false) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException перехвачено");
            }
        }

        System.out.println("Получено: " + n);
        valueSet = false;
        notify();
        return n;
    }

    synchronized void put(int n) {
        while (valueSet == true) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException перехвачено");
            }
        }

        this.n = n;
        this.valueSet = true;
        System.out.println("Отправлено: " + n);
        notify();
    }
}

class Producer implements Runnable {
    Home q;

    Producer(Home q) {
        this.q = q;
        new Thread(this, "Поставщик").start();
    }

    public void run() {
        int i = 0;
        while (true) {
            q.put(i++);
        }
    }
}

class Consumer implements Runnable {
    Home q;

    Consumer(Home q) {
        this.q = q;
        new Thread(this, "Потребитель").start();//в конструктор помимо придание значений, также запускается поток
    }

    public void run() {
        while (true) {
            q.get();
        }
    }
}

class PCFixed {
    public static void main(String args[]) {
        Home q = new Home();
        new Producer(q);//новый конструктор с параметрами int = o,valueset=false
        new Consumer(q);

        System.out.println("Для останова нажмите Control-C.");
    }
}