package ConcurrencyMultithreading.ExecutorService;

/**
 * Created by user on 11.11.16.
 */

public class Consumer implements Runnable
{

    private String name;
    private Broker broker;


    public Consumer(String name, Broker broker)
    {
        this.name = name;
        this.broker = broker;
    }


    @Override
    public void run()
    {
        try
        {
            Integer data = broker.get();

            while (broker.continueProducing || data != null)
            {
                Thread.sleep(2000);
                System.out.println("Consumer " + this.name + " processed data from broker: " + data);

                data = broker.get();
            }


            System.out.println("Comsumer " + this.name + " finished its job; terminating.");
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
    }

}
