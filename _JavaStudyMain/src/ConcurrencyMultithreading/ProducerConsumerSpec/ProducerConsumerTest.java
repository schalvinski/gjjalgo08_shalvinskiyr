package ConcurrencyMultithreading.ProducerConsumerSpec;

import static java.lang.Thread.sleep;

/**
 * Created by user on 22.10.16.
 */
public class ProducerConsumerTest {
    public static void main(String[] args) {
        Exchange exch = new Exchange();
        Producer p1 = new Producer(exch, 1);//2 - имя
        Consumer c1 = new Consumer(exch, 2);
        p1.start();
        c1.start();
    }
}
class Exchange {
    private int contents;
    private boolean consumed = true;

    public synchronized void put(int value) {
        while (consumed == false) {
            try {
                wait();
            }
            catch (InterruptedException e) {
            }
        }
        this.contents = value;
        this.consumed = false;
        notify();
    }
    public synchronized int get() {
        while (consumed == true) {
            try {
                wait();//ждет другой поток
            }
            catch (InterruptedException ignored) {
            }
        }
        try {
            sleep(5000);
        } catch (InterruptedException e) { }
        consumed = true;//
        notifyAll();
        return contents;
    }
}
class Producer extends Thread {
    private Exchange exch;
    private int number;

    public Producer(Exchange exch, int number) {
        this.exch = exch;
        this.number = number;
    }
    public void run() {
        for (int i = 0; i < 10; i++) {//передача цифр i
            exch.put(i);
            System.out.println("Producer #" + this.number
                    + " put: " + i);
            try {
                sleep(7000);
            } catch (InterruptedException e) { }
        }
    }
}
class Consumer extends Thread {
    private Exchange exch;
    private int number;
    public Consumer(Exchange exch, int number) {
        this.exch = exch;
        this.number = number;
    }
    public void run() {
        int value;
        for (int i = 0; i < 10; i++) {
            value = exch.get();
            System.out.println("Consumer #"
                    + this.number
                    + " got: " + value);
        }
    }
}