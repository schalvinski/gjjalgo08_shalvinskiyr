package ConcurrencyMultithreading.ExecutorSpec;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ConcurrencyMultithreading.ExecutorSpec.StopWatch.getElapsedTime;
import static ConcurrencyMultithreading.ExecutorSpec.StopWatch.start;

/**
 * Created by user on 22.10.16.
 */
public class Executor {
    public static void main(String[] args) {
        start();
        try {
            ExecutorService executor = Executors.newFixedThreadPool(2);//создаем пул потоков
            Runnable command = new Task();
            Runnable thread2 = new Task1();

            for (int i = 0; i < 100; i++) {
                executor.execute(command);
            }
            for (int i = 0; i < 100; i++) {
                executor.execute(thread2);
            }

            executor.execute(command);
            executor.execute(thread2);
            executor.execute(command);
            executor.execute(thread2);
            executor.execute(thread2);
            executor.execute(thread2);
            executor.execute(thread2);




            Thread.sleep(10000);
            executor.shutdown();//завершает проц
//            executor.execute(command);//новые задачи после shutdown запустить нельзя!



        }catch (Exception exc){
            exc.printStackTrace();
        }
        System.out.println("Time Elapsed    "+getElapsedTime());
    }
}
