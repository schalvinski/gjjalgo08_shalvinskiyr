package ConcurrencyMultithreading.ExecutorSpec;

import static ConcurrencyMultithreading.ExecutorSpec.StopWatch.getElapsedTime;

/**
 * Created by user on 22.10.16.
 */
public class Task1 implements Runnable {

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        System.out.println(this.getClass().getName()+ "Thread 2 is running"+this.getClass().getSimpleName()+">>");

//        try {
//            Thread.sleep(5000);//3 сек
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.println("<<Thread1 stopped");
        System.out.println("Time Elapsed    "+getElapsedTime());
    }
}
