import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by roman on 28.07.2016.
 */
public class Server {

    public static void main(String[] args) throws InterruptedException, IOException {

        try (ServerSocket ss = new ServerSocket(8080)) {

            for (; ; ) {
                Socket socket = ss.accept();
                InputStream is = socket.getInputStream();
                Scanner scanner = new Scanner(is);
                for (; scanner.hasNextLine(); ) {
                    String line = scanner.nextLine();
                    System.out.println(line.length() + " " + line);
                    if (line.length() == 0)
                        break;
                }
    //            String getHeader = scanner.nextLine();
    //            System.out.println(getHeader);


                OutputStream os = socket.getOutputStream();
                os.write("Hello".getBytes());//в хроме выводится хелло
                socket.close(); //прочитали запрос

            }
        }

    }

}
