package mathematical_operators_bitwiseoperators_типыданных.autoboxing.valueOf;

/**
 * Created by user on 04.02.17.
 */
public class ValueOf {

    public static void main(String args[]) {
        Integer x =Integer.valueOf(9);
        Double c = Double.valueOf("5");
        Float a = Float.valueOf("80");
        Integer b = Integer.valueOf("444",16);

        System.out.println(x);
        System.out.println(c);
        System.out.println(a);
        System.out.println(b);


        int i=10;
        float f = 10.0f;
        long l = 10;
        double d=10.0d;
        char z='a';
        boolean g = true;
        Object o = new String("Hello World");

  /* convert int to String */
        System.out.println( String.valueOf(i) );
  /* convert float to String */
        System.out.println( String.valueOf(f) );
  /* convert long to String */
        System.out.println( String.valueOf(l) );
  /* convert double to String */
        System.out.println( String.valueOf(d) );
  /* convert char to String */
        System.out.println( String.valueOf(c) );
  /* convert boolean to String */
        System.out.println( String.valueOf(b) );
  /* convert Object to String */
        System.out.println( String.valueOf(o) );
    }
}