package mathematical_operators_bitwiseoperators_типыданных.autoboxing;

/**
 * Created by user on 28.01.17.
 */
public class Compare {
    public static void main(String[] args) {
        Integer istInt = new Integer(1); // тк два новых объекта созданы
        Integer secondInt = new Integer(1);
        if (istInt == secondInt) {
            System.out.println("both one are equal");
        } else {
            System.out.println("Both one are not equal");
        }
    }
}
/*
Вы должны помнить о следующих вещах, используя Автоупаковку:
Как мы знаем, любая хорошая функция имеет недостаток. Автоупаковка не является исключением в этом отношении. Некоторый важные замечания, которые должен учитывать разработчик при использовании этой функции:
— Сравнивая объекты оператором ‘==’ может возникнуть путаница, так как он может применяться к примитивным типам и объектам. Когда этот оператор применяется к объектам, он фактически сравнивает ссылки на объекты а не сами объекты.*/