package mathematical_operators_bitwiseoperators_типыданных.autoboxing.AutoBox5;

// Автоупаковка/распаковка Boolean и Character.
class AutoBox5 {
	public static void main(String args[]) {
		// Автоупаковка/распаковка boolean.
		Boolean b = true;

		// b автоматически распаковывается
		// при использовании в условном выражении if.
		if (b) {
			System.out.println("b равна true");
		}

		// Автоупаковка/распаковка char.
		Character ch = 'x'; // упаковка char
		char ch2 = ch; // распаковка char

		System.out.println("ch2 равна " + ch2);
	}
}