package mathematical_operators_bitwiseoperators_типыданных.autoboxing.AutoBox;

// Демонстрация автоупаковки/автораспаковки
class AutoBox {
	public static void main(String arg[]) {
		Integer iOb = 100; // автоупаковка int

		int i = iOb; // актораспаковка

		System.out.println(i + " " + iOb); // отображает 100 100
	}
}