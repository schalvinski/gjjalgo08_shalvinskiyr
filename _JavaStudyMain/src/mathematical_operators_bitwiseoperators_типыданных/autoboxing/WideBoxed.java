package mathematical_operators_bitwiseoperators_типыданных.autoboxing;

/**
 * Created by user on 28.01.17.
 */
    public class WideBoxed {

        static void methodWide(int i) {
            System.out.println("int");
        }

        static void methodWide(Integer i) {
            System.out.println("Integer");
        }

        public static void main(String[] args) {
            short shVal = 25;
            methodWide(shVal);
        }
    }
/**
 * Автоупаковка и распаковка при перегрузке метода

 Автоупаковка и распаковка выполняется при перегрузке метода на основании следующих правил:
 — Расширение «побеждает» упаковку — В ситуации, когда становится выбор между расширением и упаковкой, расширение предпочтительней.
 Вывод программы — тип int

 — Расширение побеждает переменное количество аргументов – В ситуации, когда становится выбор между расширением и переменным количеством аргументов, расширение предпочтительней.
 */
