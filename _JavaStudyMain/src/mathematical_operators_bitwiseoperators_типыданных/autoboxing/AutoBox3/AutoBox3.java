package mathematical_operators_bitwiseoperators_типыданных.autoboxing.AutoBox3;

// Автоупаковка/распаковка происходит в выражениях.
class AutoBox3 {
	public static void main(String args[]) {
		Integer iOb, iOb2;
		int i;

		iOb = 100;
		System.out.println("Исходное значение iOb: " + iOb);

		// Следующее автоматически распаковывает iOb,
		// выполняет его приращение, затем повторно
		// упаковывает результат обратно в iOb.
		++iOb;
		System.out.println("После ++iOb: " + iOb);

		// Здесь iOb распаковано, выражение вычисляется,
		// а результат снова упаковывается и
		// присваивается iOb2.
		iOb2 = iOb + (iOb / 3);
		System.out.println("iOb2 после выражения: " + iOb2);

		// Вычисляется то же самое выражение,
		// но результат не упаковывается.
		i = iOb + (iOb / 3);
		System.out.println("i после выражения: " + i);
	}
}