package mathematical_operators_bitwiseoperators_типыданных.типы_данных;

/**
 * Created by user on 18.11.16.
 */
public class DoubleFloat
{
    public static void main (String [] arg)
    {
        double b1 = 3.62;
        double b2 = 4.12 + b1;
        // При использовании типа float необходимо использовать приведение типа,
        // так как дробные числа - это литералы типа double
        float pi = (float) 3.14; //        float  pi = 3.14f;

        double d = 27;
        double c = pi * d;
        System.out.println (c);


    }
}
