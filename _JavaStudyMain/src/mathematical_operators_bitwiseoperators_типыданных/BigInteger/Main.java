package mathematical_operators_bitwiseoperators_типыданных.BigInteger;

/**
 * Created by user on 06.02.17.
 */


import java.math.BigInteger;

public class Main {
    public static void main(String[] args) throws Exception {

        BigInteger mod = new BigInteger(1, new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9});

        System.out.println(mod);
    }
}
