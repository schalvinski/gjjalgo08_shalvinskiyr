package mathematical_operators_bitwiseoperators;

/**
 * Created by user on 02.11.16.
 */
import java.io.*;
public class Treugolnik
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String x = br.readLine();
        String y = br.readLine();
        String z = br.readLine();
        int a = Integer.parseInt(x);
        int b = Integer.parseInt(y);
        int c = Integer.parseInt(z);
        if ((a >= b + c)||(b >= a + c)||(c >= a + b))
            System.out.println("Треугольник не существует");
        else
            System.out.println("Треугольник существует");
    }
}
