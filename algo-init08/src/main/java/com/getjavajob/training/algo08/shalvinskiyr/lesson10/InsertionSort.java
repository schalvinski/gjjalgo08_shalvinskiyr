package com.getjavajob.training.algo08.shalvinskiyr.lesson10;

/**
 * Created by roman on 23.11.16.
 */
public class InsertionSort {
    static int[] doInsertionSort(int[] input) {
        int temp;
        for (int i = 1; i < input.length; i++) {
            for (int j = i; j > 0; j--) {
                if (input[j] < input[j - 1]) {
                    temp = input[j];
                    input[j] = input[j - 1];
                    input[j - 1] = temp;
                }
            }
        }
        return input;
    }
}
