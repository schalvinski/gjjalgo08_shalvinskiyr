package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 12.10.16.
 */
public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        testExpressionCalculator();
    }

    private static void testExpressionCalculator() {
        ExpressionCalculator calc = new ExpressionCalculator();
        assertEquals("testExpressionCalculator", 24, calc.eval("(3+(5*3+3*2))"));

    }
}
