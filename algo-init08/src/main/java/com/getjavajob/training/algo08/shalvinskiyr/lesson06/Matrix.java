package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

/**
 * Created by roman on 26.11.16.
 */
public interface Matrix<V> {
    V get(int i, int j);

    void set(int i, int j, V value);
}
