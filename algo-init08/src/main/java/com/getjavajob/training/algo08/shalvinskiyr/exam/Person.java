package com.getjavajob.training.algo08.shalvinskiyr.exam;

/**
 * Created by user on 24.01.17.
 */
@Deprecated
public class Person {

    public String name;

    public int age;

    public Person(String name, int age)
    {
        this.name=name;
        this.age=age;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
    //    public String toString()
//    {
//        return this.name;
//    }
//
//    public int compareTo(Person o)
//    {
//        int myLength=name.length();
//        int oLength=o.name.length();
//        if(myLength == oLength) return 0;
//        if(oLength > myLength) return -1;
//        return 1;
//
//    }



}
