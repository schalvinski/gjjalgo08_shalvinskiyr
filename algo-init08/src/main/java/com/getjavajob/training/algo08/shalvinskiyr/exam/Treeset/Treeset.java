package com.getjavajob.training.algo08.shalvinskiyr.exam.Treeset;

import java.util.TreeSet;

/**
 * Created by user on 22.01.17.
 */
public class Treeset {
    public static void main(String[] args) {


        /*
        ThirdPartyType a = new ...;
        ThirdPartyType b = new ...;
        ThirdPartyType c = new ...;
        hashSet.add(a);
        hashSet.add(b);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 3

        hashSet.add(b);
        hashSet.add(a);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 2 - todo when is it possible?*/

        TreeSet<Object> treeSet = new TreeSet<>();
        treeSet.add(new String("1"));
        treeSet.add((new String("2")));
        treeSet.add((new String("1")));
        System.out.println(treeSet.size()); // size ? todo несколько случаев и почему?

//        LinkedList ll = new LinkedList();
//        List l = new LinkedList();
//        l.add(new Object());
//        l.add(new Object());
//        l.add(new Object());
//        l.add(new Object());
//
////        System.out.println("Tredje elmentet från toppen : "+ll.get(2));
//        System.out.println(ll);
//        System.out.println(ll.size());
//
//        System.out.println(ll);
//        System.out.println(ll.size());

//        boolean containsDuplicates(int[] numbers) {} // 3 diff solutions with 2 different speeds,
// 1 of them must not use O(n) memory, all of them faster n^2
        //todo массив - true - если есть модифицировать входной массив алгоритм сложность оценка памяти
        //todo как сложность считается
        //todo lesson02

//        m(Arrays.asList("Ваня", "Таня", "Саша", "Петя", "Даша"));

    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     * <p>
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     * <p>
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     * <p>
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     * <p>
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     * <p>
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */


//    static void m(List<?> l) {
//            l.add(new Object());
//        }
//
//         static void m(List<Object> l) {
//            l.add("string");
//             System.out.println(l);
//        }
}

