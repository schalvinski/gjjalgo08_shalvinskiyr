package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 27.07.16.
 */
public class MatrixImplTest {
    public static void main(String[] args) {
        testMatrixFill();
    }

    private static void testMatrixFill() {
        MatrixImpl matrix = new MatrixImpl(new HashMap<KeyMatrix, Integer>());
        int number = 1_000_000;
        List<Integer> compare = new ArrayList<>();
        List<Object> res = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            matrix.set(i, number, i);
            compare.add(i);
        }
        for (int i = 0; i < 10; i++) {
            res.add(matrix.get(i, number));
        }
        assertEquals("\nMatrixTaskTest.testMatrixFill", compare, res);
    }
}
