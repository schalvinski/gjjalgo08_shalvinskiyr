package com.getjavajob.training.algo08.shalvinskiyr.lesson02;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class HelloWorldAlbanian {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String result = new String(("\u0050\u00eb\u0072\u0073\u0068\u00eb\u006e\u0064\u0065\u0074\u006a" +
                "\u0065\u0020\u0062\u006f\u0074\u00eb\u0021").getBytes("Cp1250"), "Cp1250");
        System.out.println(result);
        String result1 = new String(("\u0050\u00eb\u0072\u0073\u0068\u00eb\u006e\u0064\u0065\u0074\u006a" +
                "\u0065\u0020\u0062\u006f\u0074\u00eb\u0021").getBytes("UTF8"));
        System.out.println(result1);

        System.out.println(Charset.defaultCharset());
    }
}
