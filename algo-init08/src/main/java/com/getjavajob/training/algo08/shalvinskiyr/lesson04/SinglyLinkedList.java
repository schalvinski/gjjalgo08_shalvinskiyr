package com.getjavajob.training.algo08.shalvinskiyr.lesson04;

/**
 * Created by roman on 23.11.16.
 */
public class SinglyLinkedList<V> {
    public Node<V> head;

    public void add(V value) {
        Node<V> node = new Node<>();
        node.val = value;
        if (head == null) {
            head = node;
        } else {
            node.next = head;
            head = node;
        }
    }
//    public void reverse() {
//        Node currNode = head;
//        Node prevNode = null;
//        Node nextNode = null;
//        while (currNode != null) {
//            nextNode = currNode.next;   // store nextNode item
//            swap(prevNode, currNode);
//            prevNode = currNode;             // increment also pre
//            currNode = nextNode;        // increment current
//        }
//        head = prevNode;
//    }
//public void swap(Node prevNode, Node currNode) {
//    currNode.next = prevNode;
//}


    public Node<V> reverse() {
        if (head == null || head.next == null) return head;
        swap();
        return null;
    }

    public void swap() {
        Node currNode = head;
        Node prevNode = null;
        Node nextNode = null;
        while (currNode != null) {
            nextNode = currNode.next;   // store nextNode item
            currNode.next = prevNode;
            prevNode = currNode;             // increment also pre
            currNode = nextNode;        // increment current
        }
        head = prevNode;
    }

    static class Node<V> {
        public Node<V> next;
        public V val;
    }
//    public class Node<V> {
//        public Node(V val){
//            this.val = val;
//        }
//        Node<V> next;
//        V val;
//    }

}

