package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

public class LinkedListStack<V> implements Stack<V> {

    private int top = -1;
    private Node<V> head;

    public void push(V e) {
        Node node = new Node();
        node.val = e;
        node.next = head;
        head = node;
    }

    @Override
    public V pop() {
        Node<V> temp = head;
        head = head.next;
        return temp.val;
    }

    @Override
    public String toString() {
        String str = "";
        Node<V> node = head;
        while (node != null) {
            str += node.val;
            node = node.next;
        }
        return str;
    }

    static class Node<V> {
        public Node<V> next;
        public V val;
    }
}
