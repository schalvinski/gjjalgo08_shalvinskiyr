package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

/**
 * Created by roman on 10.08.16.
 */

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

public class LinkedListQueueTest {

    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    private static void testAdd() {
        LinkedListQueue<Integer> linkQueue = new LinkedListQueue<>();
        linkQueue.add(13);
        linkQueue.add(12);
        linkQueue.add(11);
        assertEquals("LinkedListQueueTest.testAdd", "13 12 11 ", linkQueue.toString());
    }

    private static void testRemove() {
        LinkedListQueue<Integer> linkQueue = new LinkedListQueue<>();
        linkQueue.add(13);
        linkQueue.add(12);
        linkQueue.add(11);
        linkQueue.remove();
        assertEquals("LinkedListQueueTest.testRemove", "12 11 ", linkQueue.toString());
    }
}
