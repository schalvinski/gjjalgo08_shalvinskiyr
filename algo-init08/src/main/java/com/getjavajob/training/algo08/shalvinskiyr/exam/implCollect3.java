package com.getjavajob.training.algo08.shalvinskiyr.exam;

/**
 * Created by user on 22.01.17.
 */
public class implCollect3 {
    public static void main(String[] args) {
        System.out.println(containsDuplicates(new int[]{1, 5, 2, 3, 4, 5}));
    }

    static boolean containsDuplicates(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] == numbers[i]) return true;
            }
        }
        return false;
    }
}


//квардратичное время
//        boolean containsDuplicates(int[] numbers) {} // 3 diff solutions with 2 different speeds,
// 1 of them must not use O(n) memory, all of them faster n^2