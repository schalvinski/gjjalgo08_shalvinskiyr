package com.getjavajob.training.algo08.shalvinskiyr.lesson01;

/**
 * Created by admin on 09.08.2015.
 */
public class Task06 {
    public static void main(String[] args) {
        System.out.println("Calculate 2^n " + pow(2));
        System.out.println("Calculate 2^n + 2^m " + Integer.toBinaryString(pow2(3, 4))+
                "  1 << n " + Integer.toBinaryString(1 << 4 ) );
        System.out.println("resetLowerBits:"+Integer.toBinaryString(resetLowerBits(0b110011001, 3)));
        System.out.println("a & ~(1 << n - 1)" + Integer.toBinaryString((0b110011001 & (1 << 8 - 1))));//обнулили посл n бит числа
        System.out.println("returnNLowerBits "+Integer.toBinaryString(returnNLowerBits(0b110011101, 4))); //возвр число последних битов
        System.out.println("returnNthBit "+returnNthBit(0b110011101, 3));
        System.out.println(setNthBit0(0b110011101, 15));
        System.out.println("setNthBit1 " + setNthBit1(0b110011101, 3));
        System.out.println(getBinaryRepresentation((byte) 2));
    }

    /**
     * Calculate 2^n
     *
     * @param m
     * @return
     */
    public static int pow(int m) {//a
        return m < 31 && m > 0 ? 2 << m - 1 : 0;
    }

    /**
     * Calculates 2 ^ n + 2 ^ m
     *
     * @param n
     * @param m
     * @return
     */
    public static int pow2(int n, int m) {//b
        return 1 << n | 1 << m;
    }

    /**
     * reset n lower bits
     *
     * @param a
     * @param n
     * @return
     */
    public static int resetLowerBits(int a, int n) {
        return a & ~(1 << n - 1);
    } //0b110011001

    /**
     * set a's n-th bit with 1
     *
     * @param a
     * @param n
     * @return
     */
    public static int setNthBit1(int a, int n) {
        a = a | 1 << n; //
        return Integer.parseInt(Integer.toString(a, 2));
    }

    /**
     * invert n-th bit
     *
     * @param a
     * @param n
     * @return
     */
    public static int invertNthBit(int a, int n) {
        a ^= 1 << n;
        return Integer.parseInt(Integer.toString(a, 2));
    }

    /**
     * set a's n-th bit with 0
     *
     * @param a
     * @param n
     * @return
     */
    public static int setNthBit0(int a, int n) {//f
        a &= ~(1 << n);
        return Integer.parseInt(Integer.toString(a, 2));
    }

    /**
     * return n lower bits
     *
     * @param a
     * @param n
     * @return
     */
    public static int returnNLowerBits(int a, int n) {
        return a & ~(~0 << n);
    }

    public static int returnNthBit(int a, int n) {//h
        a &= (a >> n) & 1;
        return Integer.parseInt(Integer.toString(a, 2));
    }

    /**
     * Returns Bin representation
     *
     * @param a
     * @return
     */
    public static String getBinaryRepresentation(byte a) {
        StringBuilder sb = new StringBuilder();
        while (a != 0) {
            if (a % 2 == 0) {
                sb.append(0);
            } else {
                sb.append(1);
            }
            a = (byte) (a / 2);
        }
        return sb.reverse().toString();
    }
}
