package com.getjavajob.training.algo08.shalvinskiyr.lesson07;

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary.LinkedBinaryTree;

import java.util.*;

import static com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary.LinkedBinaryTree.root;

/**
 * Tree interface functionality
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {

    private LinkedBinaryTree.NodeImpl<E>[] array = new LinkedBinaryTree.NodeImpl[10000];
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return root() != null;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */

    public Iterable<Node<E>> preOrder() {
        Stack<Node<E>> stack = new Stack<>();
        Stack<Integer> numbers = new Stack<>();
        ArrayList<Node<E>> preList = new ArrayList<>();
        int k = 0;
        stack.push(array[0]);
        numbers.push(k);
        while (!stack.isEmpty()) {
            Node<E> current = stack.pop();
            k = numbers.pop();
            preList.add(current);
            if (array[2 * k + 2] != null) {
                stack.push(array[2 * k + 2]);
                numbers.push(2 * k + 2);
            }
            if (array[2 * k + 1] != null) {
                stack.push(array[2 * k + 1]);
                numbers.push(2 * k + 1);
            }
        }
        return preList;
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Iterable<Node<E>> postOrder() {
        Stack<Node<E>> preList = new Stack<>();
        Node<E> head = root();
        int k = 0;
        Stack<Node<E>> stack = new Stack<>();
        Stack<Integer> numbers = new Stack<>();
        if (head == null) {
            return preList;
        }
        numbers.push(k);
        stack.push(root());

        while (!stack.isEmpty()) {
            Node next = stack.peek();
            int m = numbers.peek();
            boolean finishedSubtrees = (array[2 * m + 2] == head || array[2 * m + 1] == head);
            boolean isLeaf = (array[2 * m + 2] == null && array[2 * m + 1] == null);
            if (finishedSubtrees || isLeaf) {
                stack.pop();
                numbers.pop();
                preList.add(next);
                head = next;
            } else {
                if (array[2 * m + 2] != null) {
                    stack.push(array[2 * m + 2]);
                    numbers.push(2 * m + 2);
                }
                if (array[2 * m + 1] != null) {
                    stack.push(array[2 * m + 1]);
                    numbers.push(2 * m + 1);
                }
            }
        }
        return preList;
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     * <p>
     * must be implemented here
     */
    public Iterable<Node<E>> breadthFirst() {
        List<Node<E>> breadthList = new ArrayList<>();
        if (root == null) {
            System.out.println("Empty tree");
        } else {
            Queue<Node<E>> q = new LinkedList<>();
            q.add(root());
            while (q.peek() != null) {
                LinkedBinaryTree.NodeImpl<E> temp = (LinkedBinaryTree.NodeImpl<E>) q.remove();
                breadthList.add(temp);
                if (temp.left != null) {
                    q.add(temp.left);
                }
                if (temp.right != null) {
                    q.add(temp.right);
                }
            }
        }
        return breadthList;
    }


    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
