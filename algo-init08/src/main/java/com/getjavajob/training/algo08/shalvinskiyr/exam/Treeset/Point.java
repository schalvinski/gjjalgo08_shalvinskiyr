// Simple immutable two-dimensional integer point class - Page 37
package com.getjavajob.training.algo08.shalvinskiyr.exam.Treeset;

public class Point {
	private final int x;

	public Point(int x) {
		this.x = x;
	}
	@Override
	public boolean equals(Object o) {
		if ((o instanceof Point))
			return true;
		Point p = (Point) o;
		return p.x == x ;
	}

	// Broken - violates Liskov substitution principle - Pages 39-40
	// @Override public boolean equals(Object o) {
	// if (o == null || o.getClass() != getClass())
	// return false;
	// Point p = (Point) o;
	// return p.x == x && p.y == y;
	// }

	// See Item 9
	@Override
	public int hashCode() {
		return 31 * x ;
	}

//	@Override
//	public boolean equals(Object o) {
//		if (this == o) return true;
//		if (o == null || getClass() != o.getClass()) return false;
//		Point point = (Point) o;
//		return x == point.x;
//	}



}
