package com.getjavajob.training.algo08.shalvinskiyr.lesson01;

import static com.getjavajob.training.algo08.shalvinskiyr.lesson01.Task06.resetLowerBits;
import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by admin on 12.08.2015.
 */
public class Task06Test {
    public static void main(String[] args) {
        testPow();
        testPow2();
        testMask();
        testSetNthBit1();
        testInvertNthBit();
        testSetNthBit0();
        testReturnNLowerBits();
        testReturnNthBit();
    }

    public static void testPow() {
        assertEquals("testPow", 32, Task06.pow(5));
    }

    public static void testPow2() {
        assertEquals("testPow2", 96, Task06.pow2(5, 6));
    }

    private static void testMask() {
        assertEquals("Task06Test.test3", "1000", Integer.toBinaryString(resetLowerBits(12, 3)));
    }

    public static void testSetNthBit1() {
        assertEquals("testSetNthBit1", 110011101, Task06.setNthBit1(0b110011101, 3));
    }

    public static void testInvertNthBit() {
        assertEquals("testInvertNthBit", 110010101, Task06.invertNthBit(0b110011101, 3));
    }

    public static void testSetNthBit0() {
        assertEquals("testSetNthBit0", 110010101, Task06.setNthBit0(0b110011101, 3));
    }

    public static void testReturnNLowerBits() {
        assertEquals("testReturnNLowerBits", "101", Integer.toBinaryString(Task06.returnNLowerBits(0b110011101, 3)));//_
    }

    public static void testReturnNthBit() {
        assertEquals("testReturnNthBit", 1, Task06.returnNthBit(0b110011101, 3));
    }

}
