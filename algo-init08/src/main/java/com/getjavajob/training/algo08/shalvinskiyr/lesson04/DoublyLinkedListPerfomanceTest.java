package com.getjavajob.training.algo08.shalvinskiyr.lesson04;

import com.getjavajob.training.algo08.shalvinskiyr.util.Time;

import java.util.LinkedList;

/**
 * Created by roman on 19.11.16.
 */
public class DoublyLinkedListPerfomanceTest {
    public static void main(String[] args) {

        DoublyLinkedList<Number> doublyLinkedList = new DoublyLinkedList<>();
        for (int i = 0; i < 10_000_000; i++) {
            doublyLinkedList.add(i);
        }
        LinkedList<Number> numbers = new LinkedList<>();
        for (int i = 0; i < 10_000_000; i++) {
            numbers.add(i);
        }
        addToBeign(doublyLinkedList, numbers);
        removeFromBegin(doublyLinkedList, numbers);
        addToMiddle(doublyLinkedList, numbers);
        removeFromMiddle(doublyLinkedList, numbers);
        addToEnd(doublyLinkedList, numbers);
        removeFromEnd(doublyLinkedList, numbers);
    }

    private static void addToBeign(DoublyLinkedList<Number> doublyLinkedList, LinkedList<Number> numbers) {
        System.out.println("Add/remove to/from the beginning test");

        Time.start();
        doublyLinkedList.add(0, 10_000);
        System.out.println("DLL.add(e): " + Time.getElapsedTime() + " ms");
        Time.start();
        numbers.add(0, 147);
        System.out.println("LinkedList.add(e): " + Time.getElapsedTime() + " ms");
        System.out.println("------------");
    }

    private static void removeFromBegin(DoublyLinkedList<Number> doublyLinkedList, LinkedList<Number> numbers) {
        Time.start();
        doublyLinkedList.remove(0);
        System.out.println("DLL.remove(e): " + Time.getElapsedTime());
        Time.start();
        numbers.remove(0);
        System.out.println("LinkedList.remove(e): " + Time.getElapsedTime() + " ms");
        System.out.println("****************************************************\n");
    }

    private static void addToMiddle(DoublyLinkedList<Number> doublyLinkedList, LinkedList<Number> numbers) {
        System.out.println("Addition/remove to/from the middle test");
        Time.start();
        doublyLinkedList.add(5000000, 147);
        System.out.println("DLL.add(e): " + Time.getElapsedTime() + " ms");
        Time.start();
        numbers.add(5000000, 147);
        System.out.println("LinkedList.add(e): " + Time.getElapsedTime() + " ms");
        System.out.println("------------");
    }

    private static void removeFromMiddle(DoublyLinkedList<Number> doublyLinkedList, LinkedList<Number> numbers) {
        Time.start();
        doublyLinkedList.remove(500000);
        System.out.println("DLL.remove(e): " + Time.getElapsedTime() + " ms");
        Time.start();
        numbers.remove(500000);
        System.out.println("LinkedList.remove(e): " + Time.getElapsedTime() + " ms");
        System.out.println("****************************************************\n");
    }

    private static void addToEnd(DoublyLinkedList<Number> doublyLinkedList, LinkedList<Number> numbers) {

        System.out.println("Addition/remove to/from the end test");
        Time.start();
        doublyLinkedList.add(doublyLinkedList.size() - 1, 147);
        System.out.println("DLL.add(e): " + Time.getElapsedTime() + " ms");
        Time.start();
        numbers.add(numbers.size() - 1, 147);
        System.out.println("LinkedList.add(e): " + Time.getElapsedTime() + " ms");
        System.out.println("------------");
    }

    private static void removeFromEnd(DoublyLinkedList<Number> doublyLinkedList, LinkedList<Number> numbers) {
        Time.start();
        doublyLinkedList.remove(doublyLinkedList.size() - 1);
        System.out.println("DLL.remove(e): " + Time.getElapsedTime() + " ms");
        Time.start();
        numbers.remove(numbers.size() - 1);
        System.out.println("LinkedList.remove(e): " + Time.getElapsedTime() + " ms");
        System.out.println("****************************************************\n");
    }
}
