package com.getjavajob.training.algo08.shalvinskiyr.lesson04;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by roman on 17.11.16.
 */
public class DoublyLinkedList<V> extends AbstractDll<V> implements List<V> {

    private int size;
    private Node<V> first;
    private Node<V> last;
    private int modCount;


    public DoublyLinkedList() {
        this.size = 0;
    }

    /**
     * adds new value to Dll
     *
     * @param val
     * @return
     */
    @Override
    public boolean add(V val) {
        if (isEmpty()) {
            Node<V> node = new Node<V>(null, null, val);
            first = node;
            last = node;
        } else {
            Node<V> node = new Node<V>(last, null, val);
            last.setNext(node);
            last = node;
        }
        size++;
        modCount++;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * @param i ll number
     * @param v value
     */
    @Override
    public void add(int i, V v) {
        if (i == size()) {
            add(v);
        } else {
            insertNode(v, iterate(i));
        }
    }

    /**
     * @param v
     * @param current
     */
    private void insertNode(V v, Node<V> current) {
        Node<V> previous = current.getPrevious();
        Node<V> inserted = new Node<V>(previous, current, v);
        current.setPrev(inserted);
        if (previous == null) {
            first = inserted;
        } else {
            previous.setNext(inserted);
        }
        size++;
        modCount++;
    }

    /**
     * @param i
     * @return
     */
    private Node<V> iterate(int i) {
        if (i > (size - 1) || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + " Max: " + (size - 1) + " Min: 0");
        } else if (i < size / 2) {
            Node<V> current = first;
            for (int j = 0; j < i; j++) {
                current = current.getNext();
            }
            return current;
        } else {
            Node<V> current = last;
            for (int j = size - 1; j > i; j--) {
                current = current.getPrevious();
            }
            return current;
        }
    }

    /**
     * @return
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * @param o
     * @return
     */
    @Override
    public boolean remove(Object o) {
        boolean bool = false;
        V v = (V) o;
        Node<V> current = first;

        for (int i = 0; i < size(); i++) {
            if (current.val.equals(v)) {
                removeNode(current);
                bool = true;
                size--;
            }
            current = current.getNext();
        }
        return bool;
    }

    /**
     * @param i
     * @return
     */
    @Override
    public V remove(int i) {
        Node<V> current = iterate(i);
        removeNode(current);
        size--;
        modCount++;
        return current.getVal();
    }

    /**
     * @param current
     */
    private void removeNode(Node<V> current) {
        if (current.equals(first)) {
            removeFirst();
        } else if (current.equals(last)) {
            removeLast();
        } else {
            current.getPrevious().setNext(current.getNext());
            current.getNext().setPrev(current.getPrevious());
        }
        removeRefs(current);
    }

    /**
     * @param current
     */
    private void removeRefs(Node<V> current) {
        current.setPrev(null);
        current.setNext(null);
    }

    /**
     * @return
     */
    private Node<V> removeLast() {
        Node<V> current = last;
        last = current.getPrevious();
        current.getPrevious().setNext(null);
        removeRefs(current);
        return current;
    }

    public Node<V> removeFirst() {
        Node<V> current = first;
        first = current.getNext();
        return current;
    }

    /**
     * @param i
     * @param val
     * @return
     */
    @Override
    public V set(int i, V val) {
        Node<V> current = iterate(i);
        Node<V> node = new Node<V>(val);
        node.setNext(current.getNext());
        node.setPrev(current.getPrevious());
        current.setNext(null);
        current.setPrev(null);
        return (V) node;
    }

    /**
     * @param i
     * @return
     */
    @Override
    public V get(int i) {
        if (i + 1 > size || i + 1 < 0) {
            throw new IndexOutOfBoundsException();
        }
        Node<V> current = iterate(i);
        return current.getVal();
    }

    /**
     * @param o
     * @return
     */
    @Override
    public int indexOf(Object o) {
        return 0;
    }

    /**
     * @param e
     * @return
     */
    @Override
    public boolean contains(Object e) {
        return false;
    }

    /**
     * @return
     */
    @Override
    public Object[] toArray() {
        Object[] mas = new Object[size];
        mas[0] = first.val;
        Node<V> t = first;
        int i = 1;
        while (t.next != null) {
            t = t.next;
            mas[i++] = t.val;
        }
        return mas;
    }

    /**
     * The node class
     *
     * @param <V>
     */

    public static class Node<V> {
        Node<V> prev;
        Node<V> next;
        V val;

        public Node(V val) {
            this.val = val;
        }

        public Node(Node<V> prev, Node<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }

        /**
         * @return
         */
        public Node<V> getNext() {
            return next;
        }

        /**
         * @param next
         */
        public void setNext(Node<V> next) {
            this.next = next;
        }

        /**
         * @return
         */
        public Node<V> getPrevious() {
            return prev;
        }

        /**
         * @param prev
         */
        public void setPrev(Node<V> prev) {
            this.prev = prev;
        }

        /**
         * @return
         */
        public V getVal() {
            return val;
        }

        /**
         * @param val
         */
        public void setVal(V val) {
            this.val = val;
        }
    }

    public class ListIteratorImpl<V> implements ListIterator<V> {

        private Node<V> previousElement;
        private Node<V> nextElement = (Node<V>) first;
        private int cursor = 0;
        private int expectedModifications;

        /**
         * @return
         */
        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        /**
         * @return
         */
        @Override
        public V next() {
            checkForComodification();
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            previousElement = nextElement;
            V node = nextElement.val;
            nextElement = nextElement.next;
            cursor++;
            return node;
        }

        /**
         * @return
         */
        @Override
        public boolean hasPrevious() {
            return cursor > 0;
        }

        /**
         * @return
         */
        @Override
        public V previous() {
            checkForComodification();
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            nextElement = nextElement.prev;
            cursor--;
            previousElement = nextElement;
            return nextElement.val;
        }

        /**
         * @return
         */
        @Override
        public int nextIndex() {
            return cursor;
        }

        /**
         * @return
         */
        @Override
        public int previousIndex() {
            return cursor - 1;
        }

        /**
         *
         */
        @Override
        public void remove() {
            checkForComodification();
            if (previousElement == null) {
                throw new IllegalStateException();
            }
            Node<V> x = previousElement.prev;
            Node<V> y = previousElement.next;
            x.next = y;
            y.prev = x;
            size--;
            if (nextElement == previousElement) {
                nextElement = y;
            } else {
                cursor--;
            }
            previousElement = null;
        }

        /**
         * @param v
         */
        @Override
        public void set(V v) {
            checkForComodification();
            if (previousElement == null) {
                throw new IllegalStateException();
            }
            previousElement.val = v;
        }

        /**
         * @param v
         */
        @Override
        public void add(V v) {
            checkForComodification();
            Node<V> x = nextElement.prev;
            Node<V> y = new Node<V>(v);
            Node<V> z = nextElement;
            x.next = y;
            y.next = z;
            z.prev = y;
            y.prev = x;
            size++;
            cursor++;
            this.expectedModifications++;
            previousElement = null;
        }

        public void checkForComodification() {
            if (DoublyLinkedList.this.modCount != this.expectedModifications) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
