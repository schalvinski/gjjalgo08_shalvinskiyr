package com.getjavajob.training.algo08.shalvinskiyr.lesson03;

import java.util.ArrayList;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Time.getElapsedTime;
import static com.getjavajob.training.algo08.shalvinskiyr.util.Time.start;

/**
 * Created by roman on 17.11.16.
 */
public class DynamicArrayPerformanceTest {
    public static void main(String[] args) {
        DynamicArray<Number> numberDynamicArray = new DynamicArray<>(15_000_000);
        for (int i = 0; i < 15_000_000; i++) {
            numberDynamicArray.add(i);
        }
        ArrayList<Number> numbers = new ArrayList<>(15_000_000);
        for (int i = 0; i < 15_000_000; i++) {
            numbers.add(i);
        }
        addToBeign(numberDynamicArray, numbers);
        removeFromBegin(numberDynamicArray, numbers);
        addToMiddle(numberDynamicArray, numbers);
        removeFromMiddle(numberDynamicArray, numbers);
        addToEnd(numberDynamicArray, numbers);
        removeFromEnd(numberDynamicArray, numbers);
    }

    private static void addToBeign(DynamicArray<Number> numberDynamicArray, ArrayList<Number> numbers) {
        System.out.println("Addition to the beginning    ");
        start();
        numberDynamicArray.add(0, 1000);
        System.out.println("DynamicArray.add(e): " + getElapsedTime() + " ms");
        start();
        numbers.add(0, 147);
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
    }

    private static void removeFromBegin(DynamicArray<Number> numberDynamicArray, ArrayList<Number> numbers) {
        System.out.println("------------");
        System.out.println("Remove to the beginning    ");
        start();
        numberDynamicArray.remove(0);
        System.out.println("DynamicArray.remove(e): " + getElapsedTime() + " ms");
        start();
        numbers.remove(0);
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
        System.out.println("****************************************************\n");
    }

    private static void addToMiddle(DynamicArray<Number> numberDynamicArray, ArrayList<Number> numbers) {
        System.out.println("Add to the middle test");
        start();
        numberDynamicArray.add(10_000_000, 147);
        System.out.println("DynamicArray.add(e): " + getElapsedTime());
        start();
        numbers.add(10_000_000, 147);
        System.out.println("ArrayList.add(e): " + getElapsedTime());
        System.out.println("------------");
    }

    private static void removeFromMiddle(DynamicArray<Number> numberDynamicArray, ArrayList<Number> numbers) {
        System.out.println("Remove from the middle test");
        start();
        numberDynamicArray.remove(10_000_000);
        System.out.println("DynamicArray.remove(e): " + getElapsedTime() + " ms");
        start();
        numbers.remove(10_000_000);
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
        System.out.println("****************************************************\n");
    }

    private static void addToEnd(DynamicArray<Number> numberDynamicArray, ArrayList<Number> numbers) {
        System.out.println("Addition to the end test");
        start();
        numberDynamicArray.add(numberDynamicArray.size() - 1, 1000);
        System.out.println("DynamicArray.add    (e): " + getElapsedTime() + " ms");
        start();
        numbers.add(numbers.size() - 1, 1000);
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
        System.out.println("------------");
    }

    private static void removeFromEnd(DynamicArray<Number> numberDynamicArray, ArrayList<Number> numbers) {
        System.out.println("Remove from the end test");
        start();
        numberDynamicArray.remove(numberDynamicArray.size() - 1);
        System.out.println("DynamicArray.remove(e): " + getElapsedTime() + " ms");
        start();
        numbers.remove(numbers.size() - 1);
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
        System.out.println("****************************************************\n");
    }
}

