package com.getjavajob.training.algo08.shalvinskiyr.exam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by user on 22.01.17.
 */
public class ListSashaTania {
    public static void main(String[] args) {


//        List<String> boysAndGirls = Arrays.asList("Ваня", "Таня", "Саша", "Петя", "Даша");
//        for (String boyOrGirl : boysAndGirls) {
//            if ("Саша".equals(boyOrGirl)) {
//                boysAndGirls.add("Маша");
//            } //todo две грабли почему не 6
//        }
//        System.out.println(boysAndGirls.size());

        List<String> bAndGirls = new ArrayList<>(Arrays.asList("Ваня", "Таня", "Саша", "Петя", "Даша"));
        ListIterator<String> itr = bAndGirls.listIterator();
        while(itr.hasNext()){
            String boyOrGirl = itr.next();
            if ("Саша".equals(boyOrGirl)) {
                itr.add("Маша");
            } //todo две грабли почему не 6
        }
        System.out.println(bAndGirls.size());

        for (String s : bAndGirls){
            System.out.println(s);
        }

        /*
        This is a list view of the array, the list is partly unmodifiable, you can't add or delete elements. But the time complexity is O(1).

If you want a modifiable a List:

List<String> strings =
     new ArrayList<String>(Arrays.asList(new String[]{"one", "two", "three"}));
This will copy all elements from the source array into a new list (complexity: O(n))
         */


        /*
        * I have a collection c1<MyClass> and an array a<MyClass>.
        * I am trying to convert the array to a collection c2 and do c1.removeAll(c2), But this throws UnsupportedOperationException.
        * I found that the asList() of Arrays class returns Arrays.ArrayList class and the this class inherits the removeAll() from AbstractList() whose implementation throws UnsupportedOperationException.
        * */

        /*
        ThirdPartyType a = new ...;
        ThirdPartyType b = new ...;
        ThirdPartyType c = new ...;
        hashSet.add(a);
        hashSet.add(b);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 3

        hashSet.add(b);
        hashSet.add(a);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 2 - todo when is it possible?*/

         /* TreeSet<Object> treeSet = ...;
         treeSet.add(new Object());
         System.out.println(treeSet.size()); // size ? todo несколько случаев и почему? */

//        boolean containsDuplicates(int[] numbers) {} // 3 diff solutions with 2 different speeds,
// 1 of them must not use O(n) memory, all of them faster n^2
        //todo массив - true - если есть модифицировать входной массив алгоритм сложность оценка памяти
        //todo как сложность считается
        //todo lesson02

//        m(Arrays.asList("Ваня", "Таня", "Саша", "Петя", "Даша"));


    }
//    static void m(List<?> l) {
//            l.add(new Object());
//        }

//         static void m(List<Object> l) {
//            l.add("string");
//             System.out.println(l);
//        }
}

