package com.getjavajob.training.algo08.shalvinskiyr.exam;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by user on 22.01.17.
 */
public class TreeSet2 {
    public static void main(String[] args) {
        long a = 0x1_0000_0000L; //todo расширение значение переменной от меньшего к большему

        //todo long литералл
        //todo понимание сколько бит занимает одина разряд в любой системе счисления
        //

        Integer b = 20;//todo автобоксинг анбоксинг
        Integer c = 20;
        Integer d = new Integer(20);//todo кеш оберток в java

        //todo ввиды типов данных
        //todo кэш строк, как в него полжить что-то как достать
        //todo почему строки immutable что решают(кеш и хеш)
        //todo кодировка строк JVM
        //todo точное unicode mapping
        //todo кодировка utf кодировка - сколько памяти
        //

        Object k[] = {"asdad"};
        Object l[] = new Object[]{"asdad"}; //todo конвеншнс объявление массива
        //a[0]=1;

        List<Object> ll = new ArrayList<>();
        //todo почему generic инварианты, иерархия дженериков как они выстраиваются в иерархию относительно друг друга

//        static void m(List<Object> l) {
//            l.add("string");
//        }

        /*static void m(List<?> l) {
            l.add(new Object());
        }*/

    /*    List<String> boysAndGirls = Arrays.asList("Ваня", "Таня", "Саша", "Петя", "Даша");
        for (String boyOrGirl : boysAndGirls) {
            if ("Саша".equals(boyOrGirl)) {
                boysAndGirls.add("Маша");
//            } todo две грабли почему не 6
            }
            System.out.println(boysAndGirls.size()); //
*/

        /*
        ThirdPartyType a = new ...;
        ThirdPartyType b = new ...;
        ThirdPartyType c = new ...;
        hashSet.add(a);
        hashSet.add(b);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 3

        hashSet.add(b);
        hashSet.add(a);
        hashSet.add(c);
        System.out.println(hashSet.size()); // 2 - todo when is it possible?*/

          /* TreeSet<Object> treeSet = ...;
         treeSet.add(new Object());
         System.out.println(treeSet.size()); // size ? todo несколько случаев и почему? */

        TreeSet<Object> treeSet = new TreeSet<>();
        treeSet.add(new Object());
        System.out.println(treeSet.size()); // size ? todo несколько случаев и почему?
        /*All elements inserted into a sorted set must implement the Comparable interface (or be accepted by the specified comparator).
        Furthermore, all such elements must be mutually comparable: e1.compareTo(e2) (or comparator.compare(e1, e2))
        must not throw a ClassCastException for any elements e1 and e2 in the sorted set.
        Attempts to violate this restriction will cause the offending method or constructor invocation to throw a ClassCastException.*/

        //объект должен быть компарабле
    }
/*    public static class NameComp implements Comparator{

        @Override
        public int compare(java.lang.Object o1, java.lang.Object o2) {
            return ((Object) o1).compareTo((Object) o2);
        }
    }*/
}


