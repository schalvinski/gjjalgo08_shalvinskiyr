package com.getjavajob.training.algo08.shalvinskiyr.lesson03;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.fail;


/**
 * Created by ZinZaga on 28.09.16.
 */
public class DynamicArrayTest {
    public static void main(String[] args) {
        addTest();
        addIntTest();
        setTest();
        getTest();
        removeTest();
        removeObjTest();
        sizeTest();
        indexOfTest();
    }

    public static void addTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(45);
            arr.add(49);
            arr.add(54);
            arr.add(78);
            arr.add(56);
            arr.add(45);
            arr.add(43);
            arr.add(45);
            arr.add(49);
            arr.add(54);
            arr.add(78);
            arr.add(56);
            arr.add(16, 14);
            fail("ArrayIndexOutOfBoundsException");
        } catch (Exception e) {
            assertEquals("addTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
    }

    public static void addIntTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "b");
            arr.add(0, "a");
            arr.add(2, "arr");
            arr.add(25, "c");
        } catch (Exception e) {
            assertEquals("addIntTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
    }

    public static void setTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.set(0, "a");
            arr.set(9, "b");
        } catch (Exception e) {
            assertEquals("setTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
        try {
            DynamicArray arr = new DynamicArray();
            arr.set(0, "a");
            arr.set(12, "b");
        } catch (Exception e) {
            assertEquals("setTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
    }

    public static void getTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "a");
            arr.add(1, "b");
            arr.get(0);
            arr.get(1);
        } catch (Exception e) {
            assertEquals("getTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "a");
            arr.add(1, "b");
            arr.get(0);
            arr.get(3);
            arr.get(5);
            arr.get(15);

        } catch (Exception e) {
            assertEquals("getTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
    }

    public static void removeTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add("java");
            arr.add("the");
            arr.add("best");
            arr.add("programming");
            arr.add("language");
            arr.remove(0);
            arr.remove(1);
            arr.remove(2);
        } catch (Exception e) {
            assertEquals("removeTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
        try {
            DynamicArray arr = new DynamicArray();
            arr.add("java");
            arr.add("the");
            arr.add("best");
            arr.add("programming");
            arr.add("language");
            arr.remove(0);
            arr.remove(1);
            arr.remove(10);
        } catch (Exception e) {
            assertEquals("removeTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
    }

    public static void removeObjTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add("java");
            arr.add("the");
            arr.add("best");
            arr.add("programming");
            arr.add("language");
            arr.remove("java");
            arr.remove("best");
            arr.remove("languge");
        } catch (Exception e) {
            assertEquals("removeObjTest", "ArrayIndexOutOfBoundsException", e.getClass().getSimpleName());
        }
    }

    public static void sizeTest() {
        DynamicArray arr = new DynamicArray();
        assertEquals("sizeTest", 0, arr.size());
    }

    public static void indexOfTest() {
        DynamicArray<Integer> arr = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            arr.add(i);
        }
        assertEquals("indexOfTest", 5, arr.indexOf(new Integer(5)));
        assertEquals("indexOfTest", -1, arr.indexOf(new Integer(17)));
    }
}

