package com.getjavajob.training.algo08.shalvinskiyr.lesson06;
//hashmap
/**
 * Created by roman on 23.11.16.
 */






public class AssociativeArrayMy<V,K>{

    final int CAPACITY = 16;
    float DEF_LOAD_FACTOR=0.75f;
    final int MAX_CAPACITY = 1<<30;




        }













































//public class AssociativeArrayMy<K, V> {
//    final int CAPACITY = 16;
//    final float DEF_LOAD_FACTOR = 0.75f;
//    final int MAX_CAPACITY = 1 << 30;
//    private Entry[] table = new Entry[CAPACITY];
//    private int size;
//    private int threshold = (int) (CAPACITY * DEF_LOAD_FACTOR);
//
//    /**
//     * @param k hashcode of the key
//     * @return hashCode
//     */
//    private int hash(K k) {
//        return k.hashCode();
//    }
//
//    /**
//     * @param hash
//     * @param length
//     * @return an index in array
//     */
//    private int indexFor(int hash, int length) {
//        return Math.abs(hash % length);
//    }
//
//    /**
//     * @param key
//     * @return
//     */
//    private Entry<K, V> getEntry(K key) {
//        if (size == 0) {
//            return null;
//        } else {
//            int hash = (key == null) ? 0 : hash(key);
//            for (Entry e = table[indexFor(hash, table.length)]; e != null; e = e.next) {
//                if (e.hash == hash) {
//                    Object k = e.key;
//                    if (e.key == key || key != null && key.equals(k)) {
//                        return e;
//                    }
//                }
//            }
//            return null;
//        }
//    }
//
//    /**
//     * @return
//     */
//    private V getNullKey() {
//        if (this.size == 0) {
//            return null;
//        } else {
//            for (Entry<K, V> e = this.table[0]; e != null; e = e.next) {
//                if (e.key == null) {
//                    return e.value;
//                }
//            }
//            return null;
//        }
//    }
//
//    /**
//     * @param value
//     * @return
//     */
//    private V putForNullKey(V value) {
//        for (Entry<K, V> e = this.table[0]; e != null; e = e.next) {
//            if (e.key == null) {
//                V oldValue = e.value;
//                e.value = value;
//                return oldValue;
//            }
//        }
//        addEntry(0, null, value, 0);
//        return null;
//    }
//
//    /**
//     * resizes a table of Entry
//     *
//     * @param newCapacity
//     */
//    void resize(int newCapacity) {
//        Entry[] oldTable = table;
//        int oldCapacity = oldTable.length;
//        if (oldCapacity == MAX_CAPACITY) {
//            threshold = Integer.MAX_VALUE;
//            return;
//        }
//        Entry[] newTable = new Entry[newCapacity];
//        transfer(newTable);
//        table = newTable;
//        threshold = (int) Math.min(newCapacity * DEF_LOAD_FACTOR, MAX_CAPACITY + 1);
//    }
//
//    /**
//     * reIndexind
//     *
//     * @param newTable
//     */
//    void transfer(Entry[] newTable) {
//        int newCapacity = newTable.length;
//        for (Entry e : table) {
//            while (null != e) {
//                Entry next = e.next;
//                int i = indexFor(e.hash, newCapacity);
//                e.next = newTable[i];
//                newTable[i] = e;
//                e = next;
//            }
//        }
//    }
//
//    /**
//     * adding Entry
//     */
//    void addEntry(int hash, K key, V value, int bucketIndex) {
//        if ((size >= threshold) && (null != table[bucketIndex])) {
//            resize(2 * table.length);
//            hash = (null != key) ? hash(key) : 0;
//            bucketIndex = indexFor(hash, table.length);
//        }
//
//        createEntry(hash, key, value, bucketIndex);
//    }
//
//    //creating Entry
//    void createEntry(int hash, K key, V value, int bucketIndex) {
//        Entry e = table[bucketIndex];
//        table[bucketIndex] = new Entry(hash, key, value, e);
//        size++;
//    }
//
//    //removing entry from table
//    private Entry removeEntry(K key) {
//        int hash = 0;
//        if (key != null) {
//            hash = hash(key);
//        }
//        int i = indexFor(hash, table.length);
//        Entry prev = table[i];
//        Entry e;
//        Entry next;
//
//        for (e = prev; e != null; e = next) {
//            next = e.next;
//            if (e.hash == hash) {
//                Object k = e.key;
//                if (e.key == key || key != null && key.equals(k)) {
//                    --size;
//                    if (prev == e) {
//                        table[i] = next;
//                    } else {
//                        prev.next = next;
//                    }
//                    return e;
//                }
//            }
//            prev = e;
//        }
//        return e;
//    }
//
//    public V add(K key, V val) {
//        V res = null;
//
//        if (key == null) {
//            return putForNullKey(val);
//        }
//        int hash = hash(key);
//        int i = indexFor(hash, table.length);//i - индекс в таблице хешей хеш делиться на длину таблицы (16)
//        for (Entry<K, V> e = table[i]; e != null; e = e.next) {
//            K k;
//            if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {
//
//                res = e.getValue();
//                e.setValue(val);
//                e.value = val;
//                return res;
//            }
//        }
//        addEntry(hash, key, val, i);
//        return res;
//    }
//
//    public V get(K key) {
//        if (key == null) {
//            return getNullKey();
//        } else {
//            return getEntry(key).getValue();
//        }
//    }
//
//    public V remove(K key) {
//        Entry<K, V> e = removeEntry(key);
//        if (e != null) {
//            return e.value;
//        }
//        return null;
//    }
//}
