package com.getjavajob.training.algo08.shalvinskiyr.exam;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by user on 22.01.17.
 */
public class implCollect2 {
    public static void main(String[] args) {
        System.out.println(containsDuplicates(new int[]{1, 2, 3, 4, 5, 5}));
    }

    static boolean containsDuplicates(int[] numbers) {
        Set<Integer> hSet = new TreeSet<>();
        for (int i = 0; i < numbers.length; i++) {
            if (!hSet.add(numbers[i])) return true;
        }
        return false;
    }
}
// O(log(n) complexity)
//        boolean containsDuplicates(int[] numbers) {} // 3 diff solutions with 2 different speeds,
// 1 of them must not use O(n) memory, all of them faster n^2