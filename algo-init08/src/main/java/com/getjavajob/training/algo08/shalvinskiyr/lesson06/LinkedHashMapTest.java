package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

import java.util.LinkedHashMap;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 23.11.16.
 */
public class LinkedHashMapTest {
    public static void main(String[] args) {
        testAdd();
    }

    private static void testAdd() {
        LinkedHashMap<Integer, String> hashMap = new LinkedHashMap();
        hashMap.put(0, "Zero");
        hashMap.put(1, "One");
        hashMap.put(2, "Two");
        String[] cmp = new String[]{"Zero", "One", "Two"};
        assertEquals("LinkedHashMapTest.testAdd", cmp, hashMap.values().toArray(new String[hashMap.size()]));
    }
}
