package com.getjavajob.training.algo08.shalvinskiyr.lesson06.HashMapAlgo;
//hashmap

/**
 * Created by roman on 23.11.16.
 */
//Коллекция HashMap
public class AssociativeArray<K, V> {//Создаем класс с дженериками кей и валюе
    final int CAPACITY = 16;  //переменная количество бакетов вначале
    final float DEF_LOAD_FACTOR = 0.75f;//определеяет порог заполняемости хешмапы, когда будет увеличено количество бакетов
    final int MAX_CAPACITY = 1 << 30;// максимальная 2 в 30 степени
    private Entry[] table = new Entry[CAPACITY];//создаем массив table тейбл из объектов Entry в количестве капейсити 16
    private int size;
    private int threshold = (int) (CAPACITY * DEF_LOAD_FACTOR); //переменная порог

    //Класс Entry
    class Entry<K, V> {//Класс Entry является описание НОДА
        K key;//Переменная ключ
        V value;//Переменная значение
        Entry<K, V> next;//ссылка на следующий нод в случае коллизии
        int hash;
//создаем конструктор
        public Entry(int hash, K key, V value, Entry next) {//создаем конструктор; переменная next содержит ссылку на следующий Нод
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public final V setValue(V newValue) {//создаем сеттер для валюе старое значение перезаписывается на новое значение валюе
            //в случае если key и value совпадают
            V oldValue = this.value;
            this.value = newValue;
            return oldValue;
        }

        public K getKey() {
            return this.key;
        }  //

        public V getValue() {
            return this.value;
        }//
    }//Класс Entry закончен

    private int hash(K k) { //метод расчета хешкода ключа
        return k.hashCode();//возвращаем примененную к ключу функцию hashcode
    }

    private int indexFor(int hash, int length) {//метод расчета индекса
        return Math.abs(hash % length);//возвращеает расчет индекса по формуле остаток от хеша деленный на длинну
    }
//    Теперь, зная индекс в массиве, мы получаем список (цепочку) элементов, привязанных к этой ячейке. Хэш и ключ нового элемента поочередно
// сравниваются с хэшами и ключами элементов из списка и, при совпадении этих параметров, значение элемента перезаписывается.

    //функция расчета добавления новой пары
    public V add(K key, V val) {
        V res = null;//

        if (key == null) {
            return putForNullKey(val);
        }
        int hash = hash(key);
        int i = indexFor(hash, table.length);//i - индекс в таблице хешей хеш делиться на длину таблицы (16)
        for (Entry<K, V> e = table[i]; e != null; e = e.next) {
            K k;
            if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {

                res = e.getValue();
                e.setValue(val);
                e.value = val;
                return res;
            }
        }
        addEntry(hash, key, val, i);
        return res;
    }
    //метод добавления нового нода
    void addEntry(int hash, K key, V value, int bucketIndex) {
        if ((size >= threshold) && (table[bucketIndex] != null)) {//в случае если порог превышен вызывается метод ресайз
            resize(2 * table.length);//
            hash = (null != key) ? hash(key) : 0;
            bucketIndex = indexFor(hash, table.length);
        }
        createEntry(hash, key, value, bucketIndex);
    }

    //creating Entry
    void createEntry(int hash, K key, V value, int bucketIndex) {
        Entry e = table[bucketIndex];
        table[bucketIndex] = new Entry(hash, key, value, e);
        size++;
    }

    private Entry<K, V> getEntry(K key) {
        if (size == 0) {
            return null;
        } else {
            int hash = (key == null) ? 0 : hash(key);
            for (Entry e = table[indexFor(hash, table.length)]; e != null; e = e.next) {
                if (e.hash == hash) {
                    Object k = e.key;
                    if (e.key == key || key != null && key.equals(k)) {
                        return e;
                    }
                }
            }
            return null;
        }
    }

    /**
     * @return
     */
    private V getNullKey() {
        if (this.size == 0) {
            return null;
        } else {
            for (Entry<K, V> e = this.table[0]; e != null; e = e.next) {
                if (e.key == null) {
                    return e.value;
                }
            }
            return null;
        }
    }

    /**
     * @param value
     * @return
     */
    private V putForNullKey(V value) {
        for (Entry<K, V> e = this.table[0]; e != null; e = e.next) {
            if (e.key == null) {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
        addEntry(0, null, value, 0);
        return null;
    }

/*    Если же предыдущий шаг не выявил совпадений, будет вызван метод addEntry(hash, key, value, index) для добавления нового элемента.

    Если при добавлении элемента в качестве ключа был передан null, действия будут отличаться. Будет вызван метод putForNullKey(value), внутри которого нет вызова методов hash() и indexFor() (потому как все элементы с null-ключами всегда помещаются в table[0]), но есть такие действия:

            (-) Все элементы цепочки, привязанные к table[0], поочередно просматриваются в поисках элемента с ключом null. Если такой элемент в цепочке существует, его значение перезаписывается.
            (-) Если элемент с ключом null не был найден, будет вызван уже знакомый метод addEntry().
    addEntry(0, null, value, 0);
    Kогда при добавлении элемента возникает коллизия, новый элемент добавляется в начало цепочки.*/

    void resize(int newCapacity) {
        Entry[] oldTable = table;
        int oldCapacity = oldTable.length;
        if (oldCapacity == MAX_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }
        Entry[] newTable = new Entry[newCapacity];
        transfer(newTable);
        table = newTable;
        threshold = (int) Math.min(newCapacity * DEF_LOAD_FACTOR, MAX_CAPACITY + 1);//
    }

    void transfer(Entry[] newTable) {
        int newCapacity = newTable.length;
        for (Entry e : table) {
            while (null != e) {
                Entry next = e.next;
                int i = indexFor(e.hash, newCapacity);
                e.next = newTable[i];
                newTable[i] = e;
                e = next;
            }
        }
    }

    //removing entry from table
    private Entry removeEntry(K key) {
        int hash = 0;
        if (key != null) {
            hash = hash(key);
        }
        int i = indexFor(hash, table.length);
        Entry prev = table[i];
        Entry e;
        Entry next;

        for (e = prev; e != null; e = next) {
            next = e.next;
            if (e.hash == hash) {
                Object k = e.key;
                if (e.key == key || key != null && key.equals(k)) {
                    --size;
                    if (prev == e) {
                        table[i] = next;
                    } else {
                        prev.next = next;
                    }
                    return e;
                }
            }
            prev = e;
        }
        return e;
    }


    public V get(K key) {
        if (key == null) {
            return getNullKey();
        } else {
            return getEntry(key).getValue();
        }
    }

    public V remove(K key) {
        Entry<K, V> e = removeEntry(key);
        if (e != null) {
            return e.value;
        }
        return null;
    }
}
