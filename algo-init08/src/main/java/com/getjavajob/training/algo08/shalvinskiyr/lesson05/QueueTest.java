package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

import com.getjavajob.training.algo08.shalvinskiyr.util.Assert;

import java.util.LinkedList;
import java.util.Queue;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 23.11.16.
 */

public class QueueTest {
    public static void main(String[] args) {
        queueAddTest();
        queueOfferTest();
        queueRemoveTest();
        queuePollTest();
        queueElementTest();
        queuePeekTest();
    }

    public static void queueAddTest() {
        Queue<Integer> queue = new LinkedListQueue<>();
        assertEquals("QueueTest.queueAddTest", true, queue.add(1));
    }

    public static void queueOfferTest() {
        Queue<Integer> queue = new LinkedList<>();
        assertEquals("QueueTest.queueOfferTest", true, queue.offer(17));
    }

    public static void queueRemoveTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        String msg = "NoSuchElementException";
        try {
            queue.remove();
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queueRemoveTestFail", msg, e.getClass().getSimpleName());
        }

        queue.add(12);
        try {
            queue.remove();
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queueRemoveTestFail", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("QueueTest.queueRemoveTestOk", "AssertionError", e.getClass().getSimpleName());
        }
    }

    public static void queuePollTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        String msg = "NullPointerException";
        try {
            int k = queue.poll();
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queuePollTest", msg, e.getClass().getSimpleName());
        }
    }


    public static void queueElementTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        String msg = "NoSuchElementException";
        try {
            int k = queue.element();
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queueElementTest", msg, e.getClass().getSimpleName());
        }
    }

    public static void queuePeekTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        String msg = "NullPointerException";
        try {
            int k = queue.peek();
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queuePeekTest", msg, e.getClass().getSimpleName());
        }
    }
}
