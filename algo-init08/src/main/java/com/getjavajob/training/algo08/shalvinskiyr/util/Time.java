package com.getjavajob.training.algo08.shalvinskiyr.util;

/**
 * Created by roman on 17.11.16.
 */
public class Time {

    public static long startTime;
    public static long endTime;

    public static long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public static long getElapsedTime() {
        endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
}
