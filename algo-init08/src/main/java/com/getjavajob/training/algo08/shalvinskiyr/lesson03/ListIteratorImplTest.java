package com.getjavajob.training.algo08.shalvinskiyr.lesson03;


public class ListIteratorImplTest {

    public static void main(String[] args) {
        addTest();
        previousTest();
        nextTest();
        removeTest();
    }

    public static void addTest() {
        DynamicArray arr = new DynamicArray();
        DynamicArray.ListIteratorImpl iter = arr.listIterator();
        String test = "test";
        while (iter.hasNext()) {
            try {
                iter.add(test);
                iter.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void previousTest() {
        DynamicArray arr = new DynamicArray();
        DynamicArray.ListIteratorImpl iter = arr.listIterator();
        for (int i = 0; i < 10; i++) {
            iter.add(i);
        }
        while (iter.hasPrevious()) {
            try {
                System.out.println("test previous " + iter.previous() + " ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void nextTest() {
        DynamicArray arr = new DynamicArray();
        DynamicArray.ListIteratorImpl iter = arr.listIterator();
        for (int i = 0; i < 10; i++) {
            iter.add(i);
            iter.previous();
        }
        while (iter.hasNext()) {
            System.out.println("test next " + iter.next());
        }
    }

    public static void removeTest() {
        DynamicArray arr = new DynamicArray();
        DynamicArray.ListIteratorImpl iter = arr.listIterator();
        while (iter.hasNext()) {
            try {
                iter.remove();
                iter.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
