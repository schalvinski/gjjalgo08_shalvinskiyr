package com.getjavajob.training.algo08.shalvinskiyr.exam;

/**
 * Created by user on 27.01.17.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListIterator1 {

    public static void main(String args[]) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        ListIterator<String> it = list.listIterator();

        while (it.hasNext()) {
            String str = it.next();
            if (("B").equals(str)) {
                it.add("d");
            }
        }

        for (String str : list) {
            System.out.println(str);
        }
    }
}

