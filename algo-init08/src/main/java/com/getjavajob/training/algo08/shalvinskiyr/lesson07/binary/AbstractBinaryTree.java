package com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary;


import com.getjavajob.training.algo08.shalvinskiyr.lesson07.AbstractTree;
import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;
import com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary.ArrayBinaryTree.NodeImpl;

import java.util.LinkedList;
import java.util.Queue;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    private NodeImpl<E>[] array = new NodeImpl[10000];


    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        return null;
    }

//    @Override
    public Iterable<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        return null;
    }

//    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        return 0;
    }

//    @Override
    public Iterable<Node<E>> inOrder() {
        int k = 0;
        Queue<Node<E>> inList = new LinkedList<>();
        if (root() == null) {
            return inList;
        }
        Queue<Node<E>> queue = new LinkedList<>();
        Queue<Integer> numbers = new LinkedList<>();
        Node<E> currentNode = array[0];
        queue.add(array[0]);
        numbers.add(0);
        while (!queue.isEmpty() /*|| currentNode != null*/) {
            if (currentNode != null) {
                queue.add(currentNode);
                numbers.add(k);
                currentNode = array[2 * k + 1];
                k = 2 * k + 1;
            } else {
                Node<E> n = queue.peek();
                k = numbers.peek();
                inList.add(n);
                currentNode = array[2 * k + 2];
                k = 2 * k + 2;
            }
        }
        return inList;
    }
}
