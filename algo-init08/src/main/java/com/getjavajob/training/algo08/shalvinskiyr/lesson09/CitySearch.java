package com.getjavajob.training.algo08.shalvinskiyr.lesson09;

import java.util.Comparator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by roman on 08.06.16.
 */
public class CitySearch {
    public static void main(String[] args) {
        NavigableSet<String> cityStore = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o, String t1) {
                return o.compareToIgnoreCase(t1);
            }
        });
        cityStore.add("Kiev");
        cityStore.add("Moreupol");
        cityStore.add("Mozhenick");
        cityStore.add("Moscow");
        cityStore.add("Volgograd");
        cityStore.add("Sochi");
        cityStore.add("Rostov-on-Don");
        Set<String> result = cityStore.subSet("mo", true, "mo" + (Character.MAX_VALUE), true);
        for (String r : result) {
            System.out.println(r);
        }
    }
}
