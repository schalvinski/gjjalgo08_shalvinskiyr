package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

import java.util.HashSet;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 23.11.16.
 */
public class SetTest {

    public static void main(String[] args) {
        HashSet<Integer> hash = new HashSet<>();
        hash.add(1);
        assertEquals("SetTest.addTestFail", false, hash.add(1));
        assertEquals("SetTest.addTestPassed", true, hash.add(5));
        HashSet<Integer> another = new HashSet<>();
        another.add(45);
        another.add(37);
        assertEquals("SetTest.AddAllTrue", true, hash.addAll(another));
        HashSet<Integer> n = new HashSet<>();
        assertEquals("SetTest.AddAllTrue", false, hash.addAll(n));
    }
}
