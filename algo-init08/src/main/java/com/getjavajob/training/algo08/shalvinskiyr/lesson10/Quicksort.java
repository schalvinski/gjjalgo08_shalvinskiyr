package com.getjavajob.training.algo08.shalvinskiyr.lesson10;

/**
 * Created by roman on 19.11.2016.
 */
public class Quicksort<E extends Comparable<E>> {

    public void sort(E[] arr) {//метод sort с параметром заданного массива, создает массив с тремя параметрами
        sort(arr, 0, arr.length - 1);//параметры : массив, начальный индекс массива, последний индекс массива
    }

    public void sort(E[] arr, int left, int right) {
        if (left < right) {//если инедкс крайнего левого члена массива меньше индекса крайнего правого
            int pivot = partition(arr, left, right);//рассчитываем pivot Пивот или опорную точку массива
            if (pivot > 1)
                sort(arr, left, pivot - 1);
            if (pivot + 1 < right)
                sort(arr, pivot + 1, right);
        }
    }


    public int partition(E[] numbers, int left, int right) { //метод рассчета Пивота
        E pivot = numbers[left];//Пивот равняется крайнему левому члену массива и индексом 0; в данном случае семи
        while (true) {//цикл вайл тру
            while (numbers[left].compareTo(pivot) < 0) {//в случае если члены массива с индексом left меньше значения пивота то есть семи
                left++;//инкрементируем индекс лефт
            }
            while (numbers[right].compareTo(pivot) > 0) {//в случае если члены массива с индексом right большн значения пивота то есть семи
                right--;//уменьшаем индекс right на единицу
            }
            if (left < right) {//если индекс левого меньше индекса правого то меняем члены массива местами 0 меньше 7, то меняем местами семь и три
                E temp = numbers[right];//обмен происходит через переменную temp
                numbers[right] = numbers[left];
                numbers[left] = temp;
            } else {//если индексы равны или левый больше правого, то возвращаем левый индекс
                return right;
            }
        }
    }
}
