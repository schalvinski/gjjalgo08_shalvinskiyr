package com.getjavajob.training.algo08.shalvinskiyr.lesson06.HashMapAlgo;

import com.getjavajob.training.algo08.shalvinskiyr.util.Time;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by roman on 23.11.16.
 */
public class AssociativeArrayPerfomanceTest {
    public static void main(String[] args) {
        AssociativeArray<Integer, Integer> aa = new AssociativeArray();
        Map<Integer, Integer> map = new HashMap<>();
        testPutMethod(aa, map);
        testGetMethod(aa, map);
        testRemoveMethod(aa, map);
        testBorders();
    }

    public static void testPutMethod(AssociativeArray associativeArray, Map hashMap) {
        Random random = new Random();
        System.out.println("Addition to the map test");
        System.out.println("****************************************************");
        Time.start();
        for (int i = 0; i < 1_000_000; i++) {
            int r = random.nextInt();
            associativeArray.add(i, r);
        }
        System.out.println("AssociativeArray.put(K,V): " + Time.getElapsedTime());
        Time.start();
        for (int i = 0; i < 1_000_000; i++) {
            int r = random.nextInt();
            hashMap.put(i, r);
        }
        System.out.println("HashMap.put(K,V): " + Time.getElapsedTime());
    }

    public static void testGetMethod(AssociativeArray associativeArray, Map<Integer, Integer> hashMap) {
        System.out.println("\nGet from the map test");
        System.out.println("------------");
        Time.start();
        associativeArray.get(50_000);
        System.out.println("AssociativeArray.get(K): " + Time.getElapsedTime());
        Time.start();
        hashMap.get(50_000);
        System.out.println("HashMap.get(K): " + Time.getElapsedTime());
        System.out.println("------------");
    }

    public static void testRemoveMethod(AssociativeArray associativeArray, Map hashMap) {
        System.out.println("\nRemove from the map test");
        System.out.println("------------");
        Time.start();
        associativeArray.remove(50_000);
        System.out.println("AssociativeArray.remove(K): " + Time.getElapsedTime());
        Time.start();
        hashMap.remove(50_000);
        System.out.println("HashMap.remove(K): " + Time.getElapsedTime());
    }

    public static void testBorders() {
        AssociativeArray<Integer, Integer> associativeArray = new AssociativeArray<>();
        for (int i = 0; i < 20; i++) {
            associativeArray.add(i, i);
        }
        System.out.println(associativeArray.get(14));
        associativeArray.add(null, 3);
        associativeArray.get(null);
        associativeArray.add(1, 8);
    }
}
