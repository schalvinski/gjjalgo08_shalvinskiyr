package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

import java.util.HashMap;

/**
 * Created by roman on 26.11.16.
 */
public class MatrixImpl<E, V> implements Matrix<V> {//>>?? generics
    private HashMap<E, V> matrix;

    MatrixImpl(HashMap<E, V> matrix) {
        this.matrix = matrix;
    }

    @Override
    public V get(int i, int j) {
        return matrix.get(new KeyMatrix(i, j));
    }

    @Override
    public void set(int i, int j, V value) {
        matrix.put((E) new KeyMatrix(i, j), value);
    }
}
