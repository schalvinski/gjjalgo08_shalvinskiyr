// Attempting to add a value component to Point - Pages 37 - 38
package com.getjavajob.training.algo08.shalvinskiyr.exam.Treeset;

import java.util.HashSet;

//)
public class ColorPoint extends Point {
    private final Color color;

    public ColorPoint(int x, int y, Color color) {
        super(x);
        this.color = color;
    }

    // Broken - violates symmetry!
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ColorPoint)) {
            return false;
        }
        return super.equals(o) && ((ColorPoint) o).color == color;
    }

    // Broken - violates transitivity!
    // @Override public boolean equals(Object o) {
    // if (!(o instanceof Point))
    // return false;
    //
    // // If o is a normal Point, do a color-blind comparison
    // if (!(o instanceof ColorPoint))
    // return o.equals(this);
    //
    // // o is a ColorPoint; do a full comparison
    // return super.equals(o) && ((ColorPoint)o).color == color;
    // }

    public static void main(String[] args) {

 /*       // First equals function violates symmetry
		Point p = new Point(1);
		ColorPoint cp = new ColorPoint(1, 2, Color.RED);
		System.out.println(p.equals(cp) + " " + cp.equals(p));

		// Second equals function violates transitivity
		ColorPoint p1 = new ColorPoint(1, 2, Color.RED);
		Point p2 = new Point(1);
		Point p4 = new Point(1);
		ColorPoint p3 = new ColorPoint(1, 2, Color.BLUE);
		System.out.println(p1.equals(p2)+ " "+p2.equals(p3)+" "+p1.equals(p3));*/


        HashSet<Object> hashSet = new HashSet<>();
        ColorPoint a = new ColorPoint(1, 2, Color.RED);
        Point b = new Point(1);
        ColorPoint c = new ColorPoint(1, 2, Color.BLUE);

        hashSet.add(a);
        hashSet.add(b);
        hashSet.add(c);
        System.out.println(hashSet.size() + " " + a.equals(b) + " " + b.equals(a) + " " + a.hashCode() + +b.hashCode()+ " " + c.hashCode()); // 3

//		hashSet.add(b);
//		hashSet.add(c);
//		hashSet.add(a);
//		System.out.println(hashSet.size()+" "+c.equals(a)+" "+a.hashCode()+" "+b.hashCode()); // 2 - todo when is it possible?

    }
}
