package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.fail;

/**
 * Created by roman on 23.11.16.
 */
public class DequeTest {
    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testGetFirst();
        testGetLast();
        testElement();
        testPeekFirst();
        testPeekLast();
        testPollFirst();
        testPollLast();
        testPopPush();
        testRemove();
        testRemoveLast();
        testRemoveLastOccurrence();
    }

    private static void testRemoveLastOccurrence() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(12);
        arrayDeque.add(13);
        assertEquals("testRemoveLastOccurrence", true, arrayDeque.removeLastOccurrence(12));
    }

    private static void testRemoveLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        assertEquals("testRemoveLast", 13, arrayDeque.removeLast());
    }

    private static void testRemove() {
        Deque<Number> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        assertEquals("testRemove", true, arrayDeque.remove(13));
    }

    private static void testPopPush() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.push(12);
        assertEquals("testPopPush", 12, arrayDeque.pop());
    }

    private static void testPollLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("testPollLast", 12, arrayDeque.pollLast());
        assertEquals("testPollLastSizeAfterPoll", 1, arrayDeque.size());
    }

    private static void testPollFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("testPollFirst", 13, arrayDeque.pollFirst());
        assertEquals("testPollFirstSizeAfterPoll", 1, arrayDeque.size());
    }

    private static void testPeekLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("testPeekLast", 12, arrayDeque.peekLast());
    }

    private static void testPeekFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("testPeekFirst", 13, arrayDeque.peekFirst());
    }

    //the same as addFirstMethod
    private static void testElement() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("testElement", 13, arrayDeque.element());
    }

    private static void testGetLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(12);
        assertEquals("testGetLast", 12, arrayDeque.getLast());
        arrayDeque.remove();
        String msg = "NoSuchElementException";
        try {
            arrayDeque.getLast();
            fail(msg);
        } catch (NoSuchElementException e) {
            assertEquals("DequeTest.testGetLastException", msg, e.getClass().getSimpleName());
        }
    }

    private static void testGetFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        assertEquals("testGetFirst", 13, arrayDeque.getFirst());
        arrayDeque.remove();
    }

    public static void testAddFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.addFirst(12);
        assertEquals("testAddFirst", 12, arrayDeque.getFirst());

        String msg = "NullPointerException";
    }

    public static void testAddLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.addLast(12);
        assertEquals("testAddLast", 12, arrayDeque.getLast());
    }


    //the same as addFirst
    public static void testOfferFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.offer(15);
        arrayDeque.offerFirst(14);
        assertEquals("testOfferFirst", 14, arrayDeque.getFirst());
        assertEquals("testOfferFirstBool", true, arrayDeque.offerFirst(1));
    }

    public static void testOfferLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.offer(15);
        arrayDeque.offerLast(14);
        assertEquals("testOfferLast", 14, arrayDeque.getLast());
        assertEquals("testOfferLastBool", true, arrayDeque.offerLast(1));
    }
}

