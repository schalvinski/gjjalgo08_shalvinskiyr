package com.getjavajob.training.algo08.shalvinskiyr.lesson08.search;

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;
import com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary.LinkedBinaryTree;

import java.util.Comparator;

public class    BinarySearchTree<E> extends LinkedBinaryTree<E> {
    Node<E> thisRoot;
    private Comparator<E> comparator;

    public BinarySearchTree() {
        thisRoot = null;
    }

    BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1 first val
     * @param val2 second val
     * @return 1 if val1 > val2
     * 0 if val1 = val2
     * -1 if val1 < val2
     */
    protected int compare(E val1, E val2) {
        if (val1 != null && val2 != null) {
            if (comparator != null) {
                return comparator.compare(val1, val2);
            }
            return ((Comparable<E>) val1).compareTo(val2);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n   - root of search subtree
     * @param val - searching node's value
     * @return node if found, null if not
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        NodeImpl<E> eNode = (NodeImpl<E>) n;
        if (eNode == null) {
            return null;
        }
        int res = compare(eNode.getElement(), val);
        if (res == 0) {
            return eNode;
        } else if (res > 0) {
            return treeSearch(eNode.left, val);
        }
        return treeSearch(eNode.right, val);

    }
}
