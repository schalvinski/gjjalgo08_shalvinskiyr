package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

/**
 * Created by roman on 23.11.16.
 */
public interface Stack<V> {
    void push(V e);

    V pop();
}
