package com.getjavajob.training.algo08.shalvinskiyr.exam;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 22.01.17.
 */
public class implCollect4 {
    public static void main(String[] args) {
        System.out.println(containsDuplicates(new int[]{ 5, 2, 3, 4, 5}));
    }

    static boolean containsDuplicates(int[] numbers) {
        Map<Integer,Integer> m=new HashMap<Integer,Integer>();
        for(int i=0; i<numbers.length;i++)
        {
            if(m.containsKey(numbers[i]))
                return true;
            else
                m.put(numbers[i],1);
        }
        return false;
    }
}
//O(n) complexity
//        boolean containsDuplicates(int[] numbers) {} // 3 diff solutions with 2 different speeds,
// 1 of them must not use O(n) memory, all of them faster n^2