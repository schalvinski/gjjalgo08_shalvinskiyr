package com.getjavajob.training.algo08.shalvinskiyr.lesson08.balanced;

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;
import com.getjavajob.training.algo08.shalvinskiyr.lesson08.search.BinarySearchTree;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.pow;

public class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (!makeLeftChild) { //left rotation
            if (parent == root) {
                child.parent = null;
                root = child;
            } else {
                if (parent.parent.left == parent) {
                    parent.parent.left = child;
                    child.parent = parent.parent;
                } else {
                    parent.parent.right = child;
                    child.parent = parent.parent;
                }
            }
            child.left.parent = parent;
            parent.parent = child;
            parent.right = child.left;
            child.left = parent;
        } else {
            if (parent == root) {
                child.parent = null;
                root = child;
            } else {
                if (parent.parent.left == parent) {
                    parent.parent.left = child;
                    child.parent = parent.parent;
                } else {
                    parent.parent.right = child;
                    child.parent = parent.parent;
                }
            }
            parent.parent = child;
            child.right.parent = parent;
            parent.left = child.right;
            child.right = parent;
        }
    }

    /**
     * Rotates n with parent.
     *
     * @param n
     */
    public void rotate(Node<E> n) {
        NodeImpl<E> node = (NodeImpl<E>) n;
        boolean left = compare(n.getElement(), node.parent.getElement()) < 0;
        relink(node.parent, node, left);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> grandChild = (NodeImpl<E>) n;
        NodeImpl<E> parent = grandChild.parent;
        if (parent.parent == null) {
            rotate(grandChild);
            root = grandChild;
            return grandChild;
        }
        if (compare(parent.right.getElement(), grandChild.getElement()) == 0 &&
                compare(parent.parent.left.getElement(), parent.getElement()) == 0) {
            parent.parent.left = grandChild;
            grandChild.parent = parent.parent;
            parent.parent = grandChild;
            parent.right = grandChild.left;
            grandChild.left = parent;
            rotate(grandChild);
        } else if (compare(parent.left.getElement(), grandChild.getElement()) == 0 &&
                compare(parent.parent.right.getElement(), parent.getElement()) == 0) {
            parent.parent.right = grandChild;
            grandChild.parent = parent.parent;
            parent.parent = grandChild;
            parent.left = grandChild.right;
            grandChild.right = parent;
            rotate(grandChild);
        } else {
            rotate(grandChild);
        }
        return grandChild;
    }

    /**
     * Performs a left-right/right-left rotations.
     */
    public Node<E> rotateTwice(Node<E> n) {
        NodeImpl<E> node = (NodeImpl<E>) n;
        if (node.parent != null && node.parent.parent != null) {
            boolean left = compare(n.getElement(), node.parent.getElement()) < 0;
            boolean leftRight = compare(node.parent.getElement(), node.parent.parent.getElement()) < 0;
            relink(node.parent, node, left);
            relink(node.parent, node, leftRight);
        }
        return null;
    }

    /**
     * @param head
     */
    public void printBtree(NodeImpl<E> head) {
        List<NodeImpl<E>> list = new ArrayList<NodeImpl<E>>();
        list.add(head);
        printTree(list, getHeight(head));
    }

    /**
     * @param head
     * @return
     */
    public int getHeight(NodeImpl<E> head) {
        if (head == null) {
            return 0;
        } else {
            return 1 + max(getHeight(head.left), getHeight(head.right));
        }
    }

    /**
     * @param node
     * @return
     */
    public String toString(NodeImpl<E> node) {
        String str = "";
        if (node == null) {
            return str;
        }
        str += node.getElement();
        str += " [" + toString(node.left) + "] [" + toString(node.right) + "]";
        return str;
    }

    /**
     * print data of tree
     *
     * @param levelNodes
     * @param level
     */
    private void printTree(List<NodeImpl<E>> levelNodes, int level) {
        List<NodeImpl<E>> nodes = new ArrayList<NodeImpl<E>>();
        printIndentForLevel(level);
        for (NodeImpl<E> treeNode : levelNodes) {
            System.out.print(treeNode == null ? " " : treeNode.getElement());
            printSpacingBetweenNodes(level);
            if (level > 1) {
                nodes.add(treeNode == null ? null : treeNode.left);
                nodes.add(treeNode == null ? null : treeNode.right);
            }
        }
        System.out.println();

        if (level > 1) {
            printTree(nodes, level - 1);
        }
    }

    /**
     * @param level
     */
    private void printIndentForLevel(int level) {
        for (int i = (int) (pow(2, level - 1)); i > 0; i--) {
            System.out.print(" ");
        }
    }

    /**
     * prints spaces
     *
     * @param level
     */
    private void printSpacingBetweenNodes(int level) {
        for (int i = (int) ((pow(2, level - 1)) * 2) - 1; i > 0; i--) {
            System.out.print(" ");
        }
    }

}
