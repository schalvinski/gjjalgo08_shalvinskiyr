package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

/**
 * Created by roman on 23.11.16.
 */
public class LinkedListQueue<V> extends AbstractQueue<V> {
    private Node<V> first;    // beginning of queue
    private Node<V> last;     // end of queue
    private int count;

    @Override
    public boolean add(V e) {
        Node<V> old = last;
        last = new Node<V>();
        last.val = e;
        last.next = null;
        if (first == null) {
            first = last;
        } else {
            old.next = last;
        }
        count++;
        return true;
    }

    @Override
    public V remove() {
        if (first != null) {
            V e = first.val;
            first = first.next;
            count--;
            if (first == null) {
                last = null;
            }
            return e;
        }
        return null;
    }

    @Override
    public String toString() {
        String str = "";
        Node<V> node = first;
        while (node != null) {
            str += node.val + " ";
            node = node.next;
        }
        return str;
    }

    static class Node<V> {
        public Node<V> next;
        public V val;
    }
}
