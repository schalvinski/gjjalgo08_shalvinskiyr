package com.getjavajob.training.algo08.shalvinskiyr.lesson10;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 19.11.2016.
 */
public class MergeSortTest {
    public static void main(String[] args) {
        test1();
        test2();
    }

    public static void test1() {
        MergeSort<String> stringMergeSort = new MergeSort<>();
        String[] testStr = new String[]{"b", "a", "d", "x", "c", "q", "m"};
        String[] resultStr = new String[]{"a", "b", "c", "d", "m", "q", "x"};
        stringMergeSort.sort(testStr);
        assertEquals("merge_sortTest", resultStr, testStr);
    }

    public static void test2() {
        MergeSort<Integer> integerMergeSort = new MergeSort<>();
        Integer[] testInt = new Integer[]{7, 4, 1, 8, 5, 2, 9, 6, 3};
        Integer[] resultInt = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        integerMergeSort.sort(testInt);
        assertEquals("merge_sortTest", resultInt, testInt);
    }
}
