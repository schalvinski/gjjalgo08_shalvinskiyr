package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 10.08.16.
 */
public class LinkedListStackTest {

    public static void main(String[] args) {
        testPop();
        testPush();
    }

    private static void testPop() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.pop();
        assertEquals("LinkedListStackRemoveRemove", "87", stack.toString());
    }

    private static void testPush() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        stack.push(7);
        stack.push(8);
        stack.push(9);
        assertEquals("LinkedListStackAddTest", "987", stack.toString());
    }
}
