package com.getjavajob.training.algo08.shalvinskiyr.lesson01;

/**
 * Created by admin on 10.08.2015.
 */
public class Task07 {
    public static void main(String[] args) {
        System.out.println(swap2VarBitwise(0b0000_0001, 0b0000_0010));
        System.out.println(swap2VarBitwise2(3, 9));
        System.out.println(swap2VarArithmetic(3, 9));
        System.out.println(swap2VarArithmetic2(3, 9));
    }

    public static String swap2VarBitwise(int x, int y) {
        x = x ^ y ^ (y = x);
        return "x= " + x + " y= " + y;
    }

    public static String swap2VarBitwise2(int x, int y) {
        x = x ^ y;
        y = x ^ y;
        x = x ^ y;
        return "x= " + x + " y= " + y;
    }

    public static String swap2VarArithmetic(int x, int y) {
        x = x + y;
        y = x - y;
        x = x - y;
        return "x= " + x + " y= " + y;
    }

    public static String swap2VarArithmetic2(int x, int y) {
        x = x * y;
        y = x / y;
        x = x / y;
        return "x= " + x + " y= " + y;
    }
}
