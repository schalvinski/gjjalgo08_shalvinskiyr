package com.getjavajob.training.algo08.shalvinskiyr.lesson09.balanced;

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;
import com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary.LinkedBinaryTree;
import com.getjavajob.training.algo08.shalvinskiyr.lesson09.search.BinarySearchTree;

import java.util.ArrayList;
import java.util.List;

public abstract class BalanceableTree<E> extends BinarySearchTree<E> {
    /**
     * Relinks a parent with child node
     */
    private void relink(LinkedBinaryTree.NodeImpl<E> parent, LinkedBinaryTree.NodeImpl<E> child,
                        boolean makeLeftChild) {
        if (!makeLeftChild) { //left rotation
            if (parent == LinkedBinaryTree.root) {
                child.parent = null;
                LinkedBinaryTree.root = child;
            }
            child.left.parent = parent;
            parent.parent = child;
            parent.right = child.left;
            child.left = parent;
        } else {
            if (parent == LinkedBinaryTree.root) {
                child.parent = null;
                LinkedBinaryTree.root = child;
            }
            parent.parent = child;
            child.right.parent = parent;
            parent.left = child.right;
            child.right = parent;
        }
    }

    /**
     * Rotates n with parent.
     */
    public void rotate(Node<E> n) {
        LinkedBinaryTree.NodeImpl<E> node = (LinkedBinaryTree.NodeImpl<E>) n;
        boolean left = compare(n.getElement(), node.parent.getElement()) < 0;
        relink(node.parent, node, left);
    }

    /**
     * Performs a left-right/right-left rotations.
     */
    public Node<E> rotateTwice(Node<E> n) {
        LinkedBinaryTree.NodeImpl<E> node = (LinkedBinaryTree.NodeImpl<E>) n;
        if (node.parent != null && node.parent.parent != null) {
            boolean left = compare(n.getElement(), node.parent.getElement()) < 0;
            boolean leftRight = compare(node.parent.getElement(), node.parent.parent.getElement()) < 0;
            System.out.println("first rotate");
            relink(node.parent, node, left);
            System.out.println(node.parent.parent);
            relink(node.parent, node, leftRight);
        }
        return null;
    }

    public void printBtree(LinkedBinaryTree.NodeImpl<E> head) {
        List<LinkedBinaryTree.NodeImpl<E>> list = new ArrayList<LinkedBinaryTree.NodeImpl<E>>();
        list.add(head);
        printTree(list, getHeight(head));
    }

    public int getHeight(LinkedBinaryTree.NodeImpl<E> head) {
        if (head == null) {
            return 0;
        } else {
            return 1 + Math.max(getHeight(head.left), getHeight(head.right));
        }
    }

    public String toString(LinkedBinaryTree.NodeImpl<E> node) {
        String str = "";
        if (node == null) {
            return str;
        }
        str += node.getElement();
        str += " (" + toString(node.left) + ") (" + toString(node.right) + ")";
        return str;
    }

    private void printTree(List<LinkedBinaryTree.NodeImpl<E>> levelNodes, int level) {
        List<LinkedBinaryTree.NodeImpl<E>> nodes = new ArrayList<LinkedBinaryTree.NodeImpl<E>>();
        //indentation for first node in given level
        printIndentForLevel(level);
        for (LinkedBinaryTree.NodeImpl<E> treeNode : levelNodes) {
            System.out.print(treeNode == null ? " " : treeNode.getElement());//print node data
            printSpacingBetweenNodes(level);
            if (level > 1) {             //if its not a leaf node
                nodes.add(treeNode == null ? null : treeNode.left);
                nodes.add(treeNode == null ? null : treeNode.right);
            }
        }
        System.out.println();

        if (level > 1) {
            printTree(nodes, level - 1);
        }
    }

    private void printIndentForLevel(int level) {
        for (int i = (int) (Math.pow(2, level - 1)); i > 0; i--) {
            System.out.print(" ");
        }
    }

    private void printSpacingBetweenNodes(int level) {
        //spacing between nodes
        for (int i = (int) ((Math.pow(2, level - 1)) * 2) - 1; i > 0; i--) {
            System.out.print(" ");
        }
    }
}
