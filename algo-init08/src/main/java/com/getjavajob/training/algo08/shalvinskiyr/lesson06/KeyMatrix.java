package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

import java.util.Objects;

/**
 * Created by roman on 27.11.16.
 */
public class KeyMatrix<E> {
    private int i;
    private int j;

    public KeyMatrix(int i, int j) {
        this.i = i;
        this.j = j;
        hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyMatrix<?> keyMatrix = (KeyMatrix<?>) o;
        return i == keyMatrix.i &&
                j == keyMatrix.j;
    }

    @Override
    public int hashCode() {
        return Objects.hash(i, j);
    }
}
