package com.getjavajob.training.algo08.shalvinskiyr.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by roman on 23.11.16.
 */

interface CollectionFilter<E> {
    boolean check(E element);
}

interface ChangeTo<E, K> {
    K change(E element);
}

interface TransformList<E, K> {
    List<K> change(List<E> list);
}

interface SomeLogic<E> {
    E forElementsDo(E listElem);
}

public class CollectionUtils<E> {
    /**
     * @param list
     * @param predicate
     * @param <E>
     * @return
     */
    public static <E> List<E> filter(List<E> list, CollectionFilter<E> predicate) {
        List<E> newList = new ArrayList<>();
        for (E l : list) {
            if (predicate.check(l)) {
                newList.add(l);
            }
        }
        return newList;
    }

    /**
     * returns new collection
     *
     * @param list
     * @param rule
     * @param <E>
     * @param <K>
     * @return
     */
    public static <E, K> List<K> transform(List<E> list, ChangeTo<E, K> rule) {
        List<K> newList = new ArrayList<>();
        for (E l : list) {
            newList.add(rule.change(l));
        }
        return newList;
    }

//    /**
//     * @param list
//     * @param trans
//     * @param <E>
//     * @param <K>
//     * @return
//     */
//    public static <E, K> List<K> transform(List<E> list, TransformList<E, K> trans) {
//        return trans.change(list);
//    }

    public static <E> List<E> forAllDo(List<E> list, SomeLogic<E> logic) {
        for (E item : list) {
            logic.forElementsDo(item);
        }
        return list;
    }

    public static class UnmodifableCollection<E> implements Collection<E> {
        final Collection<E> UNMODCOL;

        public UnmodifableCollection(Collection<E> c) {
            this.UNMODCOL = c;
        }

        @Override
        public int size() {
            return UNMODCOL.size();
        }

        @Override
        public boolean isEmpty() {
            return UNMODCOL.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return UNMODCOL.contains(o);
        }

        @Override
        public Iterator<E> iterator() {
            return new Iterator<E>() {
                private final Iterator<? extends E> i;

                {
                    i = UnmodifableCollection.this.UNMODCOL.iterator();
                }

                public boolean hasNext() {
                    return this.i.hasNext();
                }

                public E next() {
                    return this.i.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        @Override
        public Object[] toArray() {
            return UNMODCOL.toArray();
        }

        @Override
        public <T> T[] toArray(T[] ts) {
            return UNMODCOL.toArray(ts);
        }

        @Override
        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean containsAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
    }
}
