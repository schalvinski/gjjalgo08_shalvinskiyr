package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

import com.getjavajob.training.algo08.shalvinskiyr.util.Assert;

import java.util.LinkedHashSet;

/**
 * Created by roman on 23.11.16.
 */
public class LinkedHashSetTest {
    public static void main(String[] args) {
        testAdd();
    }

    private static void testAdd() {
        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
        Assert.assertEquals("LinkedHashSetTest.addTrue", true, hashSet.add(12));
        Assert.assertEquals("LinkedHashSetTest.addFalse", false, hashSet.add(12));
    }
}
