package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

import com.getjavajob.training.algo08.shalvinskiyr.util.Assert;

import java.util.HashMap;
import java.util.Set;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 23.11.16.
 */
public class MapTest {
    public static void main(String[] args) {
        HashMap<Integer, Integer> map = new HashMap<>();
        assertEquals("MapTest.IsEmptyTest", true, map.isEmpty());
        map.put(2, 7);
        assertEquals("MapTest.ContainsKey", true, map.containsKey(2));
        assertEquals("MapTest.ContainsValue", true, map.containsValue(7));
        assertEquals("MapTest.getValue", 7, map.get(2));
        assertEquals("MapTest.putValue", 7, map.put(2, 7));
        map.put(0, 27);
        assertEquals("MapTest.Remove", 7, map.remove(2));
        HashMap<Integer, Integer> newMap = new HashMap<>();
        newMap.put(7, 34);
        newMap.put(2, 56);
        map.putAll(newMap);
        assertEquals("MapTest.putAll", 3, map.size());
        newMap.clear();
        assertEquals("MapTest.clear", 0, newMap.size());
        Set<Integer> set = map.keySet();
        Integer[] test2 = {27, 56, 34};
        Integer[] my = new Integer[3];
        map.values().toArray(my);
        Assert.assertEquals("Maptest.Values", my, test2);
    }
}
