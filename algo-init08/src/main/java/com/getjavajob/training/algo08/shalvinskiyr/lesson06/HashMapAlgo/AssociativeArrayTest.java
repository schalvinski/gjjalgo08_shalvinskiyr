package com.getjavajob.training.algo08.shalvinskiyr.lesson06.HashMapAlgo;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 26.07.16.
 */
public class AssociativeArrayTest {
    public static void main(String[] args) {
        testAddToArray();
        testAddNullToArray();
        testRemove();
        testGetElemntOfArray();
    }

    private static void testRemove() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "First");
        assertEquals("AssociativeArrayTest.testRemove", "First", array.remove(1));
    }

    private static void testAddToArray() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "First");
        assertEquals("AssociativeArrayTest.testAddToArray", "First", array.get(1));
    }

    private static void testAddNullToArray() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(null, "First");
        array.add(null, "First1");//перезаписал
        array.add(0, "First1");
        array.add(1, "First1");
        array.add(2, "First1" +
                "3");
        assertEquals("AssociativeArrayTest.testAddNullToArray", "First", array.get(null));
    }
    private static void testGetElemntOfArray() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.get(1);
        assertEquals("AssociativeArrayTest.testAddNullToArray", "First", array.get(1));
    }
}
