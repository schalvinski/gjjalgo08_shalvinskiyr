package com.getjavajob.training.algo08.shalvinskiyr.lesson03;

import java.util.*;

/**
 * Created by roman on 17.11.16.
 */

public class DynamicArray<E> extends AbstractList<E> {
    private E[] elements;
    private int elementsQuantity;
    private int modCount;

    public DynamicArray( int DEFAULT_CAPACITY) {
        elements = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public DynamicArray() {
        this(10);
    }

    /**
     * grow by 1,5 if array is full
     */
    private void growIfFull() {
        int newLength = (int) (elements.length * 1.5);
        elements = Arrays.copyOf(elements, newLength);
    }

    /**
     * add value to array
     *
     * @param value
     * @return
     */
    @Override
    public boolean add(E value) {
        if (elements.length > elementsQuantity && elements[elementsQuantity] == null) {
            elements[elementsQuantity] = value;
            elementsQuantity++;
            modCount++;
            return true;
        } else {
            growIfFull();
            return false;
        }
    }

    /**
     * ada value in exact cell
     *
     * @param i
     * @param value
     */
    @Override
    public void add(int i, E value) {
        if (elementsQuantity >= elements.length) {
            growIfFull();
        }
        System.arraycopy(elements, i, elements, i + 1, elementsQuantity - i);
        elements[i] = value;
        elementsQuantity++;
        modCount++;
    }

    /**
     * @param i
     * @param value
     * @return
     */
    @Override
    public E set(int i, E value) {
        E prev = elements[i];
        elements[i] = value;
        return prev;
    }

    /**
     * @param i
     * @return
     */
    @Override
    public E get(int i) {
        return elements[i];
    }

    /**
     * @param i
     * @return
     */
    @Override
    public E remove(int i) {
        E removed = elements[i];
        if (i < elementsQuantity - 1) {
            System.arraycopy(elements, i + 1, elements, i, elementsQuantity - i - 1);
        } else if (i == elementsQuantity) {
            elements[elementsQuantity] = null;
        }
        elementsQuantity--;
        modCount++;
        return removed;
    }

    /**
     * @param value
     * @return
     */
    @Override
    public boolean remove(Object value) {
        boolean b = false;
        for (int i = 0; i < elementsQuantity; i++) {
            if (elements[i].equals(value)) {
                remove(i);
                b = true;
            }
        }
        modCount++;
        return b;
    }

    /**
     * @return
     */
    @Override
    public int size() {
        return elementsQuantity;
    }

    /**
     * @param value
     * @return
     */
    @Override
    public int indexOf(Object value) {
        int k = -1;
        for (int i = 0; i < elementsQuantity; i++) {
            if (elements[i].equals(value)) {
                k = i;
                break;
            }
        }
        return k;
    }

    /**
     * @param value
     * @return
     */
    @Override
    public boolean contains(Object value) {
        for (int i = 0; i < elementsQuantity; i++) {
            if (elements[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return
     */
    @Override
    public E[] toArray() {
        return elements;
    }

    /**
     * @return
     */
    public DynamicArray.ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    public class ListIteratorImpl implements ListIterator {
        int cursor;
        int lastRet;
        int expectedModifications;

        /**
         * @param index
         */
        public ListIteratorImpl(int index) {
            this.cursor = index;
            this.lastRet = -1;
            this.expectedModifications = DynamicArray.this.modCount;
        }

        /**
         * @return
         */
        public boolean hasNext() {
            return cursor != DynamicArray.this.size();
        }

        /**
         * @return
         */
        public E next() {
            this.checkForComodification();
            int i = cursor;
            E[] elementData = DynamicArray.this.elements;
            if (i >= elementData.length) {
                throw new ConcurrentModificationException();
            } else {
                this.cursor = i + 1;
                return elementData[this.lastRet = i];
            }
        }

        /**
         * @return
         */
        public boolean hasPrevious() {
            return this.cursor != 0;
        }

        /**
         * @return
         */
        public E previous() {
            this.checkForComodification();
            int i = this.cursor - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            } else {
                E[] elementData = DynamicArray.this.elements;
                if (i >= elementData.length) {
                    throw new ConcurrentModificationException();
                } else {
                    this.cursor = i;
                    return elementData[this.lastRet = i];
                }
            }
        }

        /**
         * @return
         */
        public int nextIndex() {
            return this.cursor;
        }

        /**
         * @return
         */
        public int previousIndex() {
            return this.cursor - 1;
        }

        /**
         *
         */
        public void remove() {
            this.checkForComodification();
            try {
                DynamicArray.this.remove(this.lastRet);
                this.cursor = this.lastRet;
                this.lastRet = -1;
                this.expectedModifications = DynamicArray.this.modCount;
            } catch (IndexOutOfBoundsException var2) {
                throw new ConcurrentModificationException();
            }
        }

        /**
         * @param e
         */
        @Override
        public void set(Object e) {
            this.checkForComodification();
            try {
                DynamicArray.this.set(this.lastRet, (E) e);
            } catch (IndexOutOfBoundsException var3) {
                throw new ConcurrentModificationException();
            }
        }

        /**
         * @param e
         */
        @Override
        public void add(Object e) {
            this.checkForComodification();
            try {
                int ex = this.cursor;
                DynamicArray.this.add(ex, (E) e);
                this.cursor = ex + 1;
                this.lastRet = -1;
                this.expectedModifications++;
            } catch (IndexOutOfBoundsException var3) {
                throw new ConcurrentModificationException();
            }
        }

        public void checkForComodification() {
            if (DynamicArray.this.modCount != this.expectedModifications) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
