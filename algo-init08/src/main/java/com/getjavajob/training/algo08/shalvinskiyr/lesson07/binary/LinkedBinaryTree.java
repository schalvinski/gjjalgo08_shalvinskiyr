package com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary;

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;

import java.util.*;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> {
    //        extends AbstractBinaryTree<E> {
    protected static final NodeImpl NIL = new NodeImpl(null);
    public static NodeImpl root = NIL;
    public int size = 0;

//    public LinkedBinaryTree() {
//        size = 0;
//    }

    public static NodeImpl getRoot() {
        return root;
    }

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    //@Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        NodeImpl<E> r = new NodeImpl<>(e);
        r.parent = NIL;
        root = r;
        return r;
    }

    //@Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> r = new NodeImpl<>(e);
        NodeImpl<E> elem = validate(n);
        if (elem.left == null) {
            elem.left = new NodeImpl<E>(e);
            ((NodeImpl<E>) n).left.parent = (NodeImpl<E>) n;
            size++;
        } else if (((NodeImpl<E>) n).right == null) {
            elem.right = new NodeImpl<E>(e);
            elem.right.parent = (NodeImpl<E>) n;
            size++;
        } else {
            throw new IllegalArgumentException();
        }
        return r;
    }

    //@Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> left = new NodeImpl<E>(e);
        NodeImpl<E> elem = validate(n);
        if (elem.left != null) {
            elem.left = new NodeImpl<E>(e);
            elem.left.parent = (NodeImpl<E>) n;
            size++;
        } else {
            throw new IllegalArgumentException();
        }
        return left;
    }
//
//    //@Override
//    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
//        NodeImpl<E> el = validate(n);
//
//        if (el.getRight() != null) {
//            addRight(el.right, e);
//        } else {
//            el.right = new NodeImpl<E>(null, null,e);
//        }
//
//        return el;
//    }

    public void sizePlus() {
        size++;
    }

    //@Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> right = new NodeImpl<E>(e);
        NodeImpl<E> elem = validate(n);
        if (elem.right != null) {
            elem.right = new NodeImpl<E>(e);
            elem.right.parent = (NodeImpl<E>) n;
            size++;
        } else {
            throw new IllegalArgumentException();
        }
        return right;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    //@Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {

        NodeImpl<E> currentNode = (NodeImpl<E>) n;
        NodeImpl<E> elem = validate(n);
        if (currentNode == NIL) {
            throw new IllegalArgumentException();
        }
        E retValue = currentNode.getElement();
        currentNode.value = e;
        return retValue;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    //@Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> current = (NodeImpl<E>) n;
        if (current == NIL) {
            throw new IllegalArgumentException();
        }

        if (current.parent != null && current.parent.left != null && current.parent.left == current) {
            current.parent.left = NIL;
        } else if (current.parent != null && current.parent.right == current) {
            current.parent.right = NIL;
        }
        size--;
        return current.getElement();
    }

    // {@link Tree} and {@link BinaryTree} implementations

    //@Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> parent = (NodeImpl<E>) p;
        if (parent == NIL) {
            throw new IllegalArgumentException();
        }
        return parent.left;
    }

    //@Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> parent = (NodeImpl<E>) p;
        if (parent == NIL) {
            throw new IllegalArgumentException();
        }
        return parent.right;
    }

    //@Override
    public Node<E> root() {
        return root;
    }

    //@Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> child = (NodeImpl<E>) n;
        if (child.getElement() == null) {
            throw new IllegalArgumentException();
        }
        return child.parent;
    }

    //@Override
    public int size() {
        return size;
    }

    //@Override
    public Iterator<E> iterator() {
        LinkedList<E> list = new LinkedList<>();
        for (Node<E> n : nodes()) {
            list.add(n.getElement());
        }
        return list.iterator();
    }

    //@Override
    public Iterable<Node<E>> nodes() {
        LinkedList<Node<E>> llist = new LinkedList<Node<E>>();
        LinkedList<Node<E>> extra = new LinkedList<Node<E>>();
        NodeImpl<E> node = root;
        llist.push(node);
        while (!llist.isEmpty()) {
            node = (NodeImpl<E>) llist.pop();
            extra.add(node);
            if (node.left != null) {
                llist.push(node.left);
            }
            if (node.right != null) {
                llist.push(node.right);
            }
        }
        return extra;
    }

    //@Override
    public Iterable<Node<E>> breadthFirst() {
        List<Node<E>> breadthList = new ArrayList<>();
        Queue<Node<E>> q = new LinkedList<>();
        q.add(root());
        while (q.peek() != null) {
            NodeImpl<E> temp = (NodeImpl<E>) q.remove();
            breadthList.add(temp);
            if (temp.left != null) {
                q.add(temp.left);
            }
            if (temp.right != null) {
                q.add(temp.right);
            }
        }
        return breadthList;
    }

    public static class NodeImpl<E> implements Node<E> {

        public boolean color = true;
        public NodeImpl<E> parent = NIL;
        public NodeImpl<E> left = NIL;
        public NodeImpl<E> right = NIL;//?
        private E value;

        public NodeImpl(boolean color, NodeImpl<E> parent, NodeImpl<E> left, NodeImpl<E> right, E value) {
            this.color = color;
            this.parent = parent;
            this.left = left;
            this.right = right;
            this.value = value;
        }

//        public NodeImpl(NodeImpl<E> parent, NodeImpl<E> left, NodeImpl<E> right, E value) {
//            this.parent = parent;
//            this.left = left;
//            this.right = right;
//            this.value = value;
//        }
//
//        public NodeImpl(NodeImpl<E> lef, NodeImpl<E> rigt, E val) {
//            this.left = lef;
//            this.right = rigt;
//            this.value = val;
//        }

        public NodeImpl(E value) {
            this.value = value;
        }


        public NodeImpl<E> getRight() {
            return right;
        }

        public Node<E> getLeft() {
            return left;
        }

        @Override
        public E getElement() {
            return value;
        }
    }
}