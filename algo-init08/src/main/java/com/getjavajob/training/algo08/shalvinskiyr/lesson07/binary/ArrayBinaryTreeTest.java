package com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary;

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;

import java.util.ArrayList;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;
import static java.util.Arrays.asList;

/**
 * Created by roman on 23.11.16.
 */
public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        testLeft();
        testRight();
        testRoot();
        testParent();
        testAdd();
        testSet();
        testRemove();
        testPreorder();
        testInorder();
        testPostorder();
        testBreadthFirst();
    }

    private static void testBreadthFirst() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");
        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");
        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>(asList("F", "B", "G", "A", "D", "I", "C", "E", "H"));
        for (Node<String> n : tree.breadthFirst()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testBreadthFirst", res, cmp);
    }

    private static void testPostorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");
        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");
        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>(asList("A", "C", "E", "D", "B", "H", "I", "G", "F"));
        for (Node<String> n : tree.postOrder()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testPostorder", res, cmp);
    }

    private static void testInorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>(asList("A", "B", "C", "D", "E", "F", "G", "H", "I"));
        for (Node<String> n : tree.inOrder()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testInorder", res, cmp);
    }

    private static void testPreorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>(asList("F", "B", "A", "D", "C", "E", "G", "I", "H"));
        for (Node<String> n : tree.preOrder()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testPreorder", res, cmp);
    }

    private static void testLeft() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>(0, 12);
        tree.addRoot(11);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 12);
        assertEquals("ArrayBinaryTreeTest.testLeft", 12, tree.left(node).getElement());
    }

    private static void testRight() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> parent = new ArrayBinaryTree.NodeImpl<>(0, 1);
        tree.addRoot(1);
        tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 12);
        assertEquals("ArrayBinaryTreeTest.testRight", 12, tree.right(parent).getElement());
    }

    private static void testRoot() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        assertEquals("ArrayBinaryTreeTest.testRoot", 1, tree.root().getElement());
    }

    private static void testParent() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>();
        tree.addRoot(1);

        Node<Integer> ret = tree.addLeft(node, 127);
        assertEquals("ArrayBinaryTreeTest.testParent", 1, tree.parent(ret).getElement());
    }

    private static void testAdd() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>();
        tree.addRoot(1);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 14);
        Node<Integer> ret = tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 19);
        assertEquals("ArrayBinaryTreeTest.testAdd", 19, tree.parent(tree.add(ret, 117)).getElement());
    }

    private static void testSet() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 14);
        Node<Integer> ret = tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 19);
        assertEquals("ArrayBinaryTreeTest.testSet", 19, tree.set(ret, 67));
        assertEquals("ArrayBinaryTreeTest.testSet", 67, tree.right(tree.root()).getElement());
    }

    private static void testRemove() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>();
        tree.addRoot(1);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 14);
        Node<Integer> ret = tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 19);
        assertEquals("ArrayBinaryTreeTest.testRemove", 19, tree.remove(ret));
    }
}
