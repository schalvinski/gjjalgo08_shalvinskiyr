package com.getjavajob.training.algo08.shalvinskiyr.lesson08.balanced;

/**
 * Created by roman on 23.11.16.
 */

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;
import com.getjavajob.training.algo08.shalvinskiyr.lesson07.binary.LinkedBinaryTree.NodeImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

public class BalanceableTreeTest<E> {

    public static void main(String[] args) {
        testRotate2();
        testRotate();
        testRotateTwice();
        testToString();
    }
    public static void testRotate2() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> myTop = tree.addRoot(67);
        NodeImpl<Integer> node = (NodeImpl<Integer>) myTop;
        tree.addLeft(node, 34);
        tree.addRight(node.left, 36);
        tree.addLeft(node.left, 14);
//        tree.addLeft(node.parent, 16);
//        tree.addLeft(node.parent.left, 16);
//        tree.addLeft(node.parent.parent, 17);
        tree.addLeft(node.left.left, 12);
        tree.addRight(node, 72);
        tree.printBtree((NodeImpl<Integer>) tree.root());
        tree.reduceSubtreeHeight(node.left.left); //right
        tree.printBtree((NodeImpl<Integer>) tree.root());
        ArrayList<Integer> listFirst = new ArrayList<>();
        List<Integer> compare = new ArrayList(Arrays.asList(36,12,67,14,34,72));

        for (Node<Integer> n : tree.breadthFirst()) {
            listFirst.add(n.getElement());
        }
        assertEquals("BalanceableTreeTest.testRotateLeft", compare, listFirst);
        System.out.println("****************************************************");
        tree.rotate(((NodeImpl<Integer>) tree.root()).right); // left
        listFirst.clear();
        compare.clear();
        compare.add(67);
        compare.add(36);
        compare.add(72);
        compare.add(12);
        compare.add(34);
        compare.add(14);
        for (Node<Integer> n : tree.breadthFirst()) {
            listFirst.add(n.getElement());
        }
        assertEquals("BalanceableTreeTest.testRotateRight", compare, listFirst);
        tree.printBtree((NodeImpl<Integer>) tree.root());
        System.out.println("****************************************************");
    }

    public static void testRotate() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> myTop = tree.addRoot(67);
        NodeImpl<Integer> node = (NodeImpl<Integer>) myTop;
        tree.addLeft(node, 36);
        tree.addRight(node.left, 34);
        tree.addLeft(node.left, 12);
        tree.addRight(node.left.left, 14);
        tree.addRight(node, 72);
        tree.rotate(node.left); //right
        ArrayList<Integer> listFirst = new ArrayList<>();
        ArrayList<Integer> compare = new ArrayList() {{
            add(36);
            add(12);
            add(67);
            add(14);
            add(34);
            add(72);
        }};

        for (Node<Integer> n : tree.breadthFirst()) {
            listFirst.add(n.getElement());
        }
        assertEquals("BalanceableTreeTest.testRotateLeft", compare, listFirst);
        System.out.println("***************************************************");
        tree.rotate(((NodeImpl<Integer>) tree.root()).right); // left
        listFirst.clear();
        compare.clear();
        compare.add(67);
        compare.add(36);
        compare.add(72);
        compare.add(12);
        compare.add(34);
        compare.add(14);
        for (Node<Integer> n : tree.breadthFirst()) {
            listFirst.add(n.getElement());
        }
        assertEquals("BalanceableTreeTest.testRotateRight", compare, listFirst);
        System.out.println("****************************************************");

    }

    public static void testRotateTwice() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> myTop = tree.addRoot(67);
        NodeImpl<Integer> node = (NodeImpl<Integer>) myTop;
        tree.addLeft(node, 36);
        tree.addRight(node.left, 44);
        tree.addRight(node.left.right, 54);
        tree.addLeft(node.left.right, 39);
        tree.addLeft(node.left, 12);
        tree.addRight(node.left.left, 14);
        tree.addRight(node, 72);
        tree.rotateTwice(node.left.right);
        ArrayList<Integer> listFirst = new ArrayList<>();
        ArrayList<Integer> compare = new ArrayList() {{
            add(44);
            add(36);
            add(67);
            add(12);
            add(39);
            add(54);
            add(72);
            add(14);
        }};
        for (Node<Integer> n : tree.breadthFirst()) {
            listFirst.add(n.getElement());
        }
        assertEquals("BalanceableTreeTest.testRotateTwice", compare, listFirst);
        System.out.println("****************************************************");
    }

    private static void testToString() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> myTop = tree.addRoot(67);
        NodeImpl<Integer> node = (NodeImpl<Integer>) myTop;
        tree.addLeft(node, 34);
        tree.addRight(node.left, 36);
        tree.addLeft(node.left, 13);
        tree.addRight(node.left.left, 14);
        tree.addRight(node, 72);
        String compare = new String("67 [36 [12 [null [] []] [14 [null [] []] [null [] []]]] [34 [null [] []] [null [] []]]] [72 [null [] []] [null [] []]]");
        assertEquals("BalanceableTreeTest.testToString", compare, tree.toString((NodeImpl<Integer>) tree.root()));
        tree.printBtree((NodeImpl<Integer>) tree.root());
        System.out.println("****************************************************");
    }
}
