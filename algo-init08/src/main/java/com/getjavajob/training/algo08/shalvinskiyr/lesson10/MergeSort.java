package com.getjavajob.training.algo08.shalvinskiyr.lesson10;

/**
 * Created by roman on 19.11.2016.
 */
public class MergeSort<E> {
    public void sort(E[] arr) {
        E[] tempArray = (E[]) new Object[arr.length];
        doMerge(arr, tempArray, 0, arr.length - 1);
    }

    private void doMerge(E[] arr, E[] tempArray, int lowerIndex, int upperIndex) {
        if (lowerIndex != upperIndex) {
            int mid = (lowerIndex + upperIndex) / 2;
            doMerge(arr, tempArray, lowerIndex, mid);
            doMerge(arr, tempArray, mid + 1, upperIndex);
            merge(arr, tempArray, lowerIndex, mid + 1, upperIndex);
        }
    }

    private void merge(E[] arr, E[] tempArray, int lowerIndexCursor, int higerIndex, int upperIndex) {
        int tempIndex = 0;
        int lowerIndex = lowerIndexCursor;
        int midIndex = higerIndex - 1;
        int totalItems = upperIndex - lowerIndex + 1;
        while (lowerIndex <= midIndex && higerIndex <= upperIndex) {
            if (arr[lowerIndex].hashCode() < arr[higerIndex].hashCode()) {
                tempArray[tempIndex++] = arr[lowerIndex++];
            } else {
                tempArray[tempIndex++] = arr[higerIndex++];
            }
        }
        while (lowerIndex <= midIndex) {
            tempArray[tempIndex++] = arr[lowerIndex++];
        }
        while (higerIndex <= upperIndex) {
            tempArray[tempIndex++] = arr[higerIndex++];
        }
        System.arraycopy(tempArray, 0, arr, lowerIndexCursor, totalItems);
    }
}
