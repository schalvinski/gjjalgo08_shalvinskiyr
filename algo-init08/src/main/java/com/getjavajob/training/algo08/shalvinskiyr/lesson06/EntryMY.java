package com.getjavajob.training.algo08.shalvinskiyr.lesson06;

/**
 * Created by roman on 23.11.16.
 */
public class EntryMY<K, V> {
    K key;
    V value;
    EntryMY<K, V> next;
    int hash;

    public EntryMY(K key, V value, EntryMY<K, V> next, int hash) {
        this.key = key;
        this.value = value;
        this.next = next;
        this.hash = hash;
    }


}




















//public class EntryMY<K, V> {
//    K key;
//    V value;
//    EntryMY<K, V> next;
//    int hash;
//
//    public EntryMY(int hash, K key, V value, EntryMY next) {
//        this.hash = hash;
//        this.key = key;
//        this.value = value;
//        this.next = next;
//    }
//
//    public final V setValue(V newValue) {
//        V oldValue = this.value;
//        this.value = newValue;
//        return oldValue;
//    }
//
//    public K getKey() {
//        return this.key;
//    }
//
//    public V getValue() {
//        return this.value;
//    }
//
//    @Override
//    public String toString() {
//        return "Entry{" +
//                "hash=" + hash +
//                ", key=" + key +
//                ", value='" + value + '\'' +
//                ", next=" + next +
//                '}';
//    }
//
//}
