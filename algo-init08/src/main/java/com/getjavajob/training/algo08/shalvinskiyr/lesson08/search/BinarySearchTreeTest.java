package com.getjavajob.training.algo08.shalvinskiyr.lesson08.search;

import com.getjavajob.training.algo08.shalvinskiyr.lesson07.Node;

import java.util.Comparator;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by roman on 23.11.16.
 */
public class BinarySearchTreeTest {

    public static void main(String[] args) {
        testCompare();
        testBinarySearch();
    }

    public static void testCompare() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return integer - t1;
            }
        });
        assertEquals("testCompare'>'", true, bst.compare(77, 5) > 0);
        assertEquals("testCompare'<", false, bst.compare(5, 77) > 0);
        assertEquals("testCompare'==", true, bst.compare(77, 77) == 0);
    }

    public static void testBinarySearch() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer a1, Integer a2) {
                return a1 - a2;
            }
        });
        Node<Integer> node = bst.addRoot(11);
        bst.addLeft(bst.root(), 8);
        bst.addRight(bst.root(), 15);
        assertEquals("BinarySearchTreeTest.testBinarySearch", 15, bst.treeSearch(node, 15).getElement());
    }
}
