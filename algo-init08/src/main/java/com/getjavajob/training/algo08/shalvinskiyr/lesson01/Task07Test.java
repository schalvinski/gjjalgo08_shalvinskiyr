package com.getjavajob.training.algo08.shalvinskiyr.lesson01;

import static com.getjavajob.training.algo08.shalvinskiyr.util.Assert.assertEquals;

/**
 * Created by admin on 12.08.2015.
 */
public class Task07Test {
    public static void main(String[] args) {
        testSwap2VarArithmetic();
        testSwap2VarArithmetic2();
        testSwap2VarBitwise();
        testSwap2VarBitwise2();
    }

    public static void testSwap2VarBitwise() {
        assertEquals("testSwap2VarBitwise", "x= 7 y= 10", Task07.swap2VarArithmetic(10, 7));
    }

    public static void testSwap2VarBitwise2() {
        assertEquals("testSwap2VarBitwise2", "x= 7 y= 10", Task07.swap2VarArithmetic(10, 7));
    }

    public static void testSwap2VarArithmetic() {
        assertEquals("testSwap2VarArithmetic", "x= 7 y= 10", Task07.swap2VarArithmetic(10, 7));
    }

    public static void testSwap2VarArithmetic2() {
        assertEquals("testSwap2VarArithmetic2", "x= 7 y= 10", Task07.swap2VarArithmetic(10, 7));
    }
}
